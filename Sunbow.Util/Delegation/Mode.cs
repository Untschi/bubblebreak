﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace Sunbow.Util.Delegation
{
    public abstract class Mode
    {
        public ModeProxy Proxy { get; internal set; }
        protected Microsoft.Xna.Framework.Game Game { get { return Proxy.Game; } }
        protected ContentManager Content { get { return Proxy.Game.Content; } }
        protected GraphicsDevice GraphicsDevice { get { return Proxy.Game.GraphicsDevice; } }

        internal protected abstract void Enter();
        internal protected abstract void Leave();

        internal protected abstract void Update(GameTime gameTime, InputState inputState);
        internal protected abstract void Draw(GameTime gameTime, SpriteBatch spriteBatch);
    }
}
