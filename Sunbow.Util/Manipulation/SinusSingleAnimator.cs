﻿
using System;
using Microsoft.Xna.Framework;

namespace Sunbow.Util.Manipulation
{
    /// <summary>
    /// an animator which modifies a Single over time by using the Sinus function
    /// </summary>
    public class SinusSingleAnimator : SingleAnimator
    {

        #region Variables
        // -- VARIABLES --
        /// <param name="offset">the start direction of the animation in radians</param>
        public float StartAngle { get { return startAngle; } set { startAngle = value; } }
        /// <param name="radians">the amount of radians inside the animation</param>
        public float Radians { get { return radians; } set { radians = value; } }
        /// <summary>A scalar value to shrink or stretch the curve</summary>
        public float Scale { get { return scale; } set { scale = value; } }

        public float Offset { get { return offset; } set { offset = value; } }

        float startAngle, radians, scale, offset;

        #endregion // Variables

        #region Constructors
        // -- CONSTRUCT --
        /// <summary>creates a new instance</summary>
        /// <param name="startAngle">the start direction in radians</param>
        /// <param name="radians">the amount of radians inside the animation</param>
        /// <param name="scale">scales the output</param>
        /// <param name="offset">shifts the output (scale included)</param>
        /// <param name="duration">the duration in seconds</param>
        /// <param name="repeatLogic">the logic, how the animation repeats</param>
        public SinusSingleAnimator(float startAngle, float radians, float scale, float offset, float duration, AnimationRepeatLogic repeatLogic)
            : base(duration, repeatLogic)
        {
            this.startAngle = startAngle;
            this.radians = radians;
            this.scale = scale;
            this.offset = offset;
        }

        /// <summary>
        /// Accelerating curve from -1 to 1
        /// </summary>
        public SinusSingleAnimator(float duration)
            : this(-0.5f * (float)Math.PI, (float)Math.PI, 0.5f, 0.5f, duration, AnimationRepeatLogic.Pause)
        {
        }

        /// <summary>
        /// Creates an animator that starts at "start" and runs smooth (acceleration / deceleration) to "end".
        /// </summary>
        public static SinusSingleAnimator CreateSmoothByStartEnd(float start, float end, float duration)
        {
            return new SinusSingleAnimator(
                -(float)Math.PI / 2,
                (float)Math.PI,
                (end - start) / 2f,
                start + (end - start) / 2f,
                duration,
                AnimationRepeatLogic.Pause);
        }

        #endregion // Constructors

        #region Methods
        // -- METHODS --


        protected override void UpdateValue(float lerpValue, out float value)
        {
            value = offset + scale * (float)Math.Sin(MathHelper.Lerp(startAngle, startAngle + radians, lerpValue));
        }
        #endregion // Methods
    }
}