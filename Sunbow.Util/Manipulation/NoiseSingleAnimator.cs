﻿
using System;
using Microsoft.Xna.Framework;

namespace Sunbow.Util.Manipulation
{
    /// <summary>
    /// an Animator which creates a random-looking animation for a Single
    /// </summary>
    public class NoiseSingleAnimator : SingleAnimator
    {

        #region Variables
        // -- VARIABLES --

        /// <param name="minNoise">the minimum amount of change within one frame (positive OR negative)</param>
        public float MinNoise { get { return minNoise; } set { minNoise = value; } }
        /// <param name="maxNoise">the maximum amount of change within one frame (positive OR negative)</param>
        public float MaxNoise { get { return maxNoise; } set { maxNoise = value; } }
        float minNoise, maxNoise;
        #endregion // Variables

        #region Constructors
        // -- CONSTRUCT --
        /// <summary></summary>
        /// <param name="minNoise">the minimum amount of change within one frame (positive OR negative)</param>
        /// <param name="maxNoise">the maximum amount of change within one frame (positive OR negative)</param>
        /// <param name="duration">the duration in seconds</param>
        /// <param name="repeatLogic">the logic, how the animation repeats</param>
        public NoiseSingleAnimator(float minNoise, float maxNoise, float duration, AnimationRepeatLogic repeatLogic)
            : base(duration, repeatLogic)
        {
            this.minNoise = minNoise;
            this.maxNoise = maxNoise;
        }
        #endregion // Constructors

        #region Methods
        // -- METHODS --


        protected override void UpdateValue(float lerpValue, out float value)
        {
            float tmp = (float)Random.ProjectUInt16((int)(lerpValue * ushort.MaxValue)) / ushort.MaxValue;
            float noise = MathHelper.Lerp(minNoise, maxNoise, tmp);
            noise *= (tmp % 2 == 0) ? -1 : 1;

            value = noise;
        }
        #endregion // Methods
    }
}