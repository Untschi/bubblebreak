﻿
using System;

namespace Sunbow.Util.Manipulation
{
    /// <summary>
    /// 
    /// </summary>
    public interface IValueAnimator
    {
        float Value { get; }

        event EventHandler EndOfAnimation;

        bool IsPaused { get; set; }

        void Reset();

        void Update(float seconds);
    }
}