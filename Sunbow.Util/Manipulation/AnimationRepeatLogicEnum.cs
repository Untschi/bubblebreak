﻿
using System;

namespace Sunbow.Util.Manipulation
{
    /// <summary>
    /// 
    /// </summary>
    public enum AnimationRepeatLogic
    {
        /// <summary>The Animation pauses</summary>
        Pause = 0,
        /// <summary>the Animation resets and pauses</summary>
        Stop = 1,
        /// <summary>the Animation resets and performs the animation again</summary>
        Repeat = 2,
        /// <summary>the Animation never stops. it won't be resettet so it is going farer and farer</summary>
        RepeatAppended = 3,
        /// <summary>the Animation plays backwards when the end has been arrived. when the beginning has arrived it will go forwards again.</summary>
        Flip = 4,
    }

}