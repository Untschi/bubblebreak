﻿
using System;
using Microsoft.Xna.Framework;

namespace Sunbow.Util.Manipulation
{

    /// <summary>
    /// The SingleAnimator is the base class for objects which can animate a Single ( = float).
    /// </summary>
    public abstract class SingleAnimator : IValueAnimator
    {

        #region Variables
        // -- VARIABLES --

        /// <summary>
        /// The current Value of the animation
        /// </summary>
        public float Value { get { return value; } }
        protected float value;

        /// <summary>
        /// This event ist always called when the time has passed the duration of the animation.
        /// NOTE: if the RepeatLogic is set to Flip, this event is called when the start point of the animation has arived again (which is 2*duration)
        /// </summary>
        public event EventHandler EndOfAnimation;

        /// <summary>
        /// The Duration in seconds how long the animation takes.
        /// </summary>
        public float Duration { get { return duration; } set { duration = value; } }
        float duration;
        float time;

        /// <summary>
        /// The logic which indicates what happens when the animation has arrived it's end.
        /// </summary>
        public AnimationRepeatLogic RepeatLogic { get { return repeatLogic; } set { repeatLogic = value; } }
        AnimationRepeatLogic repeatLogic;

        public bool IsPaused { get { return isPaused; } set { isPaused = value; } }
        bool isPaused;

        #endregion // Variables

        #region Constructors
        // -- CONSTRUCT --
        /// <summary></summary>
        public SingleAnimator(float duration, AnimationRepeatLogic repeatLogic)
        {
            this.duration = duration;

            this.repeatLogic = repeatLogic;

            Update(0);
        }
        #endregion // Constructors

        #region Methods
        // -- METHODS --

        /// <summary>
        /// pauses the animation.
        /// </summary>
        public void Pause()
        {
            isPaused = true;
        }
        /// <summary>
        /// if previously paused, the animation will play again. If it is already running: no change at all
        /// </summary>
        public void Play()
        {
            isPaused = false;
        }

        /// <summary>
        /// Reset's the animation
        /// </summary>
        public void Reset()
        {
            time = 0;

            bool tmp = isPaused;
            isPaused = false;
            Update(0);
            isPaused = tmp;
        }
    


        #region IUpdatable Member

        
        /// <summary>
        /// Updates the animation
        /// </summary>
        public void Update(GameTime gameTime)
        {
            Update((float)gameTime.ElapsedGameTime.TotalSeconds);
        }
        public void Update(float seconds)
        {
            if (isPaused)
                return;

            time += seconds;

            float tmp = time / duration;

            if (time > duration)
            {
                switch (repeatLogic)
                {
                    case AnimationRepeatLogic.Stop:
                        Reset();
                        Pause();
                        OnEndOfAnimation();
                        break;
                    case AnimationRepeatLogic.Pause:
                        tmp = 1;
                        Pause();
                        OnEndOfAnimation();
                        break;
                    case AnimationRepeatLogic.Repeat:
                        Reset();
                        OnEndOfAnimation();
                        break;
                    case AnimationRepeatLogic.RepeatAppended:
                        OnEndOfAnimation();
                        break;
                    case AnimationRepeatLogic.Flip:
                        if (time < 2 * duration)
                        {
                            tmp = 1 - (tmp - 1);
                        }
                        else
                        {
                            time -= 2 * duration;
                            tmp -= 2;

                            OnEndOfAnimation();
                        }
                        break;
                    default:
                        throw new NotImplementedException();
                }
            }

            UpdateValue(tmp, out value);
        }

        protected abstract void UpdateValue(float lerpValue, out float value);


        private void OnEndOfAnimation()
        {
            if (EndOfAnimation != null)
                EndOfAnimation(this, new EventArgs());
        }
        #endregion

        public static implicit operator float(SingleAnimator a)
        {
            return a.Value;
        }
        #endregion // Methods

    }
}