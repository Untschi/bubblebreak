﻿
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace Sunbow.Util.Manipulation
{
    /// <summary>
    /// This Enum indicates how values shall be added
    /// </summary>
    public enum ValueCombinationMode
    {
        /// <summary>
        /// the middle value will be chosen => (a + b) / 2
        /// </summary>
        Combine = 0,
        /// <summary>
        /// the values are just added together => a + b
        /// </summary>
        Shift = 1,
        /// <summary>
        /// the value scales the previous result => a * b
        /// </summary>
        Scale = 2,
    }

    /// <summary>
    /// this class can animate a Single by composing some SingleAnimators and/or other UpdatableValueCompositions together.
    /// </summary>
    public class AnimatableValueComposition : IValueAnimator
    {

        #region Variables
        // -- VARIABLES --

        /// <summary>
        /// Contains the Value which is animated.
        /// </summary>
        public float Value { get { return value; } }
        float value;

        /// <summary>
        /// Scales the result of the value animation
        /// </summary>
        public float Scale { get { return scale; } set { scale = value; } }
        float scale = 1;

        /// <summary>
        /// Shifts the result of the value animation
        /// </summary>
        public float Shift { get { return shift; } set { shift = value; } }
        float shift = 0;

        public IValueAnimator this[int index] { get { return animators[index].Key; } }
        List<KeyValuePair<IValueAnimator, ValueCombinationMode>> animators = new List<KeyValuePair<IValueAnimator, ValueCombinationMode>>();


        public bool IsPaused { get { return isPaused; } set { isPaused = value; } }
        bool isPaused;

        /// <summary>
        /// The EndOfAnimation event will be called, when all the animators are paused the first time.
        /// </summary>
        public event EventHandler EndOfAnimation;
        #endregion // Variables

        #region Constructors
        // -- CONSTRUCT --
        /// <summary>creates a new instance</summary>
        /// <param name="baseAnimator">the basis animation which is a SingleAnimator in common</param>
        public AnimatableValueComposition(IValueAnimator baseAnimator)
        {
            animators.Add(new KeyValuePair<IValueAnimator, ValueCombinationMode>(baseAnimator, ValueCombinationMode.Shift));
        }
        #endregion // Constructors

        #region Methods
        // -- METHODS --

        public void Reset()
        {
            foreach(IValueAnimator ani in this.UpdatableIterator())
            {
                ani.Reset();
            }
        }

        /// <summary>
        /// Adds another animator to the composition. 
        /// Note that a different order of adding animators will cause a different result
        /// </summary>
        /// <param name="animator">the animator to add (a SingleAnimator or another UpdatableValueComposition in common)</param>
        /// <param name="combinationMode">indicates the mode how the value is added to the results of the previously added animators</param>
        public void Add(IValueAnimator animator, ValueCombinationMode combinationMode)
        {
            animators.Add(new KeyValuePair<IValueAnimator, ValueCombinationMode>(animator, combinationMode));
            isPaused = false;
        }
        /// <summary>
        /// Tries to remove an animator. Only the first matching animator will be removed.
        /// </summary>
        /// <param name="animator">the animator object to remove</param>
        /// <returns>true, if the animator could be found. otherwise false.</returns>
        public bool TryRemove(IValueAnimator animator)
        {
            for(int i = 0; i < animators.Count; i++)
            {
                if (animators[i].Key == animator)
                {
                    animators.RemoveAt(i);
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Clears all animators and sets a new base animator
        /// </summary>
        /// <param name="newBaseAnimator">the basis animation which is a SingleAnimator in common</param>
        public void Clear(IValueAnimator newBaseAnimator)
        {
            animators.Clear();
            Add(newBaseAnimator, ValueCombinationMode.Shift);
            isPaused= false;
        }

        public void Update(GameTime gameTime)
        {
            Update((float)gameTime.ElapsedGameTime.TotalSeconds);
        }

        /// <summary>
        /// Call this method to update the animations.
        /// Note: the added animators will be updated from within this method!
        /// </summary>
        public void Update(float seconds)
        {
            if (isPaused)
                return;

            value = 0;
            isPaused = true;
            foreach (KeyValuePair<IValueAnimator, ValueCombinationMode> entry in animators)
            {
                entry.Key.Update(seconds);

                switch (entry.Value)
                {
                    case ValueCombinationMode.Combine:
                        value = 0.5f * (value + entry.Key.Value);
                        break;
                    case ValueCombinationMode.Shift:
                        value += entry.Key.Value;
                        break;
                    case ValueCombinationMode.Scale:
                        value *= entry.Key.Value; 
                        break;
                    default:
                        throw new NotImplementedException();
                }
                isPaused = isPaused && entry.Key.IsPaused;
            }
            value *= scale;
            value += shift;

            if (isPaused && EndOfAnimation != null)
                EndOfAnimation(this, new EventArgs());
        }

        /// <summary>
        /// Iterates through all updatable values
        /// </summary>
        public IEnumerable<IValueAnimator> UpdatableIterator()
        {
            foreach (KeyValuePair<IValueAnimator, ValueCombinationMode> entry in animators)
                yield return entry.Key;
        }

        /// <summary>
        /// iterates through all SingleUpdater instances which are added to this composition
        /// </summary>
        public IEnumerable<SingleAnimator> SingleUpdaterIterator()
        {
            foreach (KeyValuePair<IValueAnimator, ValueCombinationMode> entry in animators)
                if(entry.Key is SingleAnimator)
                    yield return entry.Key as SingleAnimator;
        }

        public static implicit operator float(AnimatableValueComposition a)
        {
            return a.Value;
        }

        #endregion // Methods



    }
}