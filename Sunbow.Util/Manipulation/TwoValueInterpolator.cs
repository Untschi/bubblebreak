﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunbow.Util.Manipulation
{
    public class TwoValueInterpolator<T>
    {
        public T StartValue { get; set; }
        public T EndValue { get; set; }
        public T CurrentValue { get; private set; }

        Func<T, T, float, T> lerpMethod;

        public TwoValueInterpolator(Func<T, T, float, T> lerpMethod)
            : this(default(T), default(T), lerpMethod)
        { }
        public TwoValueInterpolator(T startValue, T endValue, Func<T, T, float, T> lerpMethod)
        {
            this.StartValue = startValue;
            this.EndValue = endValue;
            this.lerpMethod = lerpMethod;

            this.CurrentValue = StartValue;
        }

        public T Lerp(float amount)
        {
            CurrentValue = lerpMethod(StartValue, EndValue, amount);
            return CurrentValue;
        }

        public void SetValues(T allValues)
        {
            SetValues(allValues, allValues);
        }

        public void SetValues(T start, T end)
        {
            StartValue = start;
            EndValue = end;
            CurrentValue = start;
        }

    }
}
