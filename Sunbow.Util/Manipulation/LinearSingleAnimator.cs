﻿
using System;
using Microsoft.Xna.Framework;

namespace Sunbow.Util.Manipulation
{
    /// <summary>
    /// an Animator which interpolates a Single linear over time.
    /// </summary>
    public class LinearSingleAnimator : SingleAnimator
    {

        #region Variables
        // -- VARIABLES --

        /// <summary>Get's / Set's the start value of the animation</summary>
        public float Start { get { return start; } set { start = value; } }
        /// <summary>Get's / Set's the end value of the animation</summary>
        public float End { get { return end; } set { end = value; } }
        float start, end;

        #endregion // Variables

        #region Constructors
        // -- CONSTRUCT --
        /// <summary>creates a new instance</summary>
        /// <param name="start">the value where to start</param>
        /// <param name="end">the end value</param>
        /// <param name="duration">the duration in seconds</param>
        /// <param name="repeatLogic">the logic, how the animation repeats</param>
        public LinearSingleAnimator(float start, float end, float duration, AnimationRepeatLogic repeatLogic)
            : base(duration, repeatLogic)
        {
            this.start = start;
            this.end = end;
        }
        #endregion // Constructors

        #region Methods
        // -- METHODS --

        protected override void UpdateValue(float lerpValue, out float value)
        {
            value = MathHelper.Lerp(start, end, lerpValue);
        }
        #endregion // Methods
    }
}