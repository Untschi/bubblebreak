﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Sunbow.Util.Manipulation
{
    public class CurveSingleAnimator : SingleAnimator
    {
        public Curve Curve { get { return curve; } }
        Curve curve;

        float length;

        public CurveSingleAnimator(Curve curve, float duration, AnimationRepeatLogic repeatLogic)
            : base(duration, repeatLogic)
        {
            this.curve = curve;
            length = curve.Keys[curve.Keys.Count - 1].Position;
            Update(0);
        }

        protected override void UpdateValue(float lerpValue, out float value)
        {
            if (curve == null)
            {
                value = 0;
                return;
            }
            value = curve.Evaluate(lerpValue * length);
        }
    }
}
