using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.IsolatedStorage;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Sunbow.Util
{
    public static class Extensions
    {
        public static int CompareKeys<TKey, TValue>(this KeyValuePair<TKey, TValue> self, KeyValuePair<TKey, TValue> other)
            where TKey : IComparable<TKey>
        {
            return self.Key.CompareTo(other.Key);
        }
        public static int CompareValues<TKey, TValue>(this KeyValuePair<TKey, TValue> self, KeyValuePair<TKey, TValue> other)
            where TValue : IComparable<TValue>
        {
            return self.Value.CompareTo(other.Value);
        }

        public static bool IsBetween<T>(this T value, T a, T b)
            where T : IComparable<T>
        {
            if(a.CompareTo(b) == 1)
            {
                T tmp = a;
                a = b;
                b = tmp;
            }

            return value.CompareTo(a) >= 0 && value.CompareTo(b) <= 0;
        }

        public static float GetElapsedSeconds(this GameTime gameTime)
        {
            return (float)gameTime.ElapsedGameTime.TotalSeconds;
        }

        public static Vector2 GetPosition(this MouseState self)
        {
            return new Vector2(self.X, self.Y);
        }

        public static T Pop<T>(this Queue<T> self)
        {
            Queue<T> tmp = new Queue<T>();

            while (self.Count > 1)
                tmp.Enqueue(self.Dequeue());

            T result = self.Dequeue();

            while (tmp.Count > 0)
                self.Enqueue(tmp.Dequeue());

            return result;
        }

        public static Point ToPoint(this Vector2 self)
        {
            return new Point((int)self.X, (int)self.Y);
        }
    }
}
