﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;

namespace Sunbow.Util
{
    public class ObjectPool<T> : IEnumerable<T>
        where T : class
    {
        public ReadOnlyCollection<T> ActiveObjects { get { return activeObjects.AsReadOnly(); } }
        protected List<T> activeObjects = new List<T>();
        protected Queue<T> inactiveObjects = new Queue<T>();

        Func<T> factory;

        public ObjectPool(Func<T> factoryMethod)
        {
            this.factory = factoryMethod;
        }

        public T ActivateNext()
        {
            T result;
            if (inactiveObjects.Count == 0)
                result = factory();
            else
                result = inactiveObjects.Dequeue();

            activeObjects.Add(result);
            return result;
        }

        public void Deactivate(T obj)
        {
            inactiveObjects.Enqueue(obj);
            activeObjects.Remove(obj);

            System.Diagnostics.Debug.WriteLine(" -- deactivated");
        }

        public void Deactivate(int index)
        {
            T obj = activeObjects[index];
            inactiveObjects.Enqueue(obj);
            activeObjects.RemoveAt(index);
        }


        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            for (int i = 0; i < activeObjects.Count; i++)
            {
                yield return activeObjects[i];
            }
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            for (int i = 0; i < activeObjects.Count; i++)
            {
                yield return activeObjects[i];
            }
        }
    }
}
