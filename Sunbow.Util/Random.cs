﻿

using System;
using System.Collections.Generic;
using System.Text;

namespace Sunbow.Util
{
    /// <summary>
    /// Provides thread safe random numbers
    /// </summary>
    public static class Random
    {
        //Array containing all byte values [0, 255], in a static, randomized order.
        private static readonly byte[] RANDOM_BYTES = new byte[] { 62, 189, 38, 126, 192, 7, 138, 128, 70, 55, 232, 166, 118, 17, 236, 74, 241, 57, 249, 80, 229, 83, 29, 183, 58, 6, 158, 243, 195, 107, 111, 174, 205, 33, 71, 113, 139, 255, 225, 120, 199, 28, 218, 26, 88, 149, 162, 1, 202, 157, 141, 35, 66, 92, 133, 237, 160, 108, 67, 185, 104, 151, 190, 217, 203, 52, 215, 84, 204, 122, 188, 242, 81, 127, 132, 200, 112, 2, 94, 164, 97, 154, 37, 32, 223, 73, 105, 115, 156, 85, 211, 22, 184, 41, 12, 47, 98, 3, 79, 27, 193, 77, 51, 82, 227, 161, 210, 196, 216, 186, 123, 155, 13, 165, 86, 212, 109, 152, 253, 125, 208, 99, 180, 116, 171, 168, 48, 36, 114, 76, 100, 119, 178, 59, 238, 18, 96, 224, 60, 56, 135, 176, 167, 233, 230, 43, 11, 21, 221, 54, 201, 206, 5, 220, 75, 89, 147, 194, 169, 49, 101, 170, 68, 131, 78, 8, 246, 209, 182, 46, 106, 146, 87, 187, 254, 24, 207, 228, 144, 121, 145, 20, 65, 40, 143, 163, 110, 234, 248, 140, 130, 172, 34, 9, 239, 231, 72, 23, 137, 226, 53, 50, 93, 153, 222, 31, 117, 63, 235, 30, 150, 197, 25, 39, 175, 64, 90, 14, 250, 191, 159, 10, 240, 42, 198, 213, 102, 69, 244, 16, 181, 148, 91, 179, 19, 95, 214, 177, 103, 124, 15, 4, 0, 245, 252, 219, 134, 44, 136, 173, 142, 61, 45, 251, 129, 247 };

        /// <summary>
        /// Global random number generator, used to generate seeds for the local random number generators
        /// Explicitly not for outside use, this would destroy the thread safetiness of this class!
        /// </summary>
        private static System.Random globalRandom = new System.Random();
        
        /// <summary>Thread local random number generator</summary>
        [ThreadStatic]
        private static System.Random threadLocalRandom;

        /// <summary>Thread local random number generator. Initializes the instance for this thread if necessary.</summary>
        private static System.Random ThreadLocalRandom
        {
            get
            {
                //copy it in local variable, accessing the ThreadStatic one multiple times is more costly
                System.Random instance = threadLocalRandom;
                if (instance == null)
                {
                    int seed;
                    lock (globalRandom) seed = globalRandom.Next();
                    instance = new System.Random(seed);
                }
                return instance;
            }
        }
        
        /// <summary>
        ///     Returns a nonnegative random number.
        /// </summary>
        ///<returns>
        ///     A 32-bit signed integer greater than or equal to zero and less than System.Int32.MaxValue.
        ///</returns>
        public static int Next() { return ThreadLocalRandom.Next(); }

        /// <summary>
        /// Returns a 50:50 boolean
        /// </summary>
        /// <returns></returns>
        public static bool NextBool()
        {
            return Next(0, 2) == 1;
        }

        /// <summary>
        ///     Returns a nonnegative random number less than the specified maximum.
        /// </summary>
        /// <param name="maxValue">
        ///     The exclusive upper bound of the random number to be generated. maxValue
        ///     must be greater than or equal to zero.
        ///</param>
        ///<returns>
        ///     A 32-bit signed integer greater than or equal to zero, and less than maxValue;
        ///     that is, the range of return values ordinarily includes zero but not maxValue.
        ///     However, if maxValue equals zero, maxValue is returned.
        ///</returns>
        ///<exception cref="System.ArgumentOutOfRangeException">
        ///     maxValue is less than zero.       
        ///</exception>
        public static int Next(int maxValue) { return ThreadLocalRandom.Next(maxValue); }

        /// <summary>
        ///     Returns a random number within a specified range.
        /// </summary>
        /// <param name="minValue">
        ///     The inclusive lower bound of the random number returned.
        ///</param>
        /// <param name="maxValue">
        ///     The exclusive upper bound of the random number returned
        ///</param>
        ///<returns>
        ///     A 32-bit signed integer greater than or equal to minValue and less than maxValue;
        ///     that is, the range of return values includes minValue but not maxValue. If
        ///     minValue equals maxValue, minValue is returned.
        ///</returns>
        ///<exception cref="System.ArgumentOutOfRangeException">
        ///     minValue is greater than maxValue.
        ///</exception>
        public static int Next(int minValue, int maxValue)
        {
            return ThreadLocalRandom.Next(Math.Min(minValue, maxValue), Math.Max(minValue, maxValue));
        }

        /// <summary>
        /// only creates a random number between intmin and intmax
        /// INTERVAL BETWEEN MIN AND MAX HAS TO BE BELOW INTMAX - INTMIN!!!
        /// </summary>
        /// <param name="minValue"></param>
        /// <param name="maxValue"></param>
        /// <returns></returns>
        public static long NextFakeLong(long minValue, long maxValue)
        {
            long lDiff = Math.Max(minValue, maxValue) - Math.Min(minValue, maxValue);
            if (lDiff > (long)int.MaxValue - (long)int.MinValue)
                throw new ArgumentOutOfRangeException();
            lDiff += int.MinValue;
            lDiff = Next(int.MinValue, (int)lDiff) - int.MinValue;
            return minValue + lDiff;
        }

        /// <summary>
        ///     Returns a float between 0 and 1
        /// </summary>
        ///<returns>
        ///     A float between 0 and 1
        ///</returns>
        public static float NextFloat()
        {
            return (float)NextDouble();
        }

        /// <summary>
        ///     Returns a float in the given interval.
        /// </summary>
        ///<returns>
        ///     A random float in the given interval.
        ///</returns>
        public static float NextFloat(float min, float max)
        {
            if (min > max) throw new ArgumentException();

            return (float)(NextDouble() * (max - min) + min);
        }

        /// <summary>
        ///     Returns a double between 0 and 1
        /// </summary>
        ///<returns>
        ///     A double between 0 and 1
        ///</returns>
        public static double NextDouble()
        {
            return ThreadLocalRandom.NextDouble();
        }

        /// <summary>
        /// Get a random item from the given list.
        /// </summary>
        /// <typeparam name="T">The type of the list</typeparam>
        /// <param name="list">The list to fetch a random item from</param>
        /// <returns>A random item from the list</returns>
        /// <exception cref="System.ArgumentOutOfRangeException">
        /// The length of the passed list is zero.
        /// </exception>
        public static T NextItem<T>(IList<T> list)
        {            
            return list[Next(list.Count)];
        }

        /// <summary>
        /// Project the given input byte deterministically and bijectively into [0, 255].
        /// Useful sometimes when all you need is a random distribution, but you don't
        /// care about proper pseudorandomness.
        /// </summary>
        /// <param name="input">The input byte to project.</param>
        /// <returns>The bijective, desterministic projection of the input byte</returns>
        public static byte Project(byte input) { return RANDOM_BYTES[input]; }

        /// <summary>
        /// Project the given pair of input bytes deterministically into [0, 255].
        /// Useful whenever you need a reproducable, pseudorandom projection.
        /// Typically you'll want to use the single-parameter version of this method, but sometimes you may
        /// want to create several, (largely) independant sets of deterministic, yet randomized bytes.
        /// </summary>
        /// <param name="input1">The input byte to project.</param>
        /// <param name="input2">A second input byte</param>
        /// <returns>The desterministic projection of the input bytes</returns>
        public static byte Project(byte input1, byte input2) { return RANDOM_BYTES[(input1 + RANDOM_BYTES[(input1 + input2) % 256]) % 256]; }

        /// <summary>
        /// Project the given quartet of input bytes deterministically into [0, 255].
        /// Useful whenever you need a reproducable, pseudorandom projection.
        /// Typically you'll want to use the single-parameter version of this method, but sometimes you may
        /// want to create several, (largely) independant sets of deterministic, yet randomized bytes.
        /// </summary>
        /// <param name="input1">The first input byte.</param>
        /// <param name="input2">The second input byte.</param>
        /// <param name="input2">The third input byte.</param>
        /// <param name="input2">The fourth input byte.</param>
        /// <returns>The desterministic projection of the input bytes</returns>
        public static byte Project(byte input1, byte input2, byte input3, byte input4) { return Project(Project(input1, input2), Project(input3, input4)); }

        /// <summary>
        /// Project the given pair of input ushorts deterministically into [0, 255].
        /// Useful whenever you need a reproducable, pseudorandom projection.
        /// </summary>
        /// <param name="input1">The first input ushort.</param>
        /// <param name="input2">The second input ushort.</param>
        /// <returns>The deterministic projection of the input bytes</returns>
        public static byte Project(ushort input1, ushort input2)
        {
            return Project((byte)(input1 >> 8), (byte)input1, (byte)(input2 >> 8), (byte)input2);
        }

        /// <summary>
        /// Project the given pair of input ushorts deterministically into [0, 255].
        /// Useful whenever you need a reproducable, pseudorandom projection.
        /// </summary>
        /// <param name="input1">The first input ushort.</param>
        /// <param name="input2">The second input ushort.</param>
        /// <param name="input3">The third input ushort.</param>
        /// <returns>The deterministic projection of the input bytes</returns>
        public static byte Project(ushort input1, ushort input2, ushort input3)
        {
            return Project(Project(input1, input2), input3);
        }

        /// <summary>
        /// Project the given integer input ushorts deterministically into [0, 255].
        /// Useful whenever you need a reproducable, pseudorandom projection.
        /// </summary>
        /// <param name="input">the input value</param>
        /// <returns>The deterministic projection of the input bytes</returns>
        /// 
        public static byte Project(int input)
        {
            //yes, we could just MOD the input down into [0, RANDOM_BYTES.Length[, but
            //that would leave us with a highly periodical result, which is undesirable if we e.g.
            //enumerate a 2D tile space into this integer to select some random, deterministic feature.
            //
            // Compare:
            //   for (int i=0; i<10000; i++) Console.Write((Project((byte)(i % 256)) & 1) == 0?'X':' ');
            // with the rewritten
            //   for (int i=0; i<10000; i++) Console.Write((Project(i) & 1) == 0'X':' ');
            //
            // The latter also has 

            return Project((ushort)(input >> 16), (ushort)input);
        }

        /// <summary>
        /// Project the given integer input ushorts deterministically into [0, 65535].
        /// Useful whenever you need a reproducable, pseudorandom projection.
        /// </summary>
        /// <param name="input">the input value</param>
        /// <returns>The deterministic projection of the input value</returns>
        public static ushort ProjectUInt16(int input)
        {
            byte b1 = (byte)input;
            byte b2 = (byte)(input >> 8);
            byte b3 = (byte)(input >> 16);
            byte b4 = (byte)(input >> 24);

            uint p1 = (uint)Project(b4, b3, b2, b1);
            uint p2 = (uint)Project(b3, b2, b1, b4);

            return (ushort)(p1<<8 | p2);
        }

        /// <summary>
        /// Project the given integer input ushorts deterministically into [0, 65535].
        /// Useful whenever you need a reproducable, pseudorandom projection.
        /// </summary>
        /// <param name="input">the input value</param>
        /// <returns>The deterministic projection of the input value</returns>
        public static int ProjectInt32(int input)
        {
            byte b1 = (byte)input;
            byte b2 = (byte)(input >> 8);
            byte b3 = (byte)(input >> 16);
            byte b4 = (byte)(input >> 24);

            uint p1 = Project(b1, b2, b3, b4);
            uint p2 = Project(b2, b3, b4, b1);
            uint p3 = Project(b3, b4, b1, b2);
            uint p4 = Project(b4, b1, b2, b3);

            return (int)(p1<<24 | p2<<16 | p3<<8 | p4);
        }
    }
}
