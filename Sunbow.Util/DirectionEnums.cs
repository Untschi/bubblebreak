﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunbow.Util
{
    public enum Direction4
    {
        East = 0,
        North = 1,
        West = 2,
        South = 3,
    }
}
