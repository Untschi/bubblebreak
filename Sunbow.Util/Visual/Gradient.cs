﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Sunbow.Util.Visual
{
    public struct GradientEntry : IComparable<GradientEntry>
    {
        public float Position { get { return position; } }
        float position;

        public Color Color { get { return color; } }
        Color color;

        public GradientEntry(float position, Color color)
        {
            if (position > 1 || position < 0)
                throw new Exception("the position of a GradientEntry must be between 0 and 1");

            this.position = position;
            this.color = color;
        }

        public int CompareTo(GradientEntry other)
        {
            return this.Position.CompareTo(other.Position);
        }
    }

    public class Gradient
    {
        List<GradientEntry> entries = new List<GradientEntry>();
        TextureAddressMode AddressMode { get; set; }


        public Gradient(Color start, Color end)
            : this(new GradientEntry(0, start), new GradientEntry(1, end))
        { }
        public Gradient(TextureAddressMode addressMode, Color start, Color end)
            : this(addressMode, new GradientEntry(0, start), new GradientEntry(1, end))
        { }
        public Gradient(params GradientEntry[] entries)
            : this(TextureAddressMode.Clamp, entries)
        { }
        public Gradient(TextureAddressMode addressMode, params GradientEntry[] entries)
        {
            this.AddressMode = addressMode;

            foreach (GradientEntry entry in entries)
                this.entries.Add(entry);

            this.entries.Sort();
        }

        public void AddEntry(GradientEntry entry)
        {
            this.entries.Add(entry);
            this.entries.Sort();
        }

        public void Clear()
        {
            entries.Clear();
        }

        public Color GetColorAt(float position)
        {
            if (entries.Count == 0)
                return Color.Magenta;

            if (position < 0 || position > 1)
            {
                switch (AddressMode)
                {
                    case TextureAddressMode.Clamp:

                        position = MathHelper.Clamp(position, 0, 1);

                        break;

                    case TextureAddressMode.Mirror:

                        while (position < 0)
                            position += 2;

                        position = 1 - Math.Abs(position % 2 - 1);

                        break;

                    case TextureAddressMode.Wrap:

                        while (position < 0)
                            position += 2;

                        position = position % 1;

                        break;
                }
            }

            int idx;
            for (idx = 0; idx < entries.Count - 1; idx++)
            {
                if (entries[idx].Position > position)
                    break;
            }

            GradientEntry left = (idx > 0) ? entries[idx - 1] : entries[idx];
            GradientEntry right = entries[idx];

            float range = right.Position - left.Position;
            float val = position - left.Position;

            return Color.Lerp(left.Color, right.Color, val / range);

        }
    }
}
