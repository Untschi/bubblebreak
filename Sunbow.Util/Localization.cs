﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.IO;

namespace Sunbow.Util
{
    public class Localization
    {
        public const string DEFAULT_LANGUAGE = "en-US";

        public Table Table { get { return table; } }
        Table table;

        public string Language
        {
            get { return language; }
            set
            {
                if (table.ContainsColumn(language) && !string.IsNullOrEmpty(language))
                    language = value;
            }
        }
        string language = DEFAULT_LANGUAGE;

        public string this[string stringID] 
        {
            get
            {
                if(table.ContainsRow(stringID))
                {
                    string result = table[language, stringID];
                    if(!string.IsNullOrEmpty(result))
                        return result.Replace('~', '\n');
                    else
                        return string.Format("[{0}]", stringID);
                }
                else
                {
                    table.AppendRow(stringID);
                    return string.Format("[{0}]", stringID);
                }
            }
        }

        public Localization(Table table)
        {
            this.table = table;

            this.language = System.Globalization.CultureInfo.CurrentUICulture.TextInfo.CultureName;
        }

    }
}
