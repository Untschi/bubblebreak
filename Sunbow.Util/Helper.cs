using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.IsolatedStorage;
using System.Security.Cryptography;
using System.Reflection;

namespace Sunbow.Util
{
    public static class Helper
    {
        public static IsolatedStorageFile GetUserStoreForAppDomain()
        {
#if WINDOWS
            return IsolatedStorageFile.GetUserStoreForDomain();
#else
            return IsolatedStorageFile.GetUserStoreForApplication();
#endif
        }

        public static string GenerateHash_SHA256(string input)
        {
            SHA256Managed hasher = new SHA256Managed();
            byte[] data = hasher.ComputeHash(Encoding.UTF8.GetBytes(input));

            StringBuilder sBuilder = new StringBuilder();

            for (int i = 0; i < data.Length; i++)
            {
                sBuilder.Append(data[i].ToString("x2"));
            }

            return sBuilder.ToString();
        }

        public static TEnum[] GetEnumValues<TEnum>()
        {

            Type enumType = typeof(TEnum);
            if (!enumType.IsEnum)
            {
                throw new ArgumentException("Type '" + enumType.Name + "' is not an enum");
            }
#if WP7
            IEnumerable<FieldInfo> fields = enumType.GetFields().Where(field => field.IsLiteral);
            return fields.Select(field => field.GetValue(enumType)).Select(value => (TEnum)value).ToArray();
#else
            return (Enum.GetValues(enumType) as TEnum[]);
#endif
        }
    }
}
