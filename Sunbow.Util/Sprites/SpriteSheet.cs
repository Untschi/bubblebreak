﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Sunbow.Util.Sprites
{
    public class SpriteSheet
    {
        Dictionary<string, FrameSet> frameSets = new Dictionary<string, FrameSet>();

        public Texture2D Texture { get { return texture; } }
        Texture2D texture;

        public SpriteSheet(Texture2D texture)
        {
            this.texture = texture;
        }

        public void RegisterFrameSet(FrameSet set)
        {
            if (frameSets.ContainsKey(set.Name))
                throw new ArgumentException("A set with the same name has already been added");

            frameSets.Add(set.Name, set);
        }

        public FrameAnimation RegisterAnimation(string frameSetName, FrameAnimation animation)
        {
            return frameSets[frameSetName].RegisterAnimation(animation);
        }

        public Frame RegisterFrame(string frameSetName, string frameName, int frameIndex)
        {
            return frameSets[frameSetName].RegisterFrame(frameName, frameIndex);
        }
        public Frame RegisterFrame(string frameSetName, string frameName, Frame frame)
        {
            return frameSets[frameSetName].RegisterFrame(frameName, frame);
        }

        public FrameAnimation CreateAnimation(string frameSetName, string animationName)
        {
            return frameSets[frameSetName].CreateAnimation(animationName);
        }

        public Frame CalculateFrame(string frameSetName, int frameIndex)
        {
            return frameSets[frameSetName].CalculateFrame(frameIndex);
        }

        public Frame GetFrame(string frameSetName, string frameName)
        {
            return frameSets[frameSetName].GetFrame(frameName);
        }

        public FrameSet GetFrameSet(string frameSetName)
        {
            return frameSets[frameSetName];
        }
        public void Draw(SpriteBatch batch, Frame frame, Vector2 position, float rotation, float scale)
        {
            Draw(batch, frame, position, rotation, new Vector2(scale), Color.White, SpriteEffects.None, 0);
        }
        public void Draw(SpriteBatch batch, Frame frame, Vector2 position, float rotation, Vector2 scale)
        {
            Draw(batch, frame, position, rotation, scale, Color.White, SpriteEffects.None, 0);
        }

        public void Draw(SpriteBatch batch, Frame frame, Vector2 position, float rotation, float scale, Color color)
        {
            Draw(batch, frame, position, rotation, new Vector2(scale), color, SpriteEffects.None, 0);
        }
        public void Draw(SpriteBatch batch, Frame frame, Vector2 position, float rotation, Vector2 scale, Color color)
        {
            Draw(batch, frame, position, rotation, scale, color, SpriteEffects.None, 0);
        }
        public void Draw(SpriteBatch batch, Frame frame, Vector2 position, float rotation, float scale, Color color, SpriteEffects effects, float layerDepth)
        {
            batch.Draw(texture, position, frame.SourceRectangle, color, rotation, frame.Origin, scale, effects, layerDepth);
        }
        public void Draw(SpriteBatch batch, Frame frame, Vector2 position, float rotation, Vector2 scale, Color color, SpriteEffects effects, float layerDepth)
        {
            batch.Draw(texture, position, frame.SourceRectangle, color, rotation, frame.Origin, scale, effects, layerDepth);
        }
    }
}
