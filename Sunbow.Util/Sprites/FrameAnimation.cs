using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunbow.Util.Sprites
{
    public enum RepeatLogic
    {
        Looped = 0,
        PlayOnce = 1,
    }

    public class FrameAnimation
    {
        string name;
        protected int startIndex, currentIndex, frameCount;
        Frame currentFrame;
        float frameSwitchDuration;
        float elapsedTime;
        RepeatLogic repeatLogic;

        FrameSet frameSet;
        public FrameSet FrameSet { get { return frameSet; } internal set { frameSet = value; } }

        public event Action<FrameAnimation> AnimationEnded;

        public string Name { get { return name; } }
        public virtual int CurrentIndex { get { return currentIndex; } set { elapsedTime = value * frameSwitchDuration + float.Epsilon; } }

        public Frame CurrentFrame { get { return currentFrame; } internal set { currentFrame = value; } }

        protected FrameAnimation()
        { }
        public FrameAnimation(string name, int startFrameIndex, int frameCount, float frameSwitchDuration, RepeatLogic repeatLogic)
        {
            this.name = name;
            this.frameSwitchDuration = frameSwitchDuration;
            this.frameCount = frameCount;
            this.repeatLogic = repeatLogic;
            this.startIndex = startFrameIndex;
            this.currentIndex = -1;
            
        }


        public void Update(float seconds)
        {
            frameSet.Update(seconds, this);
        }

        internal void InternalUpdate(float seconds, out bool frameChanged)
        {
            frameChanged = false;
            elapsedTime += seconds;

            int frame = (int)(elapsedTime / frameSwitchDuration);

            if (startIndex + frame == currentIndex)
            {
                return;
            }

            frameChanged = true;
            if (frame >= frameCount)
            {
                switch (repeatLogic)
                {
                    case RepeatLogic.Looped:
                        frame = frame % frameCount;
                        break;

                    case RepeatLogic.PlayOnce:
                        frame = frameCount - 1;
                        frameChanged = false;
                        if (AnimationEnded != null)
                            AnimationEnded(this);
                        break;

                    default: throw new NotImplementedException();
                }
            }

            currentIndex = startIndex + frame;
        }

        public void Reset()
        {
            elapsedTime = 0;
            Update(0);
        }

        public virtual FrameAnimation GetCopy()
        {
            FrameAnimation copy = new FrameAnimation();
            FillCopyValues(copy);
            return copy;
        }

        protected void FillCopyValues(FrameAnimation copy)
        {
            copy.currentIndex = -1;
            copy.elapsedTime = 0;
            copy.frameCount = frameCount;
            copy.frameSwitchDuration = frameSwitchDuration;
            copy.name = name;
            copy.repeatLogic = repeatLogic;
            copy.startIndex = startIndex;
            copy.frameSet = frameSet;
        }
    }


    public class MappedFrameAnimation : FrameAnimation
    {
        int[] frames;
        public override int CurrentIndex { get { return frames[currentIndex]; } }

        private MappedFrameAnimation()
            : base()
        { }

        public MappedFrameAnimation(string name, int[] frames, float frameSwitchDuration, RepeatLogic repeatLogic)
            : base(name, 0, frames.Length, frameSwitchDuration, repeatLogic)
        {
            this.frames = frames;
        }

        public override FrameAnimation GetCopy()
        {
            MappedFrameAnimation copy = new MappedFrameAnimation();
            FillCopyValues(copy);
            copy.frames = frames;
            return copy;
        }
    }
}
