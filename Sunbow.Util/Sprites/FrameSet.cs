﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Sunbow.Util.Sprites
{
    public enum ReadDirection
    {
        LeftToRight_NextLineBelow = 0,
        UpToDown_NextLineLeft = 1,
    }

    public abstract class FrameSet
    {
        string name;
        public string Name { get { return name; } }

        Dictionary<string, FrameAnimation> animationPrefabs = new Dictionary<string,FrameAnimation>();
        Dictionary<string, Frame> namedFrames = new Dictionary<string, Frame>(); 

        protected FrameSet(string name)
        {
            this.name = name;
        }

        internal void Update(float seconds, FrameAnimation animation)
        {
            bool frameChanged;
            animation.InternalUpdate(seconds, out frameChanged);

            if (frameChanged)
                animation.CurrentFrame = CalculateFrame(animation.CurrentIndex);
        }

        public FrameAnimation CreateAnimation(string animationName)
        {
            if (!animationPrefabs.ContainsKey(animationName))
                throw new ArgumentException("the animation with the name \"" + animationName + "\" is not registered.");

            FrameAnimation ani = animationPrefabs[animationName].GetCopy();
            ani.Reset();
            //.CurrentFrame = CalculateFrame(ani.CurrentIndex);
            return ani;
        }

        public abstract Frame CalculateFrame(int index);

        public FrameAnimation RegisterAnimation(FrameAnimation animation)
        {
            if (animationPrefabs.ContainsKey(animation.Name))
                throw new ArgumentException("An animation with the name \"" + animation.Name + "\" has already been registered.");

            animation.FrameSet = this;
            animationPrefabs.Add(animation.Name, animation);

            return animation;
        }

        public Frame RegisterFrame(string name, int frameIndex)
        {
            return RegisterFrame(name, CalculateFrame(frameIndex));
        }

        public Frame RegisterFrame(string name, Frame frame)
        {
            if (namedFrames.ContainsKey(name))
                throw new ArgumentException("A frame with the name \"" + name + "\" has already been registered.");

            namedFrames.Add(name, frame);
            return frame;
        }


        public Frame GetFrame(string name)
        {
            if (!namedFrames.ContainsKey(name))
                throw new ArgumentException("The frame with the name \"" + name + "\" is not registered.");

            return namedFrames[name];
        }
    }

    public class FixedFrameSet : FrameSet
    {
        // TODO: Implement direction
        ReadDirection direction;

        public int FrameWidth { get { return length; } }
        public int FrameHeight { get { return depth; } }
        int length, depth;
        int spaceLength, spaceDepth;
        Point offset;
        int framesPerLine;

        int firstFrame, frameCount;
        public Vector2 FrameOrigin { get { return frameOrigin; } set { frameOrigin = value; } }
        Vector2 frameOrigin;

        public FixedFrameSet(string name, int length, int depth, Point offset, int frameCount, ReadDirection direction)
            : this(name, length, depth, 0, 0, offset, frameCount, 0, frameCount, direction, Vector2.Zero)
        { }

        public FixedFrameSet(string name, int length, int depth, Point offset, int framesPerLine, int firstFrameIndex, int frameCount, ReadDirection direction)
            : this(name, length, depth, 0, 0, offset, framesPerLine, firstFrameIndex, frameCount, direction, Vector2.Zero)
        { }

        public FixedFrameSet(
            string name, int length, int depth, int spaceLength, int spaceDepth,
            Point offset, int framesPerLine, int firstFrameIndex, int frameCount,
            ReadDirection direction, Vector2 frameOrigin)
            : base(name)
        {
            this.length = length;
            this.depth = depth;
            this.spaceLength = spaceLength;
            this.spaceDepth = spaceDepth;
            this.firstFrame = firstFrameIndex;
            this.frameCount = frameCount;
            this.direction = direction;
            this.frameOrigin = frameOrigin;
            this.offset = offset;
            this.framesPerLine = framesPerLine;
        }

        public override Frame CalculateFrame(int index)
        {
            int idx = firstFrame + index % frameCount;
            int xIdx = idx % framesPerLine;
            int yIdx = (idx - xIdx) / framesPerLine;

            int x = offset.X + xIdx * (length + spaceLength);
            int y = offset.Y + yIdx * (depth + spaceDepth);

            Rectangle rect = new Rectangle(x, y, length, depth);
            return new Frame(rect, frameOrigin);
        }

    }

    public class FreeFrameSet : FrameSet
    {
        Frame[] frames;

        public FreeFrameSet(string name, Frame[] frames)
            : base(name)
        {
            this.frames = frames;
        }

        public FreeFrameSet(string name)
            : base(name)
        {
            this.frames = new Frame[] { };
        }

        public override Frame CalculateFrame(int index)
        {
            return frames[index % frames.Length];
        }
    }
}
