using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Sunbow.Util.Sprites
{
    public class Frame
    {
        Rectangle rectangle;
        Vector2 origin;

        public Rectangle SourceRectangle { get { return rectangle; } internal set { rectangle = value; } }
        public Vector2 Origin { get { return origin; } internal set { origin = value; } }

        public Frame(Rectangle rectangle, Vector2 origin)
        {
            this.rectangle = rectangle;
            this.origin = origin;
        }
    }
}
