﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Sprites;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Bubblebreak.SplashScreen
{
    public class SunbowSplashScreen
    {
        const int RAY_COUNT = 9;
        const float RAY_ANLGLE_OFFSET = MathHelper.Pi / (RAY_COUNT - 1);
        const int VERTICAL_TEXT_OFFSET = 4;
        const float ROTATION_SPEED = 0.2f;

        protected SpriteSheet splashSheet;
        public Vector2 SunPosition, PalmPosition;
        public float SunScale;

        protected Color backgroundColor, foregroundColor;

        Frame sun, reflection, ray, line, background, sunbowText, gamesText, palmTop, palmBottom;
        Texture2D texture;
        float rotation;
        
        public SunbowSplashScreen()
        {
            texture = BubbleBreak.Instance.Content.Load<Texture2D>("logo_sprite_sheet");
            splashSheet = new SpriteSheet(texture);
            splashSheet.RegisterFrameSet(new FreeFrameSet("default"));
            sun = splashSheet.RegisterFrame("default", "sun", new Frame(new Rectangle(0, 0, 207, 104), new Vector2(103, 104)));
            reflection = splashSheet.RegisterFrame("default", "reflection", new Frame(new Rectangle(0, 104, 480, 52), new Vector2(240, 0)));
            ray = splashSheet.RegisterFrame("default", "ray", new Frame(new Rectangle(208, 0, 84, 10), new Vector2(-132, 5)));
            line = splashSheet.RegisterFrame("default", "line", new Frame(new Rectangle(210, 11, 80, 2), new Vector2(0, 1)));
            background = splashSheet.RegisterFrame("default", "background", new Frame(new Rectangle(210, 14, 80, 48), new Vector2(0, 0)));
            sunbowText = splashSheet.RegisterFrame("default", "sunbow_text", new Frame(new Rectangle(293, 0, 146, 26), new Vector2(73, 26)));
            gamesText = splashSheet.RegisterFrame("default", "games_text", new Frame(new Rectangle(293, 26, 83, 20), new Vector2(41, 0)));
            palmTop = splashSheet.RegisterFrame("default", "palm_top", new Frame(new Rectangle(0, 156, 193, 142), new Vector2(0, 315)));
            palmBottom = splashSheet.RegisterFrame("default", "palm_bottom", new Frame(new Rectangle(0, 298, 193, 173), new Vector2(0, 173)));

            backgroundColor = Color.Red;
            foregroundColor = Color.Yellow;

            SunPosition = new Vector2(492, 240);
            PalmPosition = new Vector2(5, 480);
        }

        public virtual void Update(float seconds)
        {
            rotation += ROTATION_SPEED * seconds;
            if (rotation > RAY_ANLGLE_OFFSET)
                rotation -= RAY_ANLGLE_OFFSET;
        }

        public virtual void Draw(SpriteBatch batch)
        {
            // 1. background
            batch.Draw(texture, new Vector2(0, 0), background.SourceRectangle, backgroundColor, 0, background.Origin, new Vector2(10, 10), SpriteEffects.None, .0f);

            // 2. rays
            for (int i = -1; i < RAY_COUNT; i++)
            {
                float angle = -rotation - i * RAY_ANLGLE_OFFSET;
                batch.Draw(texture, SunPosition, ray.SourceRectangle, foregroundColor, angle, ray.Origin, SunScale, SpriteEffects.None, .1f);
            }

            // 3. foreground
            batch.Draw(texture, new Vector2(0, SunPosition.Y), background.SourceRectangle, foregroundColor, 0, background.Origin, new Vector2(10, 5), SpriteEffects.None, .2f);

            // 4. sun and reflection
            batch.Draw(texture, SunPosition, sun.SourceRectangle, foregroundColor, 0, sun.Origin, SunScale, SpriteEffects.None, .3f);
            batch.Draw(texture, SunPosition, reflection.SourceRectangle, backgroundColor, 0, reflection.Origin, SunScale, SpriteEffects.None, .3f);

            // 5. logo text
            batch.Draw(texture, new Vector2(SunPosition.X, SunPosition.Y - VERTICAL_TEXT_OFFSET), sunbowText.SourceRectangle, backgroundColor, 0, sunbowText.Origin, SunScale, SpriteEffects.None, 0.4f);
            batch.Draw(texture, new Vector2(SunPosition.X, SunPosition.Y + VERTICAL_TEXT_OFFSET), gamesText.SourceRectangle, foregroundColor, 0, gamesText.Origin, SunScale, SpriteEffects.None, 0.4f);

            // 6. line
            batch.Draw(texture, new Vector2(0, SunPosition.Y), line.SourceRectangle, Color.Black, 0, line.Origin, new Vector2(10, 1), SpriteEffects.None, .5f);

            // 7. palm
            batch.Draw(texture, PalmPosition, palmBottom.SourceRectangle, backgroundColor, 0, palmBottom.Origin, 1, SpriteEffects.None, 1f);
            batch.Draw(texture, PalmPosition, palmTop.SourceRectangle, foregroundColor, 0, palmTop.Origin, 1, SpriteEffects.None, 1f);
        }
    }
}
