﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Bubblebreak
{
    public class AnimatedSprite
    {
        Texture2D _Texture;
        public Texture2D Texture { get { return _Texture; } }
        Rectangle _SourceRect;
        public Rectangle SourceRect { get { return _SourceRect; } }

        int _TileIndex;
        float _TimeStep, _LeftTime;
        bool _Horizontal;

        public AnimatedSprite(Texture2D texture, int partSize, float timeStep, bool horizontalCells)
        {
            _Texture = texture;

            if(horizontalCells)
                _SourceRect = new Rectangle(0, 0, partSize, texture.Height);
            else
                _SourceRect = new Rectangle(0, 0, texture.Width, partSize);

            _TimeStep = timeStep;
            _LeftTime = _TimeStep;
            _Horizontal = horizontalCells;
        }


        public void Update(float seconds)
        {
            _LeftTime -= seconds;

            if (_LeftTime <= 0)
            {
                _TileIndex++;

                if (_Horizontal)
                {
                    if (_TileIndex * _SourceRect.Width >= _Texture.Width)
                        _TileIndex = 0;

                    _SourceRect = new Rectangle(_TileIndex * _SourceRect.Width, 0, _SourceRect.Width, _Texture.Height);
                }
                else
                {
                    if (_TileIndex * _SourceRect.Height >= _Texture.Height)
                        _TileIndex = 0;

                    _SourceRect = new Rectangle(0, _TileIndex * _SourceRect.Height, _Texture.Width, _SourceRect.Height);

                }
                _LeftTime = _TimeStep;
            }
        }
    }
}
