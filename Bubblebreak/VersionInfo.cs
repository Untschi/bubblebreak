﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Highscore;

namespace Bubblebreak
{
    public static class VersionInfo
    {
        
        public const string Version = "0.9";
        public const ProductInfo ProductReleaseInfo = ProductInfo.iPhone_sell;

    }
}
