using System;
using Microsoft.Xna.Framework.GamerServices;

namespace Bubblebreak
{
    static class Program
    {
#if WINDOWS
        public static bool TrialModeSimulation { get; private set; }
#endif
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
#if WINDOWS
            if (args.Length > 0 && args[0] == "-trial")
                TrialModeSimulation = true;
#endif
            using (BubbleBreak game = new BubbleBreak())
            {

                game.Run();
            }
        }
    }
}

