﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bubblebreak.GameState;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using starLiGHT.GUI;
using starLiGHT.GUI.Widgets;
using Bubblebreak.Menu;
using Microsoft.Xna.Framework.Input;
using Sunbow.Util;

namespace Bubblebreak.Modes
{
    public class MainMenu : Mode
    {
        GuiManager guiManager;

        public ModeProxy MenuProxy { get { return menuProxy; } }
        ModeProxy menuProxy;

        public Type EnterMode { get { return enterMode; } set { enterMode = value; } }
        Type enterMode = typeof(MainMenuMode);

        ColorChanger bgColor;

        ObjectPool<Ball> backgroundSmilies;
        ObjectPool<Ball> foregroundSmilies;

        public MainMenu()
        {
            guiManager = new GuiManager(BubbleBreak.Instance, @"Content\GuiSkin.xml");
            guiManager.LoadSkin();

            bgColor = new ColorChanger(2, 0.2f, 0.2f);

            backgroundSmilies = new ObjectPool<Ball>(BackgroundSmilieFactory);
            foregroundSmilies = new ObjectPool<Ball>(ForegroundSmilieFactory);
        }

        public void RegisterAllMenus()
        {
            menuProxy = new ModeProxy(Game);
            menuProxy.Register(new MainMenuMode(guiManager));
            menuProxy.Register(new HighscoreMenuMode(guiManager));
            menuProxy.Register(new HelpMenuMode(guiManager));
            menuProxy.Register(new GameOverNameBoxMode(guiManager));
            menuProxy.Register(new CreditsMenuMode(guiManager));
            menuProxy.Register(new SettingsMenuMode(guiManager));
            menuProxy.Register(new PauseMenuMode(guiManager));
            menuProxy.Register(new TrialExitScreen(guiManager));
        }

        protected internal override void Enter()
        {
            menuProxy.SetMode(enterMode);

#if WINDOWS
            Game.IsMouseVisible = true;
#endif
        }

        protected internal override void Leave()
        {

#if WINDOWS
            Game.IsMouseVisible = false;
#endif
        }

        protected internal override void Update(GameTime gameTime, InputState inputState)
        {
            if(!MenuProxy.IsActiveMode<GameOverNameBoxMode>())
                UpdateSmilies(gameTime.GetElapsedSeconds());
            //float seconds = (float)gameTime.ElapsedGameTime.TotalSeconds;
            //_MenuManager.Update(seconds);
#if DEBUG && WINDOWS
            if (inputState != null && 
                inputState.LastKeyboardStates[0].IsKeyDown(Keys.S) && inputState.CurrentKeyboardStates[0].IsKeyDown(Keys.S))
            {
                BubbleBreak.Instance.Loc.Table.Save("loc.csv");
            }
#endif
            bgColor.Update(gameTime.GetElapsedSeconds());
            menuProxy.Update(gameTime);

            guiManager.Update(gameTime);
        }

        private void UpdateSmilies(float seconds)
        {
            if (BubbleBreak.Random.Next(66) == 0)
            {
                if (BubbleBreak.Random.Next(3) == 0)
                    CreateBall(foregroundSmilies, Ball.ColorlessMovementSpeed);
                else
                    CreateBall(backgroundSmilies, Ball.ColoredMovementSpeed);
            }

            foreach (Ball b in backgroundSmilies)
            {
                b.Update(seconds);

                if (!IsInBounds(b.Position))
                    backgroundSmilies.Deactivate(b);
            }
            foreach (Ball b in foregroundSmilies)
            {
                b.Update(seconds);

                if (!IsInBounds(b.Position))
                    foregroundSmilies.Deactivate(b);
            }
        }

        protected internal override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

            bool draw =!menuProxy.IsActiveMode<Menu.GameOverNameBoxMode>();
            if (draw)
            {
                GraphicsDevice.Clear(bgColor.CurrentColor);

                spriteBatch.Begin();
                foreach (Ball b in backgroundSmilies)
                    b.DrawAsMenuBackground(spriteBatch, 1, 1);
                spriteBatch.End();
            }


            menuProxy.Draw(gameTime, spriteBatch);

            guiManager.Draw(gameTime);

            if (draw)
            {
                spriteBatch.Begin();
                foreach (Ball b in foregroundSmilies)
                    b.DrawAsMenuBackground(spriteBatch, 1, 1);
                spriteBatch.End();
            }
        }


        private Ball BackgroundSmilieFactory()
        {
            return new Ball(Achievements.AchievementType.undefined, true)
            {
                State = BallState.MovingColorBall,
            };
        }
        private Ball ForegroundSmilieFactory()
        {
            return new Ball(Achievements.AchievementType.undefined, true)
            {
                State = BallState.ColorlessBall,
            };
        }

        private Ball CreateBall(ObjectPool<Ball> pool, float velocity)
        {
            const int NEGATIVE_VALUE = -50;
            const int WIDTH = 800 + 50;
            const int HEIGHT = 480 + 50;

            Ball b = pool.ActivateNext();
            Direction4 side = (Direction4)BubbleBreak.Random.Next(4);

            int x = NEGATIVE_VALUE;
            int y = NEGATIVE_VALUE;
            float velX = (float)BubbleBreak.Random.NextDouble();
            float velY = (float)BubbleBreak.Random.NextDouble();

            switch (side)
            {
                case Direction4.North:
                    x = BubbleBreak.Random.Next(NEGATIVE_VALUE, WIDTH);
                    y = NEGATIVE_VALUE;
                    velX -= 0.5f;
                    System.Diagnostics.Debug.WriteLine("North");
                    break;
                case Direction4.South:
                    x = BubbleBreak.Random.Next(NEGATIVE_VALUE, WIDTH);
                    y = HEIGHT;
                    velX -= 0.5f;
                    velY = -velY;
                    System.Diagnostics.Debug.WriteLine("South");
                    break;
                case Direction4.West:
                    x = NEGATIVE_VALUE;
                    y = BubbleBreak.Random.Next(NEGATIVE_VALUE, HEIGHT);
                    velY -= 0.5f;
                    System.Diagnostics.Debug.WriteLine("West");
                    break;
                case Direction4.East:
                    x = WIDTH;
                    y = BubbleBreak.Random.Next(NEGATIVE_VALUE, HEIGHT);
                    velY -= 0.5f;
                    velX = -velX;
                    System.Diagnostics.Debug.WriteLine("East");
                    break;
            }
            b.Position = new Vector2(x, y);
            b.Velocity = velocity * Vector2.Normalize(new Vector2(velX, velY));
            b.Rotation = (float)Math.Atan2(b.Velocity.Y, b.Velocity.X) + MathHelper.PiOver2;
            b.Color = (pool == backgroundSmilies)
                ? ColorField.GetColor(BubbleBreak.Random.Next(ColorField.MaxColors))
                : Color.White * (1 - Ball.BounceAlphaDecrease);
            b.Smilie.RandomizeFace();

            return b;
        }

        private bool IsInBounds(Vector2 pos)
        {
            const int NEGATIVE_VALUE = -100;
            const int WIDTH = 800 + 100;
            const int HEIGHT = 480 + 100;

            return pos.X > NEGATIVE_VALUE && pos.X < WIDTH && pos.Y > NEGATIVE_VALUE && pos.Y < HEIGHT;

        }
    }
}
