﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bubblebreak.GameState;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input.Touch;
using Sunbow.Input.Touch;
using Shapes.Geometry;
using Shapes.Misc;

namespace Bubblebreak.Modes
{
    public class Game : Mode
    {
        LevelManager _LevelManager;
        public bool InitOnEnter { get; set; }

        MultiTouchManager touchManager;
        TouchShape touchCircle, shootCircle, colorWheel, lockTop, lockBottom;

#if DEBUG
        TextureGenerator generator;
        Texture2D circleTex, shootCircleTex, wheelTex, lockTopTex, lockBottomTex;
#endif

        public Game()
        {
           
        }

        void touchManager_GestureActionCallback(TouchZone zone, GestureSample sample)
        {
        }

        protected internal override void Enter()
        {
            if (InitOnEnter)
            {

                touchManager = new MultiTouchManager();
                touchManager.GestureActionCallback += touchManager_GestureActionCallback;

                touchCircle = new TouchShape(touchManager, new Ellipse(94, 94));
                touchCircle.Shape.Center = (BubbleBreak.PlayingFieldFlipped) ? new Vector2(708, 388) : new Vector2(90, 388);
                touchCircle.EnabledGestures = GestureType.FreeDrag;

                shootCircle = new TouchShape(touchManager, new Ellipse(Level.ActualCenter, 225, 225));
                shootCircle.EnabledGestures = GestureType.Tap;

                ShapeComposition wheelShape = new ShapeComposition(new Ellipse(Level.ActualCenter, 300, 300));
                wheelShape.Add(new Ellipse(Level.ActualCenter, 225, 225), ShapeComposeFunction.Subtract);

                Rect hideRect = new Rect(Level.ActualCenter.X + 150, 480);
                if (BubbleBreak.PlayingFieldFlipped)
                    hideRect.Position = new Vector2(Level.ActualCenter.X - 150, 0);

                wheelShape.Add(hideRect, ShapeComposeFunction.Subtract);

                colorWheel = new TouchShape(touchManager, wheelShape);
                colorWheel.EnabledGestures = GestureType.FreeDrag | GestureType.DragComplete | GestureType.DoubleTap;//| GestureType.Flick;

                //lockTop = new TouchShape(touchManager, new Triangle(new Vector2(700, 145), new Vector2(625, 20), new Vector2(700, 20)));
                lockTop = new TouchShape(touchManager, new Triangle(
                    BubbleBreak.Transform(new Vector2(270, 72), BubbleBreak.PlayingFieldFlipped, 0),
                    BubbleBreak.Transform(new Vector2(232, 10), BubbleBreak.PlayingFieldFlipped, 0),
                    BubbleBreak.Transform(new Vector2(270, 10), BubbleBreak.PlayingFieldFlipped, 0)));
                lockTop.EnabledGestures = GestureType.Tap;

                lockBottom = new TouchShape(touchManager, new Triangle(
                    BubbleBreak.Transform(new Vector2(270, 165), BubbleBreak.PlayingFieldFlipped, 0),
                    BubbleBreak.Transform(new Vector2(232, 227), BubbleBreak.PlayingFieldFlipped, 0),
                    BubbleBreak.Transform(new Vector2(270, 227), BubbleBreak.PlayingFieldFlipped, 0)));
                lockBottom.EnabledGestures = GestureType.Tap;
#if DEBUG
                generator = new TextureGenerator(GraphicsDevice);
                circleTex = generator.GeometryFilled(touchCircle.Shape, Color.Green * 0.4f);
                shootCircleTex = generator.GeometryFilled(shootCircle.Shape, Color.Red * 0.3f);
                wheelTex = generator.GeometryFilled(colorWheel.Shape, Color.Blue * 0.4f);
                lockTopTex = generator.GeometryFilled(lockTop.Shape, Color.Yellow * 0.4f);
                lockBottomTex = generator.GeometryFilled(lockBottom.Shape, Color.Yellow * 0.4f);
#endif

                if (_LevelManager != null)
                    _LevelManager.Delete();

                _LevelManager = new LevelManager();
            }
            _LevelManager.Sleep = false;
        }

        protected internal override void Leave()
        {
            _LevelManager.Sleep = true;
        }

        protected internal override void Update(GameTime gameTime, InputState inputState)
        {
            float seconds = (float)gameTime.ElapsedGameTime.TotalSeconds;
            _LevelManager.Update(seconds);
        }

        protected internal override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            GraphicsDevice.Clear(Color.Black);

            spriteBatch.Begin();

            // TODO: Draw particles behind
            _LevelManager.Draw(spriteBatch);
            // TODO: Draw particles at front

#if DEBUG
            spriteBatch.Draw(circleTex, touchCircle.Shape.Position, Color.White);
            spriteBatch.Draw(shootCircleTex, shootCircle.Shape.Position, Color.White);
            spriteBatch.Draw(lockTopTex, lockTop.Shape.Position, Color.White);
            spriteBatch.Draw(lockBottomTex, lockBottom.Shape.Position, Color.White);

            spriteBatch.Draw(wheelTex, colorWheel.Shape.Position, null, Color.White, 0, colorWheel.Shape.Origin, colorWheel.Shape.Scale, SpriteEffects.None, 0);
#endif

            spriteBatch.End();
        }
    }
}
