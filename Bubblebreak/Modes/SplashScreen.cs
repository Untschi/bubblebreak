﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bubblebreak.GameState;
using Bubblebreak.SplashScreen;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Highscore;
using Sunbow.Util.Manipulation;

namespace Bubblebreak.Modes
{
    public class SplashScreen : Mode
    {
        const float Duration = 2;

        SunbowSplashScreen screen;

        float timer = 0;
        bool loaded = false;

        CurveSingleAnimator sunMove, sunScale, palmMove;

        protected internal override void Enter()
        {
            screen = new SunbowSplashScreen();


            sunMove = new CurveSingleAnimator(BubbleBreak.Instance.Content.Load<Curve>(@"Curves\sun_move"), Duration, AnimationRepeatLogic.Pause);
            sunScale = new CurveSingleAnimator(BubbleBreak.Instance.Content.Load<Curve>(@"Curves\sun_scale"), Duration, AnimationRepeatLogic.Pause);
            palmMove = new CurveSingleAnimator(BubbleBreak.Instance.Content.Load<Curve>(@"Curves\palm_move"), Duration, AnimationRepeatLogic.Pause);

#if !COMPACT
            foreach (var c in System.Globalization.CultureInfo.GetCultures(System.Globalization.CultureTypes.AllCultures))
            {
                Console.WriteLine(string.Format("{0} \t {1}", c.EnglishName, c.TextInfo.CultureName));
            }
#endif
            // TEST
            //ScoreRequester send = new ScoreRequester(
            //    "http://www.liquidfirearts.com/sunbow/bubblebreak/highscore.php",
            //    new ScoreQuery("jeret", 54, ProductInfo.WP7_free, "0.1"));
            //send.RequestSucceeded += new Action<WebRequester>(send_RequestSucceeded);
            //send.CreateWebRequest();
            //LocalHighscore localScore = new LocalHighscore(@"high.score");
            //for (int i = 0; i < 20; i++)
            //{
            //    localScore.AddNewScore("JeReT", Sunbow.Util.Random.Next(10000));
            //    foreach (string[] data in localScore.GetScoreIterator())
            //    {
            //        Console.WriteLine("{3} {0}. {1}\t\t{2}", data[0], data[1], data[2], (data[0] == localScore.PlayerRank) ? ">" : " ");
            //    }
            //    Console.WriteLine();
            //    Console.WriteLine("---");
            //    Console.WriteLine();
            //}
            //localScore.ScoreTable.Save();

        }

        //void send_RequestSucceeded(WebRequester obj)
        //{
        //    ScoreRequester requester = obj as ScoreRequester;
        //    // Test
        //    foreach (string key in requester.ScoreLists.Keys)
        //    {
        //        Console.WriteLine();
        //        Console.WriteLine(key + ":");

        //        foreach (string[] data in requester.ScoreLists[key].GetScoreIterator())
        //        {
        //            Console.WriteLine("{3} {0}. {1}\t\t{2}", data[0], data[1], data[2], (data[0] == requester.ScoreLists[key].PlayerRank) ? ">" : " ");
        //        }
        //    }
        //}

        protected internal override void Leave()
        {
            
        }
        protected internal override void Update(GameTime gameTime, InputState inputState)
        {
            float seconds = (float)gameTime.ElapsedGameTime.TotalSeconds;

            sunMove.Update(seconds);
            sunScale.Update(seconds);
            palmMove.Update(seconds);

            screen.PalmPosition = new Vector2(800 * palmMove, screen.PalmPosition.Y);
            screen.SunPosition = new Vector2(800 * sunMove, screen.SunPosition.Y);
            screen.SunScale = sunScale;
            
            screen.Update(seconds);


            timer += seconds;

            if (!loaded && timer >= 0.5f * Duration)
            {
                BubbleBreak.Instance.LoadEverything();
                loaded = true;
            }

            if(timer >= Duration)
                Proxy.SetMode<MainMenu>();
        }

        protected internal override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            GraphicsDevice.Clear(Color.Black);
            spriteBatch.Begin();
            screen.Draw(spriteBatch);
            spriteBatch.End();
        }
    }
}
