﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Bubblebreak
{
    public class ColorChanger
    {
        Color _CurrentColor;
        public Color CurrentColor { get { return _CurrentColor; } }

        Color _PreviousColor;
        Color _TargetColor;

        readonly float _ByteChangeDuration, _ColorLightness, _ColorScale;
        float _LerpAmount, _LerpSpeed;


        public ColorChanger(float durationBlackToWhite, float colorLightness)
            : this(durationBlackToWhite, colorLightness, 1)
        { }
        public ColorChanger(float durationBlackToWhite, float colorLightness, float colorScale)
        {
            _ByteChangeDuration = durationBlackToWhite / (255);

            _ColorLightness = colorLightness;
            _CurrentColor = GetRandomColorLight(_ColorLightness, _ColorScale);
            _ColorScale = colorScale;

            NextTargetColor();
        }


        private void NextTargetColor()
        {
            _PreviousColor = CurrentColor;
            _TargetColor = GetRandomColorLight(_ColorLightness, _ColorScale);

            _LerpAmount = 0;
            _LerpSpeed = _ByteChangeDuration *
                (Math.Abs(_PreviousColor.R - _TargetColor.R)
                + Math.Abs(_PreviousColor.G - _TargetColor.G)
                + Math.Abs(_PreviousColor.B - _TargetColor.B)) / 3;
        }

        private void Lerp(float amount)
        {
            _CurrentColor.R = (byte)(_PreviousColor.R + (byte)Math.Round((_TargetColor.R - _PreviousColor.R) * amount));
            _CurrentColor.G = (byte)(_PreviousColor.G + (byte)Math.Round((_TargetColor.G - _PreviousColor.G) * amount));
            _CurrentColor.B = (byte)(_PreviousColor.B + (byte)Math.Round((_TargetColor.B - _PreviousColor.B) * amount));

            _LerpAmount = amount;
            if (_LerpAmount >= 1)
                NextTargetColor();

        }

        public void Update(float seconds)
        {
            Lerp(_LerpAmount + seconds * _LerpSpeed);
        }

        public static Color GetRandomColorLight(float lightValue, float multiplier)
        {
            Vector3 col = GetRandomColor().ToVector3();

            float diff = 1 - lightValue;
            col.X = multiplier * (lightValue + col.X * diff);
            col.Y = multiplier * (lightValue + col.Y * diff);
            col.Z = multiplier * (lightValue + col.Z * diff);

            return new Color(col);
        }
        public static Color GetRandomColor()
        {
            Color c = new Color();
            c.R = (byte)(BubbleBreak.Random.Next() % 255);
            c.G = (byte)(BubbleBreak.Random.Next() % 255);
            c.B = (byte)(BubbleBreak.Random.Next() % 255);
            c.A = 255;
            return c;
        }

        public static Color GetInvertedColor(Color color)
        {
            color.R = (byte)(255 - color.R);
            color.G = (byte)(255 - color.G);
            color.B = (byte)(255 - color.B);
            return color;
        }
    }
}
