﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Bubblebreak.GameState
{
    public class ModeProxy
    {
        Dictionary<Type, Mode> modes = new Dictionary<Type, Mode>();

        /// <summary>
        /// The mode which is currently active
        /// </summary>
        public Mode CurrentMode { get { return currentMode; } }
        Mode currentMode;

        public InputState InputState { get { return inputState; } }
        InputState inputState = new InputState();

        public Microsoft.Xna.Framework.Game Game { get; private set; }

        public ModeProxy(Microsoft.Xna.Framework.Game game)
        {
            this.Game = game;
        }



        /// <summary>
        /// Registers a Mode. Note that every mode can be registered only once.
        /// </summary>
        /// <param name="mode">the mode to register</param>
        public void Register(Mode mode) 
        {
            Type modeType = mode.GetType();
            if (modes.ContainsKey(modeType))
                throw new ArgumentException(string.Format("a mode with the type {0} has already been registered.", modeType));

            modes.Add(modeType, mode);
            mode.Proxy = this;
        }

        /// <summary>
        /// Get's the Mode of the type T
        /// </summary>
        /// <typeparam name="T">the type of the mode to get</typeparam>
        /// <returns>the requested mode</returns>
        public T GetMode<T>() where T : Mode
        {
            return (T)modes[typeof(T)];
        }

        /// <summary>
        /// Set's the current mode to the mode with the specified type. Calls Leave() on the old and Enter() on the new mode
        /// </summary>
        /// <typeparam name="T">the type of the mode to set</typeparam>
        public void SetMode<T>() where T : Mode
        {
            SetMode(typeof(T));
        }
        /// <summary>
        /// Set's the current mode to the mode with the specified type. Calls Leave() on the old and Enter() on the new mode
        /// </summary>
        /// <param name="enterMode">the type of the mode to set</param>
        public void SetMode(Type enterMode)
        {
            if (currentMode != null)
                currentMode.Leave();

            this.currentMode = modes[enterMode];

            currentMode.Enter();
        }


        /// <summary>
        /// Checks wether the passed mode-type is currently the active mode.
        /// </summary>
        /// <typeparam name="T">the mode to check</typeparam>
        /// <returns>true, if the current mode is of the given type</returns>
        public bool IsActiveMode<T>() where T : Mode
        {
            return modes.ContainsKey(typeof(T)) && modes[typeof(T)] == currentMode;
        }


        /// <summary>
        /// Updates the input and the currently active mode
        /// </summary>
        /// <param name="gameTime">the game time object</param>
        public void Update(GameTime gameTime)
        {
            inputState.Update();

            if (currentMode == null)
                return;

            currentMode.Update(gameTime, inputState);
        }

        /// <summary>
        /// Draws the current mode
        /// </summary>
        /// <param name="gameTime">the draw-time</param>
        public void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (currentMode != null)
                currentMode.Draw(gameTime, spriteBatch);
        }

    }
}
