﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Bubblebreak.GameState;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using starLiGHT.GUI;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework.GamerServices;

#if WINDOWS_PHONE
using Microsoft.Phone.Tasks;
#endif

namespace Bubblebreak.Menu
{
    public class TrialExitScreen : MenuMode
    {

        Texture2D background;
        Button buyButton, exitButton;
        Label title, features;

        public TrialExitScreen(GuiManager manager)
            : base(manager, "TrialExitScreen")
        { }

        protected internal override void Enter()
        {
            buyButton = CreateButton("buy", new Vector2(300, 400), new Vector2(400, 63), (o, a) => BuyButtonPressed());
            exitButton = CreateButton("exit_button", new Vector2(720, 400), new Vector2(63, 63),
                (o, a) => BubbleBreak.Instance.Exit(), "CancelButton");

            
            title = new Label(GuiManager, Name + "title")
            {
                Location = new Vector2(5, 5),
                Size = new Vector2(400, 30),
            };
            container.Widgets.Add(title);

            features = new Label(GuiManager, Name + "features")
            {
                Location = new Vector2(10, 40),
                Size = new Vector2(400, 400),
                WrapText = true,
                Font = BubbleBreak.FontSmall,
                ClauseExtraSpacing = 2,
            };
            container.Widgets.Add(features);

            buyButton.Text = Loc["buy"]; //"Vollversion kaufen!";
            title.Text = Loc["buy_title"]; //"In the full version awaits you:";
            features.Text = Loc["buy_features"];
            //"- Play as long as you want or can\n"
            //    + "- 10 different Levels\n"
            //    + "- 29 Achievements & Smilies\n"
            //    + "- Progress is saved\n"
            //    + "- Online Highscores";

            background = Game.Content.Load<Texture2D>("trial_exit_screen");

            base.Enter();
        }

        protected internal override void Leave()
        {
            base.Leave();
        }

        private void BuyButtonPressed()
        {
#if WINDOWS_PHONE
            MarketplaceLauncher.Show(MarketplaceContent.Applications, MarketplaceOperation.ViewDetails);
#endif
        }

        protected internal override void Update(GameTime gameTime, InputState inputState)
        {

            base.Update(gameTime, inputState);
        }

        protected internal override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Begin();

            spriteBatch.Draw(background, Vector2.Zero, Color.White);

            spriteBatch.End();

            base.Draw(gameTime, spriteBatch);
        }
    }
}
