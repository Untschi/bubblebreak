﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework;
using Bubblebreak.Modes;

namespace Bubblebreak.Menu
{
    public class GameOverNameBoxMode : MenuMode
    {
        TextBox textBox;
        Label label;

        public GameOverNameBoxMode(GuiManager manager)
            : base(manager, "GameOverNameBoxMode")
        {

            label = new Label(manager, Name + "text")
            {
                Location = new Vector2(300, 140),
                Size = new Vector2(200, 30),
                Font = BubbleBreak.FontSmall,
                TextAlignment = TextAlignment.Center,
            };
            container.Widgets.Add(label);

            textBox = new TextBox(manager, Name + "_textBox")
            {
                Location = new Vector2(300, 180),
                Size = new Vector2(200, 36),
              //  Font = BubbleBreak.FontSmall,
            };
            container.Widgets.Add(textBox);

            CreateButton("apply", new Vector2(368, 280), new Vector2(63, 63), Apply, "ApplyButton");

        }

        protected internal override void Enter()
        {
            label.Text = Loc["enter_player_name"];
            textBox.Text = BubbleBreak.PlayerName;
            base.Enter();
        }

        void Apply(object sender, EventArgs args)
        {
            if(string.IsNullOrEmpty(textBox.Text))
            {
                textBox.Text = "Player";
                return;
            }

            if (string.IsNullOrEmpty(BubbleBreak.PlayerName))
                BubbleBreak.PlayerName = textBox.Text;

            PlayerScoreEntryInfo info = new PlayerScoreEntryInfo(textBox.Text, LevelManager.Instance.TotalScore);
            BubbleBreak.Instance.PlayerScoreEntryInfo = info;

            
            BubbleBreak.Instance.ModeProxy.GetMode<MainMenu>().EnterMode = typeof(HighscoreMenuMode);
            BubbleBreak.Instance.ModeProxy.SetMode<MainMenu>();

            if (Proxy.GetMode<HighscoreMenuMode>().AddScore(info.PlayerName, info.Score))
                Proxy.GetMode<HighscoreMenuMode>().ShowLocalScore();
            else
                Proxy.GetMode<HighscoreMenuMode>().ShowLocalsCorrupt();

        }
    }
}
