﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util;
using Sunbow.Util.Sprites;
using System.IO.IsolatedStorage;
using System.IO;

namespace Bubblebreak.Menu
{
    public class SettingsMenuMode : MenuMode
    {
        static readonly Vector2 TitleCirclePos = new Vector2(110, 110);
        static readonly Vector2 GameCirclePos = new Vector2(240, 290);
        static readonly Vector2 VolumeCirclePos = new Vector2(370, 110);
        static readonly Vector2 AppearanceCirclePos = new Vector2(500, 290);
        static readonly Vector2 PlayerNameCirclePos = new Vector2(630, 110);

        Checkbox sensorInputEnabled, playingFieldFlipEnabled;
        ClickableStateStepper ballAppearance;
        HScrollBar musicVolumeBar, sfxVolumeBar;
        Label title, volumeLabel, musicVolumeLabel, sfxVolumeLabel, appearanceTitleLabel, appearanceLabel, 
            sensorInputLabel, playingFieldFlipLabel, playerNameLabel;

        Button resetBtn, resetConfirmBtn, resetCancelBtn;
        Label resetConfirmLbl;
        ContainerWidget resetConfimPnl;

        TextBox nameBox;

        public SettingsMenuMode(GuiManager manager)
            : base(manager, "SettingsMenuMode")
        {
            #region title
            title = new Label(manager, Name + "_title")
            {
                Location = TitleCirclePos - new Vector2(80, 17),
                Size = new Vector2(160, 34),
                TextAlignment = TextAlignment.Center,
                Font = BubbleBreak.FontSmall,
            };
            container.Widgets.Add(title);
            #endregion

            #region sound
            volumeLabel = new Label(manager, Name + "_volume_label")
            {
                Location = VolumeCirclePos - new Vector2(80, 30),
                Size = new Vector2(160, 34),
                TextAlignment = TextAlignment.Center,
            };
            container.Widgets.Add(volumeLabel);

            musicVolumeLabel = new Label(manager, Name + "_music_vol_label")
            {
                Location = VolumeCirclePos - new Vector2(70, 90),
                Size = new Vector2(140, 30),
                TextAlignment = starLiGHT.GUI.TextAlignment.Center,
                Font = BubbleBreak.FontSmall,
            };
            container.Widgets.Add(musicVolumeLabel);

            musicVolumeBar = new HScrollBar(manager, Name + "_music_vol_bar")
            {
                Location = VolumeCirclePos - new Vector2(70, 55),
                Size = new Vector2(140, 28),
            };
            container.Widgets.Add(musicVolumeBar);

            sfxVolumeLabel = new Label(manager, Name + "_sfx_vol_label")
            {
                Location = VolumeCirclePos + new Vector2(-80, 35),
                Size = new Vector2(160, 60),
                TextAlignment = starLiGHT.GUI.TextAlignment.Center,
                WrapText = true,
                Font = BubbleBreak.FontSmall,
            };
            container.Widgets.Add(sfxVolumeLabel);

            sfxVolumeBar = new HScrollBar(manager, Name + "_sfx_vol_bar")
            {
                Location = VolumeCirclePos + new Vector2(-70, 10),
                Size = new Vector2(140, 28),
            };
            container.Widgets.Add(sfxVolumeBar);

            musicVolumeBar.ValueChanged += VolumeChanged;
            sfxVolumeBar.ValueChanged += VolumeChanged;
            #endregion

            #region game
            sensorInputEnabled = new Checkbox(manager, Name + "_sensor_checkbox")
            {
                Location = GameCirclePos - new Vector2(14, 90),
                Size = new Vector2(28, 28),
            };
            container.Widgets.Add(sensorInputEnabled);

            sensorInputLabel = new Label(manager, Name + "_sensor_label")
            {
                Location = GameCirclePos - new Vector2(80, 65),
                Size = new Vector2(160, 60),
                WrapText = true,
                TextAlignment = TextAlignment.Center,
                Font = BubbleBreak.FontSmall,
            };
            container.Widgets.Add(sensorInputLabel);

            playingFieldFlipEnabled = new Checkbox(manager, Name + "_field_flip_checkbox")
            {
                Location = GameCirclePos + new Vector2(-14, 60),
                Size = new Vector2(28, 28),
            };
            container.Widgets.Add(playingFieldFlipEnabled);

            playingFieldFlipLabel = new Label(manager, Name + "_field_flip_label")
            {
                Location = GameCirclePos + new Vector2(-110, 0),
                Size = new Vector2(220, 60),
                WrapText = true,
                TextAlignment = TextAlignment.Center,
                Font = BubbleBreak.FontSmall,
            };
            container.Widgets.Add(playingFieldFlipLabel);

            sensorInputEnabled.CheckChanged += CheckChanged;
            playingFieldFlipEnabled.CheckChanged += CheckChanged;
            #endregion

            #region appearance

            appearanceTitleLabel = new Label(manager, Name + "_appearance_label")
            {
                Location = AppearanceCirclePos - new Vector2(80, 60),
                Size = new Vector2(160, 34),
                TextAlignment = TextAlignment.Center,
                WrapText = true,
            };
            container.Widgets.Add(appearanceTitleLabel);

            ballAppearance = new ClickableStateStepper(manager, Name + "_smilie_checkbox", "BallAppearance")
            {
                Location = AppearanceCirclePos + new Vector2(-14, 60),
                Size = new Vector2(28, 28),
            };
            container.Widgets.Add(ballAppearance);

            appearanceLabel = new Label(manager, Name + "_smilie_label")
            {
                Location = AppearanceCirclePos + new Vector2(-80, 10),
                Size = new Vector2(160, 60),
                WrapText = true,
                TextAlignment = TextAlignment.Center,
                Font = BubbleBreak.FontSmall,
            };
            container.Widgets.Add(appearanceLabel);

            ballAppearance.StateChanged += CheckChanged;
            #endregion

            #region player name
            playerNameLabel = new Label(manager, Name + "player_name")
            {
                Location = PlayerNameCirclePos - new Vector2(100, 80),
                Size = new Vector2(200, 60),
                Font = BubbleBreak.FontSmall,
                TextAlignment = TextAlignment.Center,
                WrapText = true,
            };
            container.Widgets.Add(playerNameLabel);

            nameBox = new TextBox(manager, Name + "player_name_text_box")
            {
                Location =PlayerNameCirclePos - new Vector2(100, 18),
                Size = new Vector2(200, 36),
                //  Font = BubbleBreak.FontSmall,
            };
            container.Widgets.Add(nameBox);

            #endregion

            #region reset

            resetBtn = CreateButton("reset_progress", new Vector2(20, 400), new Vector2(680, 63), ResetButtonPressed);

            resetConfimPnl = new ContainerWidget(manager, Name + "_reset_confirm_panel", "ToolTipPanel")
            {
                Location = new Vector2(52, 150),
                Size = new Vector2(620, 230),
            };
            container.Widgets.Add(resetConfimPnl);

            resetConfirmLbl = new Label(manager, Name + "_reset_confirm_label")
            {
                Location = new Vector2(20, 20),
                Size = new Vector2(580, 150),
                WrapText = true,
                TextAlignment = TextAlignment.Center,
            };
            resetConfimPnl.Widgets.Add(resetConfirmLbl);

            resetConfirmBtn = new Button(manager, Name + "_reset_confirm_btn", "ApplyButton")
            {
                Size = new Vector2(63, 63),
                Location = new Vector2(220, 150),
            };
            resetConfirmBtn.Click += resetConfirmBtn_Click;
            resetConfimPnl.Widgets.Add(resetConfirmBtn);

            resetCancelBtn = new Button(manager, Name + "_reset_cancel_btn", "CancelButton")
            {
                Size = new Vector2(63, 63),
                Location = new Vector2(340, 150),
            };
            resetCancelBtn.Click += resetCancelBtn_Click;
            resetConfimPnl.Widgets.Add(resetCancelBtn);


            #endregion

            CreateBackButton();

            colors.Add(new ColorChanger(2, 0.7f));
            colors.Add(new ColorChanger(2, 0.7f));
            colors.Add(new ColorChanger(2, 0.7f));
            colors.Add(new ColorChanger(2, 0.7f));
            colors.Add(new ColorChanger(2, 0.7f));


            Load();
        }

        void resetCancelBtn_Click(object sender, EventArgs e)
        {
            resetConfimPnl.Visible = false;
        }

        void resetConfirmBtn_Click(object sender, EventArgs e)
        {
            using (IsolatedStorageFile file = Helper.GetUserStoreForAppDomain())
            {
                try
                {
                    file.DeleteFile("high.score");
                } catch (Exception) { }   

                try
                {
                    file.DeleteFile("achievements");
                } catch (Exception) { }
            }
            BubbleBreak.Instance.Achievements.ClearAchievements();
            Proxy.GetMode<HighscoreMenuMode>().ReInitializeLocalScores();

            resetConfimPnl.Visible = false;
        }
        void ResetButtonPressed(object sender, EventArgs e)
        {
            resetConfimPnl.Visible = true;
        }
        void VolumeChanged(object sender, EventArgs e)
        {
            if (sender == musicVolumeBar)
            {
                BubblebreakSound.MusicVolume = musicVolumeBar.Value / 100f;
            }
            else if (sender == sfxVolumeBar)
            {
                BubblebreakSound.SoundEffectVolume = sfxVolumeBar.Value / 100f;
                BubblebreakSound.Play(BubblebreakSound.ShootBar);
            }
        }

        void CheckChanged(object sender, EventArgs e)
        {
            if (sender == sensorInputEnabled)
            {
                BubbleBreak.SensorInputEnabled = sensorInputEnabled.Checked;
            }
            else if (sender == ballAppearance)
            {
                Ball.BallAppearance = (BallAppearance)ballAppearance.CurrentStateIndex;
                SetBallAppearanceText();
            }
            else if (sender == playingFieldFlipEnabled)
            {
                BubbleBreak.PlayingFieldFlipped = playingFieldFlipEnabled.Checked;
            }
        }


        protected internal override void Enter()
        {

            title.Text = Loc["btn_settings"];
            musicVolumeLabel.Text = Loc["music"];
            sfxVolumeLabel.Text = Loc["sfx"];
            volumeLabel.Text = Loc["volume"];
            appearanceTitleLabel.Text = Loc["bubble_appearance"];
            sensorInputLabel.Text = Loc["sensor_input"];
            playingFieldFlipLabel.Text = Loc["flip_playing_field"];
            playerNameLabel.Text = Loc["enter_player_name"];
            resetBtn.Text = Loc["reset_progress"];
            resetConfirmLbl.Text = Loc["reset_progress_confirm"];

            nameBox.Text = BubbleBreak.PlayerName;

            resetConfimPnl.Visible = false;

            SetBallAppearanceText();

            base.Enter();
        }

        private void SetBallAppearanceText()
        {
            switch (Ball.BallAppearance)
            {
                case BallAppearance.Smilie:
                    appearanceLabel.Text = Loc["smilies"];
                    break;
                case BallAppearance.Bubble:
                    appearanceLabel.Text = Loc["bubble"];
                    break;
                case BallAppearance.ColorSymbol:
                    appearanceLabel.Text = Loc["color_symbols"];
                    break;
            }
        }

        protected internal override void Leave()
        {
            BubbleBreak.PlayerName = nameBox.Text;

            Save();

            base.Leave();
        }


        protected internal override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Frame frame = menuSheet.GetFrame("default", "small_bubble");

            spriteBatch.Begin();

            menuSheet.Draw(spriteBatch, frame, TitleCirclePos, 0, 1, colors[0].CurrentColor);
            menuSheet.Draw(spriteBatch, frame, VolumeCirclePos, 0, 1, colors[1].CurrentColor);
            menuSheet.Draw(spriteBatch, frame, GameCirclePos, 0, 1, colors[2].CurrentColor);
            menuSheet.Draw(spriteBatch, frame, AppearanceCirclePos, 0, 1, colors[3].CurrentColor);
            menuSheet.Draw(spriteBatch, frame, PlayerNameCirclePos, 0, 1, colors[4].CurrentColor);

            spriteBatch.End();


            base.Draw(gameTime, spriteBatch);
        }



        public void Save()
        {
            using (IsolatedStorageFile file = Helper.GetUserStoreForAppDomain())
            {
                using (IsolatedStorageFileStream stream = file.OpenFile(@"settings", FileMode.OpenOrCreate))
                {
                    BinaryWriter writer = new BinaryWriter(stream);

                    writer.Write(BubblebreakSound.SoundEffectVolume);
                    writer.Write(BubblebreakSound.MusicVolume);
                    writer.Write(BubbleBreak.SensorInputEnabled);
                    writer.Write((int)Ball.BallAppearance);
                    writer.Write(BubbleBreak.PlayingFieldFlipped);
                    writer.Write(BubbleBreak.PlayerName);

                    writer.Close();
                }
            }
        }

        public void Load()
        {
            using (IsolatedStorageFile file = Helper.GetUserStoreForAppDomain())
            {
                if (file.FileExists(@"settings"))
                {
                    try
                    {
                        using (IsolatedStorageFileStream stream = file.OpenFile(@"settings", FileMode.Open))
                        {
                            BinaryReader reader = new BinaryReader(stream);

                            BubblebreakSound.SoundEffectVolume = reader.ReadSingle();
                            BubblebreakSound.MusicVolume = reader.ReadSingle();
                            BubbleBreak.SensorInputEnabled = reader.ReadBoolean();
                            Ball.BallAppearance = (BallAppearance)reader.ReadInt32();
                            BubbleBreak.PlayingFieldFlipped = reader.ReadBoolean();
                            BubbleBreak.PlayerName = reader.ReadString();

                            reader.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                        // System.Diagnostics.Contracts.Contract.Assert(false, "couldn't read settings file");
                    }
                }

                this.sfxVolumeBar.Value = (int)(100 * BubblebreakSound.SoundEffectVolume);
                this.musicVolumeBar.Value = (int)(100 * BubblebreakSound.MusicVolume);
                this.sensorInputEnabled.Checked = BubbleBreak.SensorInputEnabled;
                this.playingFieldFlipEnabled.Checked = BubbleBreak.PlayingFieldFlipped;
                this.ballAppearance.CurrentStateIndex = (int)Ball.BallAppearance;
                this.nameBox.Text = BubbleBreak.PlayerName;
            }



        }

    }
}
