﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using starLiGHT.GUI;
using Microsoft.Xna.Framework;
using Bubblebreak.GameState;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util;
using Sunbow.Util.Sprites;

namespace Bubblebreak.Menu
{
    public abstract class MenuMode : Mode
    {
        internal static SpriteSheet menuSheet;
        protected static string[] langOrder = new string[] { "en-US", "de-DE", "fr-FR", "es-ES", "pt-PT" };
        static MenuMode()
        {
            menuSheet = new SpriteSheet(BubbleBreak.Instance.Content.Load<Texture2D>("MenuSheet"));
            menuSheet.RegisterFrameSet(new FreeFrameSet("default"));
            menuSheet.RegisterFrame("default", "big_bubble", new Frame(new Rectangle(224, 528, 800, 496), new Vector2(0, 8)));
            menuSheet.RegisterFrame("default", "line", new Frame(new Rectangle(500, 528, 100, 8), new Vector2(0, 4)));
            menuSheet.RegisterFrame("default", "small_bubble", new Frame(new Rectangle(0, 752, 224, 223), new Vector2(112, 112)));
            menuSheet.RegisterFrame("default", "big_dot", new Frame(new Rectangle(143, 975, 49, 49), new Vector2(24, 24)));
            menuSheet.RegisterFrame("default", "small_dot", new Frame(new Rectangle(192, 975, 49, 49), new Vector2(24, 24)));
            menuSheet.RegisterFrame("default", "logo", new Frame(new Rectangle(0, 975, 143, 49), new Vector2(71, 0)));
            menuSheet.RegisterFrame("default", "sunbow", new Frame(new Rectangle(736, 322, 138, 92), new Vector2(69, 80)));
            menuSheet.RegisterFrame("default", "libraries", new Frame(new Rectangle(866, 407, 158, 91), Vector2.Zero));
            menuSheet.RegisterFrame("default", "thanks", new Frame(new Rectangle(877, 337, 146, 64), Vector2.Zero));

            menuSheet.RegisterFrameSet(new FixedFrameSet("flags", 32, 22, new Point(224, 506), 5, ReadDirection.LeftToRight_NextLineBelow));

            const float switchDuration = 1;
            FixedFrameSet help = new FixedFrameSet("help", 150, 150, 0, 0, new Point(574, 0), 3, 0, 6, ReadDirection.LeftToRight_NextLineBelow, new Vector2(75, 75));
            menuSheet.RegisterFrameSet(help);
            help.RegisterAnimation(new FrameAnimation("help_ani0", 0, 1, switchDuration, RepeatLogic.Looped));
            help.RegisterAnimation(new FrameAnimation("help_ani1", 0, 3, switchDuration, RepeatLogic.Looped));
            help.RegisterAnimation(new FrameAnimation("help_ani2", 0, 4, switchDuration, RepeatLogic.Looped));
            help.RegisterAnimation(new FrameAnimation("help_ani3", 0, 5, switchDuration, RepeatLogic.Looped));
            help.RegisterAnimation(new FrameAnimation("help_ani4", 0, 6, switchDuration, RepeatLogic.Looped));


            //menuSheet.RegisterFrame("default", "test", new Frame(new Rectangle(300, 600, 3, 3), new Vector2(1, 1)));
        }


        protected ContainerWidget container;

        public string Name { get { return container.Name; } }
        public GuiManager GuiManager { get; private set; }
        public Localization Loc { get { return BubbleBreak.Instance.Loc; } }

        protected List<ColorChanger> colors = new List<ColorChanger>();

        protected MenuMode(GuiManager guiManager, string panelName)
        {
            this.GuiManager = guiManager;

            container = new ContainerWidget(guiManager, panelName);
            container.Size = new Vector2(guiManager.GraphicsDevice.Viewport.Width, guiManager.GraphicsDevice.Viewport.Height);
            container.Visible = false;

            guiManager.Widgets.Add(container);
        }
        protected CheckButton CreateCheckButton(string name, Vector2 position, Vector2 size, EventHandler action)
        {
            CheckButton btn = new CheckButton(GuiManager, string.Format("{0}_{1}", container.Name, name), "Button");
                //btn.Text = Loc[name];

            btn.Location = position;
            btn.Size = size;
            btn.TextAlignment = TextAlignment.Center;

            btn.Click += action;
            container.Widgets.Add(btn);

            return btn;
        }


        protected Button CreateButton(string name, Vector2 position, Vector2 size, EventHandler action)
        {
            return CreateButton(name, position, size, action, "Button");
        }
        protected Button CreateButton(string name, Vector2 position, Vector2 size, EventHandler action, string layout)
        {
            Button btn = new Button(GuiManager, string.Format("{0}_{1}", container.Name, name), layout);
            if (layout == "Button")
                btn.Text = Loc[name];

            btn.Location = position;
            btn.Size = size;
            btn.TextAlignment = TextAlignment.Center;

            btn.Click += action;
            container.Widgets.Add(btn);

            return btn;
        }

        protected Button CreateBackButton()
        {
            return CreateButton("back_button", new Vector2(720, 400), new Vector2(63, 63), (s, a) => Proxy.SetMode<MainMenuMode>(), "BackButton");
        }

        protected Label CreateLabel(string name, Vector2 position, Vector2 size, SpriteFont font, TextAlignment align, bool wrap)
        {
            return CreateLabel(name, position, size, font, align, true, Color.White);
        }
        protected Label CreateLabel(string name, Vector2 position, Vector2 size, SpriteFont font, TextAlignment align, bool wrap, Color foreColor)
        {
            Label lbl = new Label(GuiManager, string.Format("{0}_{1}", Name, name))
            {
                Location = position,
                Size = size,
                Font = font,
                TextAlignment = align,
                WrapText = wrap,
                ForeColor = foreColor,
            };
            container.Widgets.Add(lbl);
            return lbl;
        }

        protected internal override void Enter()
        {
            container.Visible = true;
        }

        protected internal override void Leave()
        {
            container.Visible = false;
        }

        protected internal override void Update(GameTime gameTime, InputState inputState)
        {
            float sec = gameTime.GetElapsedSeconds();

            foreach (ColorChanger col in colors)
                col.Update(sec);
        }

        protected internal override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            // nothing to do
        }

    }
}
