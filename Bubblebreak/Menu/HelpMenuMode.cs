﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI.Widgets;
using starLiGHT.GUI;
using Microsoft.Xna.Framework;
using Sunbow.Util.Sprites;

namespace Bubblebreak.Menu
{
    public class HelpMenuMode : MenuMode
    {
        const int PAGE_COUNT = 6;

        Label title;
        Label text;
        Label specialName;
        int helpIndex;

        FrameAnimation animation;

        public HelpMenuMode(GuiManager manager)
            : base(manager, "HelpMenuMode")
        {
            title = new Label(manager, "help_title")
            {
                Location = new Vector2(50, 25),
                Size = new Vector2(425, 50),
            };
            container.Widgets.Add(title);

            text = new Label(manager, "help_text")
            {
                Location = new Vector2(25, 75),
                Size = new Vector2(450, 380),
                WrapText = true,
                Font = BubbleBreak.FontSmall,
                TextAlignment = TextAlignment.Justify,
            };
            container.Widgets.Add(text);

            specialName = new Label(manager, "help_special_name")
            {
                Location = new Vector2(565, 120),
                Size = new Vector2(190, 75),
                WrapText = true,
                TextAlignment = TextAlignment.Center,
                Font = BubbleBreak.FontSmall,
            };
            container.Widgets.Add(specialName);

            CreateButton("help_prev", new Vector2(50, 400), new Vector2(63, 63), (s, a) => SetHelpIndex((helpIndex + PAGE_COUNT - 1) % PAGE_COUNT), "PreviousButton");
            CreateButton("help_next", new Vector2(400, 400), new Vector2(63, 63), (s, a) => SetHelpIndex((helpIndex + 1) % PAGE_COUNT), "NextButton");

            CreateBackButton();

            colors.Add(new ColorChanger(2, 0.7f));
            colors.Add(new ColorChanger(2, 0.7f));

        }

        protected internal override void Enter()
        {
            SetHelpIndex(0);

            base.Enter();
        }

        private void SetHelpIndex(int index)
        {
            helpIndex = index;
            title.Text = string.Format("{0}. {1}", index + 1, Loc["hlp_heading" + (index + 1)]);
            text.Text = Loc["hlp_text" + (index + 1)];

            if (index <= 4)
            {
                int idx = (animation != null) ? animation.CurrentIndex : 0;
                animation = menuSheet.CreateAnimation("help", "help_ani" + index);
                animation.CurrentIndex = idx;
            }
            else if (index == 5)
            {
                animation = BubbleBreak.Instance.GameSheet.CreateAnimation("BigItems", "BigItem_ani");
            }
            else
            {
                animation = null;
            }
        }

        protected internal override void Update(GameTime gameTime, GameState.InputState inputState)
        {
            float sec = (float)gameTime.ElapsedGameTime.TotalSeconds;
            
            if (animation != null)
                animation.Update(sec);
            
            base.Update(gameTime, inputState);
        }

        protected internal override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            Frame bigBubble = menuSheet.GetFrame("default", "big_bubble");
            Frame smallBubble = menuSheet.GetFrame("default", "small_bubble");
            Frame bigDot = menuSheet.GetFrame("default", "big_dot");
            Frame smallDot = menuSheet.GetFrame("default", "small_dot");



            spriteBatch.Begin();

            Vector2 picPos = new Vector2(660, 125);
            menuSheet.Draw(spriteBatch, smallBubble, picPos, 0, 1, colors[0].CurrentColor);
            menuSheet.Draw(spriteBatch, bigBubble, new Vector2(-230, 0), 0, 1, colors[1].CurrentColor);

            for (int i = 0; i < PAGE_COUNT; i++)
            {
                Frame f = (i == helpIndex) ? bigDot : smallDot;
                menuSheet.Draw(spriteBatch, f, new Vector2(150 + i * 40, 425), 0, 1);
            }

            if (animation != null)
            {
                if (animation.Name == "BigItem_ani")
                {
                    specialName.Text = Loc["hlp_special" + animation.CurrentIndex];
                    BubbleBreak.Instance.GameSheet.Draw(spriteBatch, animation.CurrentFrame, picPos - new Vector2(56, 58), 0, 1);
                }
                else
                {
                    specialName.Text = string.Empty;
                    menuSheet.Draw(spriteBatch, animation.CurrentFrame, picPos, 0, 1);
                }
            }
            spriteBatch.End();

            base.Draw(gameTime, spriteBatch);
        }

    }
}
