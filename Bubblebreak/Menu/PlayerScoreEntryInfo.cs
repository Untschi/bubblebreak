﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bubblebreak.Menu
{
    internal class PlayerScoreEntryInfo
    {
        internal string PlayerName { get; set; }
        internal int Score { get; set; }
        internal bool OnlineSubmitted { get; set; }


        internal PlayerScoreEntryInfo(string playerName, int score)
        {
            this.PlayerName = playerName;
            this.Score = score;
        }

    }
}
