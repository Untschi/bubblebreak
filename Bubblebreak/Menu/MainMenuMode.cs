﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework;
using Sunbow.Util.Sprites;
using System.Diagnostics;

#if WINDOWS_PHONE
using Microsoft.Phone.Tasks;
#endif

namespace Bubblebreak.Menu
{
    public class MainMenuMode : MenuMode
    {
        static readonly Color SunbowLinkColor = Color.Wheat * 0.25f;

        Button startGame, highscores, settings, help, about,
            starButton, marketButton, bugButton, webButton, exitButton;
        RadioButton[] flagButtons;
        Label rateLabel, marketLabel, bugLabel, webLabel, exitLabel, versionLabel;

        int langIndex;

        public MainMenuMode(GuiManager manager)
            : base(manager, "MainMenu")
        {
            startGame = CreateButton("btn_start_game", new Vector2(100, 48), new Vector2(310, 63), 
                (s, a) => StartGame());
            highscores = CreateButton("btn_highscores", new Vector2(120, 128), new Vector2(310, 63), 
                (s, a) => HighScores());
            settings = CreateButton("btn_settings", new Vector2(130, 208), new Vector2(310, 63), 
                (s,a) => Proxy.SetMode<SettingsMenuMode>());
            help = CreateButton("btn_help", new Vector2(120, 288), new Vector2(310, 63), 
                (s, a) => Proxy.SetMode<HelpMenuMode>());
            about = CreateButton("btn_about", new Vector2(100, 368), new Vector2(310, 63), 
                (s, a) => Proxy.SetMode<CreditsMenuMode>());

            starButton = CreateButton("star_btn", new Vector2(720, 70), new Vector2(63, 63),
                SunbowAction, "StarButton");
            marketButton = CreateButton("market_btn", new Vector2(720, 145), new Vector2(63, 63),
                SunbowAction, "MarketButton");
            //bugButton = CreateButton("bug_btn", new Vector2(720, 220), new Vector2(63, 63),
            //    SunbowAction, "BugButton"); 
            webButton = CreateButton("web_btn", new Vector2(720, 220), new Vector2(63, 63), // pos = new Vector2(720, 295)
                SunbowAction, "WebButton");

            exitButton = CreateButton("exit_button", new Vector2(720, 400), new Vector2(63, 63),
                SunbowAction, "CancelButton");

            rateLabel = CreateLabel("star_lbl", new Vector2(500, 70), new Vector2(200, 80), 
                BubbleBreak.FontSmall, TextAlignment.Right, true, SunbowLinkColor);
            marketLabel = CreateLabel("market_lbl", new Vector2(480, 145), new Vector2(220, 80),
                BubbleBreak.FontSmall, TextAlignment.Right, true, SunbowLinkColor);
            //bugLabel = CreateLabel("bug_lbl", new Vector2(480, 220), new Vector2(220, 80),
            //    BubbleBreak.FontSmall, TextAlignment.Right, true, SunbowLinkColor);
            webLabel = CreateLabel("web_lbl", new Vector2(500, 220), new Vector2(200, 80), // pos = new Vector2(500, 295)
                BubbleBreak.FontSmall, TextAlignment.Right, true, SunbowLinkColor);
            exitLabel = CreateLabel("exit_lbl", new Vector2(500, 430), new Vector2(200, 80),
                BubbleBreak.FontSmall, TextAlignment.Right, true, SunbowLinkColor);

            versionLabel = CreateLabel("version_lbl", new Vector2(700, 5), new Vector2(95, 40),
                BubbleBreak.FontSmall, TextAlignment.Right, false, Color.DarkGray);
            versionLabel.Text = "v" + VersionInfo.Version;

            colors.Add(new ColorChanger(2, 0.7f));
            colors.Add(new ColorChanger(2, 0.7f));
            colors.Add(new ColorChanger(2, 0.7f));
            colors.Add(new ColorChanger(2, 0f));
            //fgColor = new ColorChanger(2, 0.7f);

            flagButtons = new RadioButton[langOrder.Length];
            for (int i = 0; i < flagButtons.Length; i++)
            {
                flagButtons[i] = new RadioButton(manager, "flag" + langOrder[i], "FlagRadioButton");
                flagButtons[i].Location = new Vector2(16, 100 - 3 + 60 * i);
                flagButtons[i].Size = new Vector2(40, 29);
                flagButtons[i].CheckChanged += FlagCheckChanged;

                if (langOrder[i] == Loc.Language)
                {
                    flagButtons[i].Checked = true;
                    langIndex = i;
                }

                container.Widgets.Add(flagButtons[i]);
            }

            ReloadTexts();
        }

        void ReloadTexts()
        {
            startGame.Text = Loc["btn_start_game"];
            highscores.Text = Loc["btn_highscores"];
            settings.Text = Loc["btn_settings"];
            help.Text = Loc["btn_help"];
            about.Text = Loc["btn_about"];

            rateLabel.Text = Loc["rate_review"];
            marketLabel.Text = Loc["market"];
            //bugLabel.Text = Loc["bug"];
            webLabel.Text = Loc["web"];
            exitLabel.Text = Loc["exit"];
        }
        void SunbowAction(object sender, EventArgs e)
        {
            if (sender == starButton)
            {
#if WINDOWS_PHONE
                MarketplaceReviewTask market = new MarketplaceReviewTask();
                market.Show();
#endif
            }
            else if (sender == marketButton)
            {
#if WINDOWS_PHONE
                MarketplaceSearchTask market = new MarketplaceSearchTask()
                {
                    ContentType = MarketplaceContentType.Applications,
                    SearchTerms = "sunbow games",
                };
                market.Show();
#endif
            }
            else if (sender == webButton)
            {
#if WINDOWS_PHONE
                WebBrowserTask www = new WebBrowserTask()
                {
                    URL = "http://sunbowgames.com/",
                };
                www.Show();
#elif WINDOWS
                Process.Start("http://sunbowgames.com/");
#endif
            }
            else if (sender == exitButton)
            {
                if (BubbleBreak.IsTestVersion)
                {
                    Proxy.SetMode<TrialExitScreen>();
                }
                else
                {
                    BubbleBreak.Instance.Exit();
                }
            }
        }
        void FlagCheckChanged(object sender, EventArgs e)
        {
            if (!(sender as RadioButton).Checked)
                return;

            int prevIdx = langIndex;
            
            for (int i = 0; i < flagButtons.Length; i++)
            {
                if (flagButtons[i].Equals(sender))
                {
                    langIndex = i;
                    break;
                }
            }
            if (langIndex != prevIdx)
            {
                flagButtons[prevIdx].Checked = false;
                return;
            }
            Loc.Language = langOrder[langIndex];
            ReloadTexts();

        }

        void StartGame()
        {
            BubbleBreak.Instance.Achievements.ClearGameAchievements();
            BubbleBreak.Instance.PlayerScoreEntryInfo = null;
            BubbleBreak.Instance.ModeProxy.GetMode<Modes.Game>().InitOnEnter = true;
            BubbleBreak.Instance.ModeProxy.SetMode<Modes.Game>();
            LevelManager.Instance.Start(0);
        }
        void ExitGame()
        {
            BubbleBreak.Instance.Exit();
        }

        void HighScores()
        {
            Proxy.SetMode<HighscoreMenuMode>();
            Proxy.GetMode<HighscoreMenuMode>().ShowLocalScore();
        }


        protected internal override void Draw(GameTime gameTime, Microsoft.Xna.Framework.Graphics.SpriteBatch spriteBatch)
        {
            Frame frame = menuSheet.GetFrame("default", "big_bubble");
            Frame line = menuSheet.GetFrame("default", "line");


            spriteBatch.Begin();

            menuSheet.Draw(spriteBatch, line, new Vector2(410, 100), 0, new Vector2(3f, 1));
            menuSheet.Draw(spriteBatch, line, new Vector2(410, 175), 0, new Vector2(3f, 1));
            menuSheet.Draw(spriteBatch, line, new Vector2(410, 250), 0, new Vector2(3f, 1));
            //menuSheet.Draw(spriteBatch, line, new Vector2(410, 325), 0, new Vector2(3f, 1));
            menuSheet.Draw(spriteBatch, line, new Vector2(410, 430), 0, new Vector2(3f, 1));

            menuSheet.Draw(spriteBatch, menuSheet.GetFrame("default", "small_bubble"), new Vector2(442, -10), 0, 1, colors[0].CurrentColor);
            menuSheet.Draw(spriteBatch, menuSheet.GetFrame("default", "small_bubble"), new Vector2(442, 490), 0, 1, colors[0].CurrentColor);

            menuSheet.Draw(spriteBatch, menuSheet.GetFrame("default", "logo"), new Vector2(475, 5), 0, 1);
            menuSheet.Draw(spriteBatch, menuSheet.GetFrame("default", "sunbow"), new Vector2(475, 480), 0, 1);

            menuSheet.Draw(spriteBatch, frame, new Vector2(1190, -200), MathHelper.PiOver2, 1, colors[1].CurrentColor);
            menuSheet.Draw(spriteBatch, frame, new Vector2(-330, 0), 0, 1, colors[2].CurrentColor);
            menuSheet.Draw(spriteBatch, frame, new Vector2(-680, 0), 0, 1, colors[3].CurrentColor);

            for(int i = 0; i < langOrder.Length; i++)
            {
                menuSheet.Draw(spriteBatch, menuSheet.CalculateFrame("flags", i), new Vector2(20, 100 + i * 60), 0, 1);
            }


            //menuSheet.Draw(spriteBatch, menuSheet.GetFrame("default", "test"), new Vector2(100, 240), 0, new Vector2(100, 1), Color.Red);
            spriteBatch.End();

            base.Draw(gameTime, spriteBatch);
        }
    }
}
