﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util.Sprites;
using starLiGHT.GUI.Widgets;

namespace Bubblebreak.Menu
{
    public class CreditsMenuMode : MenuMode
    {
        static readonly Color TitleColor = Color.Yellow;
        static readonly Color NameColor = Color.Wheat;

        Label creatorTitle, creatorName,
            translatorTitles, translatorNames,
            testerTitle, testerNames,
            thanksToTitle, thanksToNames,
            madeWithTitle,
            contentByTitle, contentByNames;
        

        public CreditsMenuMode(GuiManager manager)
            : base(manager, "AboutMenuMode")
        {
            creatorTitle = CreateLabel("creator_title", new Vector2(250, 10), new Vector2(300, 150),
                BubbleBreak.FontSmall, TextAlignment.Center, true, TitleColor);
            creatorName = CreateLabel("creator_name", new Vector2(250, 175), new Vector2(300, 50),
                BubbleBreak.FontSmall, TextAlignment.Center, true, NameColor);

            translatorTitles = CreateLabel("translator_title", new Vector2(250, 260), new Vector2(300, 150),
                BubbleBreak.FontSmall, TextAlignment.Center, true, TitleColor);
            translatorNames = CreateLabel("translator_name", new Vector2(305, 300), new Vector2(300, 140),
                BubbleBreak.FontSmall, TextAlignment.Left, true, NameColor);
            translatorNames.ClauseExtraSpacing = 4;

            testerTitle = CreateLabel("tester_title", new Vector2(5, 10), new Vector2(200, 50),
                BubbleBreak.FontSmall, TextAlignment.Left, true, TitleColor);
            testerNames = CreateLabel("tester_names", new Vector2(10, 40), new Vector2(175, 200),
                BubbleBreak.FontSmall, TextAlignment.Left, true, NameColor);
            testerNames.ClauseExtraSpacing = 4;

            thanksToTitle = CreateLabel("thanks_title", new Vector2(5, 260), new Vector2(200, 50),
                BubbleBreak.FontSmall, TextAlignment.Left, true, TitleColor);
            thanksToNames = CreateLabel("thanks_names", new Vector2(10, 290), new Vector2(175, 200),
                BubbleBreak.FontSmall, TextAlignment.Left, true, NameColor);
            thanksToNames.ClauseExtraSpacing = 4;

            madeWithTitle = CreateLabel("made_with_title", new Vector2(620, 260), new Vector2(180, 50),
                BubbleBreak.FontSmall, TextAlignment.Center, true, TitleColor);

            contentByTitle = CreateLabel("content_by_title", new Vector2(600, 10), new Vector2(200, 50),
                BubbleBreak.FontSmall, TextAlignment.Right, true, TitleColor);
            contentByNames = CreateLabel("content_by_names", new Vector2(600, 40), new Vector2(200, 50),
                BubbleBreak.FontSmall, TextAlignment.Right, true, NameColor);
            contentByNames.ClauseExtraSpacing = 4;

            CreateBackButton();

            for(int i = 0; i < 8; i++)
                colors.Add(new ColorChanger(2, 0.7f));
        }

        protected internal override void Enter()
        {
            creatorTitle.Text = Loc["creator_title"];// "Concept, Coding, Graphics Design, Music, Sound Editing\n\nby";
            creatorName.Text = "Salomon Zwecker";

            translatorTitles.Text = Loc["translation"];
            translatorNames.Text = "Salomon Zwecker\nSonja Dirschl\nJavier Gonzalez Villarejo\nErnst Zwecker";

            testerTitle.Text = Loc["tester"];
            testerNames.Text = "Christoph Büssecker\nQuirin Sammeck";

            thanksToTitle.Text = Loc["thanks"];
            thanksToNames.Text = "Mahdi Khodadadi Fard";

            madeWithTitle.Text = Loc["made_with"];

            contentByTitle.Text = Loc["content_by"];
            contentByNames.Text = "easyvectors .com\nfreesound .org\nfonts101 .com";

            base.Enter();
        }

        protected internal override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Frame frame = menuSheet.GetFrame("default", "big_bubble");
            Frame line = menuSheet.GetFrame("default", "line");
            int cidx = 0;
            
            spriteBatch.Begin();

            // center
            menuSheet.Draw(spriteBatch, frame, new Vector2(0, -244), 0, Vector2.One, colors[cidx++].CurrentColor, SpriteEffects.FlipVertically, 0f);
            menuSheet.Draw(spriteBatch, frame, new Vector2(0, 244), 0, 1, colors[cidx++].CurrentColor);

            // left
            menuSheet.Draw(spriteBatch, frame, new Vector2(-550, -244), 0, Vector2.One, colors[cidx++].CurrentColor, SpriteEffects.FlipVertically, 0f);
            menuSheet.Draw(spriteBatch, frame, new Vector2(-550, 244), 0, 1, colors[cidx++].CurrentColor);
            
            // right
            menuSheet.Draw(spriteBatch, frame, new Vector2(550, -244), 0, Vector2.One, colors[cidx++].CurrentColor, SpriteEffects.FlipVertically, 0f);
            menuSheet.Draw(spriteBatch, frame, new Vector2(550, 244), 0, 1, colors[cidx++].CurrentColor);

            #region Flags
            FrameSet flags = menuSheet.GetFrameSet("flags");
            menuSheet.Draw(spriteBatch, flags.CalculateFrame(1), new Vector2(248, 310), 0, 0.75f);
            menuSheet.Draw(spriteBatch, flags.CalculateFrame(0), new Vector2(276, 310), 0, 0.75f);

            menuSheet.Draw(spriteBatch, flags.CalculateFrame(4), new Vector2(276, 338), 0, 0.75f);

            menuSheet.Draw(spriteBatch, flags.CalculateFrame(3), new Vector2(276, 366), 0, 0.75f);

            menuSheet.Draw(spriteBatch, flags.CalculateFrame(2), new Vector2(276, 418), 0, 0.75f);

            #endregion

            menuSheet.Draw(spriteBatch, menuSheet.GetFrame("default", "libraries"), new Vector2(625, 300), 0, 1);
            menuSheet.Draw(spriteBatch, menuSheet.GetFrame("default", "thanks"), new Vector2(30, 380), 0, 1);

            spriteBatch.End();
            
            
            base.Draw(gameTime, spriteBatch);
        }
    }
}
