﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI;
using Sunbow.Highscore;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util.Sprites;
using Microsoft.Xna.Framework;
using Sunbow.Util.IO;

namespace Bubblebreak.Menu
{
    public class HighscoreMenuMode : MenuMode
    {
        const int ACHIEVEMENT_PAGE_COUNT = 3;
        static readonly Color subtitleTextColor = Color.LightGray;

        List<Ball> AchivementSmilies = new List<Ball>();

        LocalHighscore localScore;
        
        TableLayoutPanel mainScorePanel, rankScorePanel, achievementPanel;

        Label title, description, rankLabel, onlineLabel;
        Button onlineScoreButton, onlineSubmitRetryBtn,
            achievementNextBtn, achievementPrevBtn;
        CheckButton localScoreBtn, achievementScoreBtn, 
            todayScoreBtn, weekScoreBtn, monthScoreBtn, allTimeScoreBtn;

        RadioButtonController scoreMenuButtonController, achievementSelectionController;
       
        ContainerWidget achievementDescription;
        Label achievementDescriptionText;

        ScoreRequester requester;

        TextBox nameBox;

        bool online;
        int achievementPageIndex;

        public HighscoreMenuMode(GuiManager manager)
            : base(manager, "Highscore")
        {
            SpriteFont smallFont = BubbleBreak.FontSmall;

            float[] columnSizes = new float[] { 85, 5, 200, 5, 125 };
            float[] rowSizes = new float[] { 20 };

            localScore = new LocalHighscore(@"high.score");
            mainScorePanel = new TableLayoutPanel(manager, Name + "_main_score_table", 5, 21,
                columnSizes, rowSizes)
                {
                    Location = new Vector2(255, 50),
                };
            for (int i = 0; i < mainScorePanel.RowCount - 1; i++)
            {
                mainScorePanel[0, i] = new Label(manager, Name + "_mainScoreRank_" + i) { Font = smallFont, TextAlignment = TextAlignment.Right };
                mainScorePanel[2, i] = new Label(manager, Name + "_mainScoreName_" + i) { Font = smallFont };
                mainScorePanel[4, i] = new Label(manager, Name + "_mainScorePoints_" + i) { Font = smallFont, TextAlignment = TextAlignment.Right };
            }
            container.Widgets.Add(mainScorePanel);

            rankScorePanel = new TableLayoutPanel(manager, Name + "_online_rank_table", 5, 6,
                columnSizes, rowSizes)
                {
                    Location = new Vector2(255, 300),
                };
            for (int i = 0; i < rankScorePanel.RowCount - 1; i++)
            {
                rankScorePanel[0, i] = new Label(manager, Name + "_rankScoreRank_" + i) { Font = smallFont, TextAlignment = TextAlignment.Right };
                rankScorePanel[2, i] = new Label(manager, Name + "_rankScoreName_" + i) { Font = smallFont };
                rankScorePanel[4, i] = new Label(manager, Name + "_rankScorePoints_" + i) { Font = smallFont, TextAlignment = TextAlignment.Right };
            }
            container.Widgets.Add(rankScorePanel);

            achievementSelectionController = new RadioButtonController();
            achievementSelectionController.AllowNoCheckedControl = true;
            achievementSelectionController.SelectionChanged += achievementSelectionController_SelectionChanged;
            achievementPanel = new TableLayoutPanel(manager, Name + "_achievement_table", 1, 11, new float[] { 395 }, new float[] { 30 })
            {
                Location = new Vector2(315, 50),
            };
            for (int i = 0; i < achievementPanel.RowCount - 1; i++)
            {
                CheckButton cb = new CheckButton(manager, Name + "_achievement_title" + i, "TinyButton") { Font = smallFont, Text = "Blablabla" };
                achievementPanel[0, i] = cb;
                achievementSelectionController.Add(cb);

                AchivementSmilies.Add(new Ball() { Position = new Vector2(300, 64 + i * 30) });

            }
            container.Widgets.Add(achievementPanel);

            achievementDescription = new ContainerWidget(manager, Name + "_achievement_description_panel", "ToolTipPanel")
            {
                Location = new Vector2(350, 350),
                Size = new Vector2(285, 130),

            };
            achievementDescriptionText = new Label(manager, Name + "_achievement_description_lbl")
            {
                //Location = new Vector2(360, 360),
                Size = new Vector2(260, 150),
                Font = BubbleBreak.FontSmall,
                WrapText = true,
                TextAlignment = starLiGHT.GUI.TextAlignment.Center,
            };
            achievementDescription.Widgets.Add(achievementDescriptionText);
            container.Widgets.Add(achievementDescription);

            title = new Label(manager, Name + "_title")
            {
                Size = new Vector2(400, 40),
                Location = new Vector2(280, 10),
                WrapText = true,
            };
            container.Widgets.Add(title);

            onlineLabel = CreateLabel("online_label", new Vector2(20, 160), new Vector2(200, 40),
                BubbleBreak.FontSmall, TextAlignment.Left, false, subtitleTextColor);


            rankLabel = CreateLabel("rank_label", new Vector2(280, 275), new Vector2(300, 30),
                BubbleBreak.FontSmall, TextAlignment.Left, false, subtitleTextColor);

            nameBox = new TextBox(manager, Name + "_textBox")
            {
                Location = new Vector2(400, 250),
                Size = new Vector2(200, 36),
                //  Font = BubbleBreak.FontSmall,
            };
            container.Widgets.Add(nameBox);

            description = new Label(manager, Name + "_descr")
            {
                Location = new Vector2(280, 100),
                Size = new Vector2(400, 300),
                Font = BubbleBreak.FontSmall,
                TextAlignment = TextAlignment.Justify,
                WrapText = true,
            };
            container.Widgets.Add(description);


            onlineScoreButton = CreateButton("online_button", new Vector2(5, 200), new Vector2(250, 63), (s, a) => RequestWeb());
            onlineSubmitRetryBtn = CreateButton("retry_button", new Vector2(375, 400), new Vector2(250, 63), (s, a) => RequestWeb());

            localScoreBtn = CreateCheckButton("local_score_button", new Vector2(5, 10), new Vector2(250, 63), (s, a) => ShowLocalScore());
            achievementScoreBtn = CreateCheckButton("achievement_button", new Vector2(5, 80), new Vector2(250, 63), (s, a) => ShowAchievements());

            todayScoreBtn = CreateCheckButton("today_score_button", new Vector2(5, 200), new Vector2(250, 63), (s, a) => FillOnlineData("day"));
            weekScoreBtn = CreateCheckButton("week_score_button", new Vector2(5, 270), new Vector2(250, 63), (s, a) => FillOnlineData("week"));
            monthScoreBtn = CreateCheckButton("month_score_button", new Vector2(5, 340), new Vector2(250, 63), (s, a) => FillOnlineData("month"));
            allTimeScoreBtn = CreateCheckButton("all_time_score_button", new Vector2(5, 410), new Vector2(250, 63), (s, a) => FillOnlineData("all"));

            scoreMenuButtonController = new RadioButtonController(localScoreBtn, achievementScoreBtn, todayScoreBtn, weekScoreBtn, monthScoreBtn, allTimeScoreBtn);

            achievementPrevBtn = CreateButton("help_prev", new Vector2(285, 400), new Vector2(63, 63),
                (s, a) => SetAchievementPageIndex((achievementPageIndex + ACHIEVEMENT_PAGE_COUNT - 1) % ACHIEVEMENT_PAGE_COUNT),
                "PreviousButton");
            achievementNextBtn = CreateButton("help_next", new Vector2(640, 400), new Vector2(63, 63),
                (s, a) => SetAchievementPageIndex((achievementPageIndex + 1) % ACHIEVEMENT_PAGE_COUNT),
                "NextButton");


            CreateBackButton();

            colors.Add(new ColorChanger(2, 0.7f));
            colors.Add(new ColorChanger(2, 0.7f));
        }


        protected internal override void Enter()
        {
            bool played = BubbleBreak.Instance.PlayerScoreEntryInfo != null;
            online = played && BubbleBreak.Instance.PlayerScoreEntryInfo.OnlineSubmitted;

            SetAchievementPageIndex(0);

            title.Text = Loc["local_score_title"];

            onlineScoreButton.Text = (played) ? Loc["submit_score_online"] : Loc["view_score_online"];
            onlineSubmitRetryBtn.Text = Loc["retry_button"];

            localScoreBtn.Text = Loc["btn_local_score"];
            achievementScoreBtn.Text = Loc["btn_achievements"];

            onlineLabel.Text = Loc["online"];

            todayScoreBtn.Text = Loc["btn_today_score"];
            weekScoreBtn.Text = Loc["btn_week_score"];
            monthScoreBtn.Text = Loc["btn_month_score"];
            allTimeScoreBtn.Text = Loc["btn_all_time_score"];

            rankLabel.Text = Loc["your_ranking"];

            if (BubbleBreak.Instance.PlayerScoreEntryInfo != null)
                nameBox.Text = BubbleBreak.Instance.PlayerScoreEntryInfo.PlayerName;
            else
                nameBox.Text = BubbleBreak.PlayerName;

            onlineScoreButton.Visible = !online;

            todayScoreBtn.Visible =
            weekScoreBtn.Visible =
            monthScoreBtn.Visible =
            allTimeScoreBtn.Visible = online;

            description.Visible =
            nameBox.Visible =
            onlineSubmitRetryBtn.Visible = false;

            rankScorePanel.Visible = 
            rankLabel.Visible = played;

            localScoreBtn.Checked = true;

            base.Enter();
        }

        void HideAll()
        {
            mainScorePanel.Visible = 
            rankScorePanel.Visible =
            achievementPanel.Visible =
            rankLabel.Visible =
            nameBox.Visible = 
            onlineSubmitRetryBtn.Visible =
            achievementNextBtn.Visible =
            achievementPrevBtn.Visible =
            achievementDescription.Visible =
            description.Visible = false;

        }

        private void ShowAchievements()
        {
            HideAll();

            achievementNextBtn.Visible =
            achievementPrevBtn.Visible =
            achievementPanel.Visible = true;

            title.Text = Loc["achievements_title"];

            if (achievementSelectionController.CurrentSelection != null)
                achievementSelectionController.CurrentSelection.Checked = false;
        }

        public void SetAchievementPageIndex(int index)
        {
            achievementPageIndex = index;

            int start = 10 * index;
            for (int i = 0; i < 10; i++)
            {
                if ((start + i) >= (int)Achievements.AchievementType._Count)
                {
                    achievementPanel[0, i].Text = "";
                    AchivementSmilies[i].Color = Color.Transparent;
                    AchivementSmilies[i].SetSmilieIcon(Achievements.AchievementType.undefined);
                }
                else
                {
                    Achievements.AchievementType a = (Achievements.AchievementType)(start + i);
                    achievementPanel[0, i].Text = Loc[a.ToString()];

                    if (BubbleBreak.Instance.Achievements.IsAchieved(a))
                    {
                        AchivementSmilies[i].Color = Color.LightGreen;
                        AchivementSmilies[i].SetSmilieIcon(a);
                    }
                    else
                    {
                        AchivementSmilies[i].Color = Color.White;
                        AchivementSmilies[i].SetSmilieIcon(Achievements.AchievementType.undefined);
                    }
                }
            }

            if(achievementSelectionController.CurrentSelection != null)
                achievementSelectionController.CurrentSelection.Checked = false;
        }
        void achievementSelectionController_SelectionChanged(ICheckable obj)
        {
            if (obj == null)
            {
                achievementDescription.Visible = false;
            }
            else
            {

                int idx = 10 * achievementPageIndex + int.Parse(((Widget)obj).Name.Remove(0, (Name + "_achievement_title").Length));
                if (idx < (int)Achievements.AchievementType._Count)
                {
                    achievementDescription.Visible = true;
                    achievementDescriptionText.Text = Loc[((Achievements.AchievementType)idx) + "_description"];
                }
            }
        }

        private void RequestWeb()
        {
            HideAll();
            description.Visible = true;

            if (BubbleBreak.IsTestVersion)
            {
                description.Text = Loc["not_in_trial"];
                title.Text = Loc["not_in_trial_title"];
                return;
            }
            onlineScoreButton.Visible = false;

            description.Text = Loc["connecting"];
            title.Text = Loc["connecting_title"];

            var scoreInfo = BubbleBreak.Instance.PlayerScoreEntryInfo;

            if(scoreInfo != null)
                scoreInfo.PlayerName = nameBox.Text;

            ScoreQuery query = (scoreInfo != null) 
                ? new ScoreQuery(scoreInfo.PlayerName, scoreInfo.Score, VersionInfo.ProductReleaseInfo, VersionInfo.Version) 
                : new ScoreQuery();

            ScoreRequester send = new ScoreRequester("http://www.liquidfirearts.com/sunbow/bubblebreak/highscore.php", query);

            send.RequestFailed += send_RequestFailed;
            send.RequestSucceeded += send_RequestSucceeded;
            send.CreateWebRequest();
        }
        void send_RequestFailed(WebRequester obj, Exception ex, RequestErrorCause cause)
        {
            onlineScoreButton.Visible = false;
            description.Visible = 
            onlineSubmitRetryBtn.Visible = true;

            nameBox.Visible = cause == RequestErrorCause.ForbiddenChar;

            title.Text = Loc["error_title"];
            description.Text = Loc["online_error_" + cause.ToString().ToLower()];
        }
        void send_RequestSucceeded(WebRequester obj)
        {
            if(BubbleBreak.Instance.PlayerScoreEntryInfo != null)
                BubbleBreak.Instance.PlayerScoreEntryInfo.OnlineSubmitted = true;

            HideAll();

            todayScoreBtn.Visible =
            weekScoreBtn.Visible =
            monthScoreBtn.Visible =
            allTimeScoreBtn.Visible = true;

            allTimeScoreBtn.Checked = true;

            requester = obj as ScoreRequester;

            FillOnlineData("all");
            
        }

        void FillOnlineData(string key)
        {
            HideAll();

            mainScorePanel.Visible = true;

            title.Text = Loc["online_score_" + key];

            ScoreList top = requester.GetScoreList("top_" + key);
            ScoreList rank = requester.GetScoreList("rank_" + key);

            rankScorePanel.Visible =
            rankLabel.Visible = rank != null;

            if (top != null)
                FillScore(top.GetScoreIterator(), mainScorePanel, top.PlayerRank);
            else
                FillEmpty(mainScorePanel, 0);

            if (rank != null)
                FillScore(rank.GetScoreIterator(), rankScorePanel, rank.PlayerRank);
            else
                FillEmpty(rankScorePanel, 0);

        }

        public bool AddScore(string playerName, int score)
        {
            int trials = 3;

            while (trials >= 0)
            {
                try
                {
                    localScore.AddNewScore(playerName, score);
                    localScore.ScoreTable.Save();

                    break;
                }
                catch (Exception)
                {
                    localScore.ScoreTable.Clear(false);

                    trials--;
                }
            }
            return trials == 3;
        }

        public void ShowLocalScore()
        {
            HideAll();
            mainScorePanel.Visible = true;

            title.Text = Loc["local_score_title"];

            if (!string.IsNullOrEmpty(localScore.PlayerRank))
            {
                int rank;
                if (SystemParseMethods.TryParseInt32(localScore.PlayerRank, out rank) && rank != -1)
                {
                    rankScorePanel.Visible = true;
                    rankLabel.Visible = true;

                    FillScore(localScore.GetScoreIterator(1, 10), mainScorePanel, localScore.PlayerRank);
                    FillScore(localScore.GetScoreIterator(Math.Max(1, rank - 2), 5), rankScorePanel, localScore.PlayerRank);
                    return;
                }
            }
            FillScore(localScore.GetScoreIterator(1, 20), mainScorePanel, localScore.PlayerRank);
        }

        public void ShowLocalsCorrupt()
        {
            HideAll();
            description.Visible = true;

            title.Text = Loc["error_title"];
            description.Text = Loc["locals_corrupt"];
        }

        static void FillScore(IEnumerable<string[]> entryIterator, TableLayoutPanel panel, string playerRank)
        {
            SpriteFont font = BubbleBreak.FontSmall;
            int idx = 0;
            foreach (string[] scoreData in entryIterator)
            {
                panel[0, idx].Text = scoreData[0] + ".";
                panel[2, idx].Text = scoreData[1];
                panel[4, idx].Text = scoreData[2];

                panel[0, idx].ForeColor = 
                panel[2, idx].ForeColor = 
                panel[4, idx].ForeColor = (playerRank == scoreData[0]) ? Color.Red : Color.White;

                idx++;
                if (idx >= panel.RowCount)
                    break;
            }

            FillEmpty(panel, idx);
        }

        static void FillEmpty(TableLayoutPanel panel, int startIndex)
        {

            for (int i = startIndex; i < panel.RowCount - 1; i++)
            {
                panel[0, i].Text = string.Empty;
                panel[2, i].Text = string.Empty;
                panel[4, i].Text = string.Empty;
            }
        }


        protected internal override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Frame bigBubble = menuSheet.GetFrame("default", "big_bubble");
            Frame bigDot = menuSheet.GetFrame("default", "big_dot");
            Frame smallDot = menuSheet.GetFrame("default", "small_dot");

            spriteBatch.Begin();

            menuSheet.Draw(spriteBatch, bigBubble, new Vector2(276, 600), -MathHelper.PiOver2, new Vector2(1, 0.9f), colors[0].CurrentColor);
            menuSheet.Draw(spriteBatch, bigBubble, new Vector2(-210, 600), -MathHelper.PiOver2, 1, colors[1].CurrentColor);

            if (this.achievementPanel.Visible)
            {
                for (int i = 0; i < 3; i++)
                {
                    Frame f = (i == achievementPageIndex) ? bigDot : smallDot;
                    menuSheet.Draw(spriteBatch, f, new Vector2(460 + i * 40, 425), 0, 1);
                }

                foreach (Ball b in AchivementSmilies)
                    b.DrawAsIcon(spriteBatch, 1, 1);
            }

            spriteBatch.End();


            base.Draw(gameTime, spriteBatch);
        }

        public void ReInitializeLocalScores()
        {
            this.localScore = new LocalHighscore(@"high.score");
        }
    }
}
