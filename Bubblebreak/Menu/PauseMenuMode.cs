﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using starLiGHT.GUI;
using Microsoft.Xna.Framework;
using starLiGHT.GUI.Widgets;
using Microsoft.Xna.Framework.Graphics;
using Bubblebreak.GameState;
using Sunbow.Util;
using Sunbow.Util.Sprites;

namespace Bubblebreak.Menu
{
    public class PauseMenuMode : MenuMode
    {
        Button continueBtn, backToMenuBtn;
        Label title;
        
        public PauseMenuMode(GuiManager manager)
            : base(manager, "PauseMenuMode")
        {
            title = new Label(manager, Name + "_title")
            {
                Location = new Vector2(250, 50),
                Size = new Vector2(300, 50),
                TextAlignment = TextAlignment.Center,
            };
            container.Widgets.Add(title);

            continueBtn = CreateButton("continue_game", new Vector2(250, 150), new Vector2(300, 63), ContinueGame);
            backToMenuBtn = CreateButton("back_to_menu", new Vector2(250, 250), new Vector2(300, 63), (s, a) => Proxy.SetMode<MainMenuMode>());

            colors.Add(new ColorChanger(2, 0.7f));

        }

        protected internal override void Enter()
        {
            title.Text = Loc["pause"];
            continueBtn.Text = Loc["continue_game"];
            backToMenuBtn.Text = Loc["back_to_menu"];

            base.Enter();
        }

        void ContinueGame(object sender, EventArgs args)
        {
            ModeProxy mainProxy = BubbleBreak.Instance.ModeProxy;
            mainProxy.GetMode<Modes.Game>().InitOnEnter = false;
            mainProxy.SetMode<Modes.Game>();
            mainProxy.GetMode<Modes.Game>().InitOnEnter = true;
        }

        protected internal override void Draw(GameTime gameTime, SpriteBatch spriteBatch)
        {

            Frame frame = menuSheet.GetFrame("default", "big_bubble");


            spriteBatch.Begin();
            menuSheet.Draw(spriteBatch, frame, Vector2.Zero, 0, 1, colors[0].CurrentColor);
            spriteBatch.End();
            
            
            base.Draw(gameTime, spriteBatch);
        }
    }
}
