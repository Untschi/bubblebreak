using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using Bubblebreak.SplashScreen;
using Bubblebreak.GameState;
using Sunbow.Util.Sprites;
using Sunbow.Util.IO;
using Bubblebreak.Menu;
//using Microsoft.Xna.Framework.Net;
//using Microsoft.Xna.Framework.Storage;
using Bubblebreak.Achievements;

namespace Bubblebreak
{
    public class BubbleBreak : Microsoft.Xna.Framework.Game
    {
        public static float Scale = 2f;
        public static readonly Vector2 Shift = new Vector2(160,0);

        public static Vector2 Transform(Vector2 position)
        {
            return new Vector2(
                (int)Math.Round(Shift.X + Scale * position.X),
                (int)Math.Round(Shift.Y + Scale * position.Y));
        }
        public static Vector2 Transform(Vector2 position, bool flip, float sizeShiftH)
        {
            Vector2 pos = new Vector2(
                (int)Math.Round(Shift.X + Scale * position.X),
                (int)Math.Round(Shift.Y + Scale * position.Y));

            if (flip)
            {
                pos = new Vector2(800 - pos.X - sizeShiftH, pos.Y);
            }

            return pos;
        }

        public static BubbleBreak Instance { get; private set; }

        public static Random Random { get; private set; }

        public static SpriteFont Font { get; private set; }
        public static SpriteFont FontSmall { get; private set; }

        public static bool SensorInputEnabled { get { return sensorInputEnabled; } set { sensorInputEnabled = value; } }
        static bool sensorInputEnabled = true;

        public static bool PlayingFieldFlipped { get { return playingFieldFlipped; } set { playingFieldFlipped = value; } }
        static bool playingFieldFlipped = true;

        public static string PlayerName { get { return playerName; } set { playerName = value; } }
        static string playerName = string.Empty;

        GraphicsDeviceManager _Graphics;
        SpriteBatch _SpriteBatch;

        BubblebreakInput _Input;


        public ModeProxy ModeProxy { get; private set; }
        public Sunbow.Util.Localization Loc { get; private set; }
        internal PlayerScoreEntryInfo PlayerScoreEntryInfo { get; set; }


        public AchievementManager Achievements { get; private set; }

#if !ZUNE
        Texture2D _CursorTex;
#endif
        public SpriteSheet GameSheet { get; private set; }
        public FixedFrameSet BallFrames { get; private set; }

        public GameTime CurrentUpdateTime;

        public static bool IsTestVersion { get; private set; }

        public BubbleBreak()
        {
            Instance = this;

            Random = new Random();

            _Graphics = new GraphicsDeviceManager(this);
            //_Graphics.PreferredBackBufferWidth = 240;
            //_Graphics.PreferredBackBufferHeight = 320;
            //_Graphics.ApplyChanges();

            Content.RootDirectory = "Content";

            _Input = new BubblebreakInput(new Point(120, 120));

            Activated += BubbleBreak_Activated;
        }

        void BubbleBreak_Activated(object sender, EventArgs e)
        {
            IsTestVersion = 
#if WINDOWS
                Program.TrialModeSimulation;
#else
                Guide.IsTrialMode;
#endif
        }

        protected override void LoadContent()
        {
            IsTestVersion =
#if WINDOWS
                Program.TrialModeSimulation;
#else
                Guide.IsTrialMode;
#endif            
            SystemParseMethods.RegisterCommonSystemTypeParseMethods();

            ModeProxy = new GameState.ModeProxy(this);
            ModeProxy.Register(new Modes.SplashScreen());

            ModeProxy.SetMode<Modes.SplashScreen>();

            _SpriteBatch = new SpriteBatch(GraphicsDevice);
            base.LoadContent();
        }
        protected override void UnloadContent()
        {
            //particleSystemManager.DestroyAndRemoveAllParticleSystems();
            base.UnloadContent();
        }

        public void LoadEverything()
        {
            Loc = new Sunbow.Util.Localization(new Table(@"Content\Loc.csv", false)); //Content.Load<Table>("Loc"));
            Achievements = new AchievementManager();

            Font = Content.Load<SpriteFont>("Gumbo23");
            FontSmall = Content.Load<SpriteFont>("Gumbo20");

            //particleSystemManager = new ParticleSystemManager();
            //particleSystemManager.UpdatesPerSecond = 30;
            //particleSystem = new DefaultSpriteParticleSystem(this);
            //particleSystemManager.AddParticleSystem(particleSystem);
            //particleSystemManager.AutoInitializeAllParticleSystems(GraphicsDevice, Content, _SpriteBatch);


            GameSheet = new SpriteSheet(Content.Load<Texture2D>("GameSheet"));
            BallFrames = new FixedFrameSet("balls", 28, 28, 0, 0, Point.Zero, 36, 0, 64, ReadDirection.LeftToRight_NextLineBelow, new Vector2(14, 14));
            BallFrames.RegisterFrame("NormalBall", 0);
            BallFrames.RegisterFrame("MovableBall", 1);
            BallFrames.RegisterAnimation(new FrameAnimation("BombBall", 2, 2, 0.1f, RepeatLogic.Looped));
            BallFrames.RegisterAnimation(new FrameAnimation("AllColorBall", 4, 18, 0.2f, RepeatLogic.Looped));
            BallFrames.RegisterAnimation(new FrameAnimation("ColorKillerBall", 22, 3, 0.15f, RepeatLogic.Looped));
            BallFrames.RegisterAnimation(new FrameAnimation("CentralBall", 25, 5, 0.12f, RepeatLogic.Looped));
            GameSheet.RegisterFrameSet(BallFrames);

            FixedFrameSet smileFrames = new FixedFrameSet("smile", 28, 28, 0, 0, new Point(0, 28), 31, 0, 93, ReadDirection.LeftToRight_NextLineBelow, new Vector2(14, 14));
            GameSheet.RegisterFrameSet(smileFrames);

            FixedFrameSet lineFrames = new FixedFrameSet("DashedLine", 220, 3, 0, 0, new Point(0, 468), 1, 0, 12, ReadDirection.UpToDown_NextLineLeft, new Vector2(0, 1.5f));
            lineFrames.RegisterAnimation(new FrameAnimation("LineAnimation", 0, 12, 0.1f, RepeatLogic.Looped));
            GameSheet.RegisterFrameSet(lineFrames);

            FixedFrameSet bigItems = new FixedFrameSet("BigItems", 112, 56, new Point(0, 112), 2, 0, 10, ReadDirection.LeftToRight_NextLineBelow);
            bigItems.RegisterAnimation(new FrameAnimation("BigItem_ani", 0, 10, 2, RepeatLogic.Looped));
            //FixedFrameSet smallItems = new FixedFrameSet("SmallItems", 37, 18, 0, 1, new Point(0, 392), 5, 0, 10, ReadDirection.LeftToRight_NextLineBelow, new Vector2(19, 9));
            FixedFrameSet smallItems = new FixedFrameSet("SmallItems", 28, 14, 0, 0, new Point(0, 392), 2, 0, 10, ReadDirection.LeftToRight_NextLineBelow, new Vector2(14, 7));
            for (int i = 0; i < (int)ItemType._Count; i++)
            {
                bigItems.RegisterFrame(((ItemType)i).ToString(), i);
                smallItems.RegisterFrame(((ItemType)i).ToString(), i);
            }
            GameSheet.RegisterFrameSet(bigItems);
            GameSheet.RegisterFrameSet(smallItems);

            FixedFrameSet lockButtons = new FixedFrameSet("LockButtons", 71, 115, new Point(460, 137), 4, ReadDirection.LeftToRight_NextLineBelow);
            GameSheet.RegisterFrameSet(lockButtons);

            FreeFrameSet defaultFrames = new FreeFrameSet("default");
            defaultFrames.RegisterFrame("BackgroundNormal", new Frame(new Rectangle(224, 544, 800, 480), Vector2.Zero));
            defaultFrames.RegisterFrame("ColorWindow", new Frame(new Rectangle(744, 126, 127, 418), new Vector2(-169, 209)));
            defaultFrames.RegisterFrame("Cog", new Frame(new Rectangle(872, 40, 150, 504), new Vector2(-148, 252)));
            defaultFrames.RegisterFrame("ColorArc", new Frame(new Rectangle(0, 508, 149, 516), new Vector2(0, 258)));
            defaultFrames.RegisterFrame("HudBackground", new Frame(new Rectangle(224, 336, 224, 196), Vector2.Zero));
            defaultFrames.RegisterFrame("LevelScoreBackground", new Frame(new Rectangle(448, 252, 280, 280), new Vector2(140, 140)));

            defaultFrames.RegisterFrame("BounceLine", new Frame(new Rectangle(84, 392, 28, 56), new Vector2(-188, 28)));
            defaultFrames.RegisterFrame("BounceUpperLine", new Frame(new Rectangle(84, 392, 28, 28), new Vector2(-188, 28)));
            defaultFrames.RegisterFrame("BounceLowerLine", new Frame(new Rectangle(84, 420, 28, 28), new Vector2(-188, 0)));
            defaultFrames.RegisterFrame("BounceUpperArc", new Frame(new Rectangle(112, 392, 56, 28), new Vector2(-188, 28)));
            defaultFrames.RegisterFrame("BounceLowerArc", new Frame(new Rectangle(112, 420, 56, 28), new Vector2(-188, 0)));

            GameSheet.RegisterFrameSet(defaultFrames);


            ModeProxy.Register(new Modes.MainMenu());
            ModeProxy.Register(new Modes.Game());
            ModeProxy.GetMode<Modes.MainMenu>().RegisterAllMenus();

#if !ZUNE
            //_CursorTex = Content.Load<Texture2D>("Cursor");
#endif

        }

        protected override void Update(GameTime gameTime)
        {
            if (!IsActive)
                return;

            CurrentUpdateTime = gameTime;

            float seconds = (float)gameTime.ElapsedGameTime.TotalSeconds;

            _Input.Update(seconds);
            ModeProxy.Update(gameTime);

            if (!ModeProxy.IsActiveMode<Modes.SplashScreen>())
            {
                BubblebreakSound.Update();
            }
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            
            ModeProxy.Draw(gameTime, _SpriteBatch);

            base.Draw(gameTime);

            //if (FontSmall != null)
            //{
            //    _SpriteBatch.Begin();
            //    _SpriteBatch.DrawString(FontSmall, "Test version licensed to Kaspar Pohl", new Vector2(0, 450), Color.White * 0.5f);
            //    _SpriteBatch.End();
            //}
        }
    }
}
