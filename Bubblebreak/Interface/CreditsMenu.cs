﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Bubblebreak
{
    public class CreditsMenu : ButtonMenu
    {

        public CreditsMenu()
            : base("CreditsMenu")
        {
            AddButton("Back", new Action(OnButtonB), new Vector2(0, 300));
        }

        public override void OnButtonB()
        {
            MenuManager.SetActiveMenu("MainMenu");
        }

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);
            batch.DrawString(BubbleBreak.Font,
                "This Game was \n"
                + "created for the \n"
                + "Zune Contest\n"
                + "at XnaMag.de.\n\n"
                + "Idea, Graphics, \n"
                + "Sound Editing, Music\n"
                + "and Programming\n\n"
                + "      by JeReT", new Vector2(10, 70), Color.White);

        }
    }
}
