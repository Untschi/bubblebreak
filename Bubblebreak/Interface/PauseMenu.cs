﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Bubblebreak
{
    public class PauseMenu : ButtonMenu
    {

        public PauseMenu()
            : base("PauseMenu")
        {
            AddButton("Continue", new Action(OnButtonB));
            AddButton("Back to Menu", new Action(MainMenu));
        }

        void MainMenu()
        {
            MenuManager.SetActiveMenu("MainMenu");
        }
        public override void OnButtonB()
        {
            MenuManager.Sleep = true;
            LevelManager.Instance.Sleep = false;
        }

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);
            batch.DrawString(BubbleBreak.Font, "Pause", new Vector2(85, 280), Color.Gray);
        }

    }
}
