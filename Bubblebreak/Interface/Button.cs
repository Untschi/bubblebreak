﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;

namespace Bubblebreak.GUI
{
    public class Button
    {
        public string Text { get; set; }
        public Vector2 Position { get; set; }
        public event Action OnPush;

        public Button(string text)
        {
            Text = text;
        }

        public Button(string text, Action onPush)
        {
            Text = text;
            OnPush = onPush;
        }
        public Button(string text, Action onPush, Vector2 position)
        {
            Text = text;
            OnPush = onPush;
            Position = position;
        }

        internal void CallOnPush()
        {
            if (OnPush != null)
                OnPush();
        }
    }
}
