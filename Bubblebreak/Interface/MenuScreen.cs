﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;

namespace Bubblebreak
{
    public abstract class MenuScreen
    {
        protected MenuManager MenuManager { get { return MenuManager.Instance; } }

        protected MenuScreen(string name)
        {
            MenuManager._MenuScreens.Add(name, this);

            if (MenuManager._ActiveMenuScreen == null)
                MenuManager.SwitchTo(name);
        }

        public virtual void Update(float seconds) { }
        public abstract void Draw(SpriteBatch batch);

        public virtual void OnUpKey() { }
        public virtual void OnDownKey() { }
        public virtual void OnButtonA() { }
        public virtual void OnButtonB() { }
    }
}
