﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Bubblebreak
{
    public class MainMenu : ButtonMenu
    {
        public MainMenu()
            : base("MainMenu")
        {
            AddButton("Start Game", new Action(StartGame));
            AddButton("Settings", new Action(Settings));
            AddButton("Highscores", new Action(Highscore));
            AddButton("Help", new Action(Help));
            AddButton("Credits", new Action(Credits));
            AddButton("Quit", new Action(Quit), new Vector2(0, 300));

        }

        void StartGame()
        {
            MenuManager.Sleep = true;
            int lvl = (MenuManager._MenuScreens["SettingsMenu"] as SettingsMenu).StartLevel - 1;
            BubbleBreak.Instance.ModeProxy.SetMode<Modes.Game>();
            LevelManager.Instance.Start(lvl);
        }

        void Settings()
        {
            MenuManager.SwitchTo("SettingsMenu");
        }

        void Highscore()
        {
            MenuManager.SwitchTo("Highscore");
            (MenuManager._ActiveMenuScreen as HighScore).Show();
        }

        void Help()
        {
            MenuManager.SwitchTo("HelpMenu");
        }

        void Credits()
        {
            MenuManager.SwitchTo("CreditsMenu");
        }
        

        void Quit()
        {
            BubbleBreak.Instance.Exit();
        }

    }
}
