﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Bubblebreak
{
    class TextChooser
    {
        char[] _Alphabet;
        char[] _Word;

        int _WordIndex;
        int _ChosenCharNum;

        public bool IsInputLocked { get; set; }
        public event Action<string> OnTextChange;

        public TextChooser(int maximumWordLength)
        {
            _Alphabet = new char[27];
            _Alphabet[0] = '*';
            for (int i = 1; i < _Alphabet.Length; i++)
            {
                _Alphabet[i] = (char)(i + 64);
            }

            _Word = new char[maximumWordLength];
        }

        public void Reset()
        {
            _WordIndex = 0;
            for (int i = 0; i < _Word.Length; i++)
            {
                _Word[i] = '\0';
            }
            IsInputLocked = false;
        }

        public void OnBackKey()
        {
            if (!IsInputLocked)
            {
                if (_WordIndex > 0)
                {
                    _WordIndex--;
                    _Word[_WordIndex] = '\0';
                    CallOnTextChange();
                    BubblebreakSound.Play(BubblebreakSound.PopDestroy);
                }
            }
            else
            {
                IsInputLocked = false;
                OnBackKey();
            }
        }

        public void OnEnterKey()
        {
            if (!IsInputLocked)
            {
                if (_WordIndex < _Word.Length)
                {
                    if (_ChosenCharNum == 0)
                    {
                        IsInputLocked = true;
                        return;
                    }
                    _Word[_WordIndex] = _Alphabet[_ChosenCharNum];
                    _WordIndex++;
                    CallOnTextChange();
                    BubblebreakSound.Play(BubblebreakSound.PopBunch);

                    if (_WordIndex >= _Word.Length)
                    {
                        IsInputLocked = true;
                    }
                }
            }
        }

        private void CallOnTextChange()
        {
            if (OnTextChange != null)
            {
                string text = "";
                foreach (char c in _Word)
                {
                    if (c == '\0')
                        break;
                    text += c;
                }
                OnTextChange(text);
            }

        }

        public void Update(float seconds)
        {
            _ChosenCharNum = (int)(((BubblebreakInput.Instance.CurrentBarAngle 
                            + MathHelper.PiOver2 + MathHelper.Pi / _Alphabet.Length
                            + MathHelper.TwoPi) % MathHelper.TwoPi) 
                            / (MathHelper.TwoPi / _Alphabet.Length));

        }
        public void Draw(SpriteBatch batch)
        {

            Vector2 pos;
            float angle;
            for (int i = 0; i < _Alphabet.Length; i++)
            {
                angle     = i * MathHelper.TwoPi / _Alphabet.Length - MathHelper.PiOver2;

                pos = new Vector2(
                    Level.PlayfieldCenterX + (float)Math.Cos(angle) * Level.PlayfieldRadius - 8,
                    Level.PlayfieldCenterY + (float)Math.Sin(angle) * Level.PlayfieldRadius - 8);

                if(i == _ChosenCharNum)
                    if(!IsInputLocked)
                        batch.DrawString(BubbleBreak.Font, _Alphabet[i].ToString(), pos, Color.Red);
                    else
                        batch.DrawString(BubbleBreak.Font, _Alphabet[i].ToString(), pos, Color.DarkRed);
                else
                    if (!IsInputLocked)
                        batch.DrawString(BubbleBreak.Font, _Alphabet[i].ToString(), pos, Color.Yellow);
                    else
                        batch.DrawString(BubbleBreak.Font, _Alphabet[i].ToString(), pos, Color.Brown);
            }

        }
    }
}
