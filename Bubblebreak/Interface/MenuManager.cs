﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Bubblebreak
{
    
    public class MenuManager
    {
        public static MenuManager Instance { get; private set; }

        internal Dictionary<string, MenuScreen> _MenuScreens = new Dictionary<string, MenuScreen>();
        internal MenuScreen _ActiveMenuScreen;

        ColorChanger _BackgroundColor = new ColorChanger(1, 0.5f);
        internal Texture2D _Background;

        public bool Sleep;

        public MenuManager()
        {
            Instance = this;

            _Background = BubbleBreak.Instance.Content.Load<Texture2D>("MenuBG");
           
            BubblebreakInput.Instance.OnDigitalDownDown += new Action(OnDigitalDownDown);
            BubblebreakInput.Instance.OnDigitalUpDown += new Action(OnDigitalUpDown);
            BubblebreakInput.Instance.OnShootKeyUp += new Action(OnButtonA);
            BubblebreakInput.Instance.OnColorKeyUp += new Action(OnButtonB);
        }

        public void SwitchTo(string menuName)
        {
            _ActiveMenuScreen = _MenuScreens[menuName];
        }

        void OnButtonB()
        {
            if (!Sleep)
                _ActiveMenuScreen.OnButtonB();
        }

        void OnButtonA()
        {
            if (!Sleep)
                _ActiveMenuScreen.OnButtonA();
        }

        void OnDigitalUpDown()
        {
            if (!Sleep)
                _ActiveMenuScreen.OnUpKey();
        }

        void OnDigitalDownDown()
        {
            if (!Sleep)
                _ActiveMenuScreen.OnDownKey();
        }

        public void AddMenu(string name, MenuScreen menu)
        {
            _MenuScreens.Add(name, menu);
        }

        public void SetActiveMenu(string name)
        {
            _ActiveMenuScreen = _MenuScreens[name];
        }

        public void Update(float seconds)
        {
            if (!Sleep)
            {
                _ActiveMenuScreen.Update(seconds);
                _BackgroundColor.Update(seconds);
            }
        }

        public void Draw(SpriteBatch batch)
        {
            if(!Sleep)
                _ActiveMenuScreen.Draw(batch);
        }

        public void DrawBG(SpriteBatch batch)
        {
            BubbleBreak.Instance.GraphicsDevice.Clear(ColorChanger.GetInvertedColor(_BackgroundColor.CurrentColor));
            batch.Draw(_Background, Vector2.Zero, _BackgroundColor.CurrentColor);
        }
    }
}
