﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Bubblebreak
{
    public class HelpMenu : ButtonMenu
    {
        string[] _Pages;
        int _Index;

        public HelpMenu()
            : base("HelpMenu")
        {
            AddButton("< Previous Page", new Action(PreviousPage));
            AddButton("Next Page >", new Action(NextPage), new Vector2(100, 240));
            AddButton("Back", new Action(OnButtonB), new Vector2(0, 300));

            _Pages = new string[5];
            _Pages[0] = "Shoot your colored   \n"
                        + "Balls to the bunch.  \n"
                        + "3 or more Balls with \n"
                        + "the same color will  \n"
                        + "pop off the Bunch.";

            _Pages[1] = "Popped Balls will die\n"
                        + "when they collide    \n"
                        + "with the bunch again.\n"
                        + "But they get     \n"
                        + "colored again, when  \n"
                        + "they collide with    \n"
                        + "your bar.";

            _Pages[2] = "Black Balls are \n"
                        + "holding the colored  \n"
                        + "Balls at the bunch.\n"
                        + "Destroy the black\n"
                        + "ones to let all \n"
                        + "Balls free! \n"
                        + "(this is your goal)";

            _Pages[3] = "left / right:\n"
                        + "   move bar\n"
                        + "up / down:\n"
                        + "   change speed\n\n"
                        + "Analog Pad:\n"
                        + "   set direction";

            _Pages[4] = "Button A:\n"
                        + "   shoot Ball \n\n"
                        + "Button B (hold):\n"
                        + "   rotate Colors";

        }
        void PreviousPage()
        {
            _Index = (_Index + _Pages.Length - 1) % _Pages.Length;
        }
        void NextPage()
        {
            _Index = (_Index + 1) % _Pages.Length;
        }

        public override void OnButtonB()
        {
            MenuManager.SetActiveMenu("MainMenu");
        }
        
        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);
            batch.DrawString(BubbleBreak.Font, _Pages[_Index], new Vector2(5, 100), Color.White);
            batch.DrawString(BubbleBreak.Font, "Page " + (_Index + 1) + " / " + _Pages.Length, new Vector2(70, 275), Color.Gray);
            
        }
    }
}
