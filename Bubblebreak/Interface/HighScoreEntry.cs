﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bubblebreak
{
    public class HighScoreEntry : IComparable<HighScoreEntry>, IEquatable<HighScoreEntry>
    {
        public static HighScoreEntry BlankEntry { get { return new HighScoreEntry(); } }

        public string Name { get; set; }
        public int Score { get; set; }

        public HighScoreEntry()
            : this("", 0)
        {
        }
        public HighScoreEntry(int score)
            : this("? ? ?", score)
        {
        }
        public HighScoreEntry(string name, int score)
        {
            Name = name;
            Score = score;
        }

        public int CompareTo(HighScoreEntry other)
        {
            if (Score < other.Score)
                return +1;
            else if (Score > other.Score)
                return -1;

            return 0;
        }


        public bool Equals(HighScoreEntry other)
        {
            if (Name == other.Name && Score == other.Score)
                return true;
            return false;
        }
    }
}
