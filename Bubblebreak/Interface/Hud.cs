﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Sunbow.Util.Manipulation;
using Sunbow.Util.Sprites;
using Sunbow.Util;

namespace Bubblebreak
{
    public class Hud
    {

        static readonly float RIGHT_BORDER = 310;
        static readonly Vector2 ITEM_POS = new Vector2(-69, 105);
        static readonly Vector2 TEXT_POS = new Vector2(-75, 5);
        static readonly Vector2 SCORE_POS = new Vector2(27, 19);
        static readonly Vector2 TIMER_POS = new Vector2(5, 47);
        static readonly Vector2 LEVEL_POS = new Vector2(-5, 75);


        LevelStats LevelStats { get; set; }

        public Timer Timer { get; private set; }

        Frame backgroundFrame, currentItemFrame;
        FrameSet lockButtons;
        SinusSingleAnimator itemAnimator;

        bool isVisible = true;

        string score, time, level;

        public Hud()
        {
            Timer = new Timer();
            itemAnimator = new SinusSingleAnimator(7) { Scale = -0.5f, Offset = 0.5f };
            backgroundFrame = BubbleBreak.Instance.GameSheet.GetFrame("default", "HudBackground");
            lockButtons = BubbleBreak.Instance.GameSheet.GetFrameSet("LockButtons");
            InitLocalization();
        }

        public void Reset()
        {
            Timer.Reset();
            Timer.Play();
            LevelStats = Level.CurrentLevel.Stats;
            currentItemFrame = null;

            InitLocalization();
        }

        private void InitLocalization()
        {
            Localization loc = BubbleBreak.Instance.Loc;
            score = loc["lbl_score"];
            time = loc["lbl_time"];
            level = loc["lbl_level"];
        }

        public void Hide()
        {
            itemAnimator.Play();
            isVisible = false;
        }
        public void Show()
        {
            isVisible = true;
        }

        public void SetItem(ItemType item)
        {
            if (item == ItemType.Undefined)
            {
                currentItemFrame = null;
                return;
            }

            currentItemFrame = BubbleBreak.Instance.GameSheet.GetFrame("BigItems", item.ToString());
            itemAnimator.Reset();
            switch (item)
            {
                case ItemType.BallsFromEverywhere:
                case ItemType.ColorRandomizer:
                case ItemType.ExtraBall:
                case ItemType.ExtraColor:
                case ItemType.RandomBallToCentral:
                case ItemType.RandomBallToStone:
                    itemAnimator.Play();
                    break;
                default:
                    itemAnimator.Pause();
                    break;
            }
        }

        public void Shoot()
        {
            itemAnimator.Play();
        }


        public void Update(float seconds)
        {
            Timer.Update(seconds);
            itemAnimator.Update(seconds);
        }

        public void Draw(SpriteBatch batch)
        {
            Vector2 lockFramePosTop = new Vector2(235, 10);
            Vector2 lockFramePosBottom = new Vector2(235, 170);

            Vector2 pos, size;

            #region lock buttons

            SpriteEffects flip = (BubbleBreak.PlayingFieldFlipped) ? SpriteEffects.FlipHorizontally : SpriteEffects.None;
            int idx = (Level.CurrentLevel.ColorField.IsUserControl) ? 1 : 0;

            pos = BubbleBreak.Transform(lockFramePosBottom, BubbleBreak.PlayingFieldFlipped, 71);
            BubbleBreak.Instance.GameSheet.Draw(batch, lockButtons.CalculateFrame(idx + 0), pos, 0, 1, Color.White, flip, 0);

            pos = BubbleBreak.Transform(lockFramePosTop, BubbleBreak.PlayingFieldFlipped, 71);
            BubbleBreak.Instance.GameSheet.Draw(batch, lockButtons.CalculateFrame(idx + 2), pos, 0, 1, Color.White, flip, 0);

            #endregion

            if (currentItemFrame != null && itemAnimator.Value > 0)
            {
                pos = BubbleBreak.Transform(ITEM_POS, BubbleBreak.PlayingFieldFlipped, 112);

                BubbleBreak.Instance.GameSheet.Draw(batch, currentItemFrame, pos, 0, 1, Color.White * itemAnimator);//new Vector2(22, 210)
            }

            if (!isVisible)
                return;

            Vector2 bgPos = (BubbleBreak.PlayingFieldFlipped) ? new Vector2(576, 0) : Vector2.Zero;
            BubbleBreak.Instance.GameSheet.Draw(batch, backgroundFrame, bgPos, 0, 1, Level.CurrentLevel.BackgroundColor, Level.Flip, 0);

            string txt = string.Format("{0}: \n\n{1}: \n\n{2}: ", score, time, level);
            pos = BubbleBreak.Transform(TEXT_POS, BubbleBreak.PlayingFieldFlipped, 140);
            batch.DrawString(BubbleBreak.Font, txt, pos, Color.White);


            string scr = LevelManager.Instance.TotalScore.ToString("# ### ##0");
            size = BubbleBreak.Font.MeasureString(scr);
            pos = (BubbleBreak.PlayingFieldFlipped) ? BubbleBreak.Transform(new Vector2(312, SCORE_POS.Y)) : BubbleBreak.Transform(SCORE_POS);
            batch.DrawString(BubbleBreak.Font, scr, new Vector2(pos.X - size.X, pos.Y), Color.GreenYellow);
            
            string tm = Timer.GetTimeString();
            size = BubbleBreak.Font.MeasureString(tm);
            pos = (BubbleBreak.PlayingFieldFlipped) ? BubbleBreak.Transform(new Vector2(RIGHT_BORDER, TIMER_POS.Y)) : BubbleBreak.Transform(TIMER_POS);
            batch.DrawString(BubbleBreak.Font, tm, new Vector2(pos.X - size.X, pos.Y), Color.LightGreen);

            string lvl = LevelManager.Instance.CurrentLevelNumber.ToString();
            size = BubbleBreak.Font.MeasureString(lvl);
            pos = (BubbleBreak.PlayingFieldFlipped) ? BubbleBreak.Transform(new Vector2(RIGHT_BORDER, LEVEL_POS.Y)) : BubbleBreak.Transform(LEVEL_POS);
            batch.DrawString(BubbleBreak.Font, lvl, new Vector2(pos.X - size.X, pos.Y), Color.Yellow);
        }

        internal void Delete()
        {
            Timer = null;
        }
    }
}
