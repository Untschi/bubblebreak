﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Bubblebreak
{
    public abstract class ButtonMenu : MenuScreen
    {
        const int ButtonHeight = 30;

        protected List<GUI.Button> _Buttons = new List<GUI.Button>();
        protected int _ActiveButtonIndex = 0;

        protected ButtonMenu(string name)
            : base(name) 
        { }
       
        protected void AddButton(string text, Action onPush)
        {
            Vector2 pos = (_Buttons.Count > 0) ? _Buttons[_Buttons.Count - 1].Position : new Vector2(10, 40);
            pos.Y += ButtonHeight;

            AddButton(text, onPush, pos);
        }
        protected void AddButton(string text, Action onPush, Vector2 position)
        {
            _Buttons.Add(new GUI.Button(text, onPush, position));
        }
        public override void Update(float seconds)
        {
#if !ZUNE
            int y = (int)BubblebreakInput.Instance.GetMousePosition().Y;

            for (int i = 0; i < _Buttons.Count; i++)
            {
                if (y >= _Buttons[i].Position.Y && y < _Buttons[i].Position.Y + ButtonHeight)
                {
                    if (_ActiveButtonIndex != i)
                        BubblebreakSound.Play(BubblebreakSound.LoadBar);
                    _ActiveButtonIndex = i;
                    break;
                }
            }
#endif
        }

        public override void Draw(SpriteBatch batch)
        {
            MenuManager.DrawBG(batch);

            Color col;
            for (int i = 0; i < _Buttons.Count; i++)
            {
                col = (_ActiveButtonIndex == i) ? Color.Red : Color.Yellow;

                batch.DrawString(BubbleBreak.Font, _Buttons[i].Text, _Buttons[i].Position, col);
            }
        }


        public override void OnButtonA()
        {
            _Buttons[_ActiveButtonIndex].CallOnPush();
            BubblebreakSound.Play(BubblebreakSound.ShootBar);
        }

        public override void OnDownKey()
        {
            _ActiveButtonIndex = (_ActiveButtonIndex + 1) % _Buttons.Count;
            
#if !ZUNE
            Mouse.SetPosition((int)_Buttons[_ActiveButtonIndex].Position.X, (int)_Buttons[_ActiveButtonIndex].Position.Y + ButtonHeight / 2);
#else
            BubblebreakSound.Play(BubblebreakSound.LoadBar);
#endif
        }
        public override void OnUpKey()
        {
            _ActiveButtonIndex = (_ActiveButtonIndex + _Buttons.Count - 1) % _Buttons.Count;
            
#if !ZUNE
            Mouse.SetPosition((int)_Buttons[_ActiveButtonIndex].Position.X, (int)_Buttons[_ActiveButtonIndex].Position.Y + ButtonHeight / 2);
#else
            BubblebreakSound.Play(BubblebreakSound.LoadBar);
#endif

        }
    }
}
