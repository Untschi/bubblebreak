﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Sunbow.Util.Sprites;
using Sunbow.Util;
using Bubblebreak.Modes;
using Bubblebreak.Menu;

namespace Bubblebreak
{
    public class ScoreBoard
    {
        const float DisplayDuration = 2;
        const float Intervall = 0.0333f;
        
        bool _IsVisible, _IsGameOver;
        public bool IsVisible { get { return _IsVisible; } }
        public bool IsGameOver { get { return _IsGameOver; } }

        string title;

        int barBallScore, timeBonusScore;

        float waiter;

        List<Ball> levelAchievements = new List<Ball>();
        List<Ball> gameAchievements = new List<Ball>();

        public ScoreBoard()
        {
        }

        public void SetLevelCompleted(int level, int barBalls, LevelStats stats)
        {
            _IsGameOver = false;

            barBallScore = stats.GetBarBallScore(level, barBalls);
            timeBonusScore = stats.GetTimeBonus(level);
            waiter = DisplayDuration;

            SetupLevelAchievements();
            title = BubbleBreak.Instance.Loc["lbl_level_complete"];
        }
        public void SetEndOfTestVersion(LevelStats stats)
        {
            SetGameOver(stats);
            title = BubbleBreak.Instance.Loc["lbl_trial_over"];
        }

        public void SetGameOver(LevelStats stats)
        {
            timeBonusScore = 0;
            barBallScore = 0;
            _IsGameOver = true;

            SetupLevelAchievements();

            int idx = 0;
            float posX = (BubbleBreak.PlayingFieldFlipped) ? 60 : 740;
            gameAchievements.Clear();
            foreach (Achievements.AchievementType a in BubbleBreak.Instance.Achievements.GetGameAchievementIterator())
                gameAchievements.Add(new Ball(a, true) { Position = new Vector2(posX, 80 + 30 * idx++)});

            BubbleBreak.Instance.ModeProxy.GetMode<MainMenu>().MenuProxy.SetMode<GameOverNameBoxMode>();

#if WINDOWS
            BubbleBreak.Instance.IsMouseVisible = true;
#endif

            title = BubbleBreak.Instance.Loc["lbl_game_over"];
        }

        private void SetupLevelAchievements()
        {
            int idx = 0;
            float posX = (BubbleBreak.PlayingFieldFlipped) ? 600 : 20;
            levelAchievements.Clear();
            foreach (Achievements.AchievementType a in BubbleBreak.Instance.Achievements.GetLevelAchievementIterator())
                levelAchievements.Add(new Ball(a, true) { Position = new Vector2(posX + 30 * idx++, 50) });

            BubbleBreak.Instance.Achievements.ClearLevelList();
        }

        public void Show()
        {
            _IsVisible = true;
        }
        public void Hide()
        {
            LevelManager.Instance.TotalScore += timeBonusScore;
            LevelManager.Instance.TotalScore += barBallScore;
            timeBonusScore = 0;
            barBallScore = 0;

            _IsVisible = false;
        }

        public void Update(float seconds)
        {
            if (!_IsVisible)
                return;

            waiter -= seconds;

            if (waiter < 0)
            {
                int cnt = 0;
                if (barBallScore > 0)
                {
                    cnt++;
                    barBallScore--;
                }
                if (timeBonusScore > 0)
                {
                    cnt++;
                    timeBonusScore--;
                }

                LevelManager.Instance.TotalScore += cnt;

                waiter += Intervall;
            }

            if (_IsGameOver)
            {
                BubbleBreak.Instance.ModeProxy.GetMode<MainMenu>().Update(BubbleBreak.Instance.CurrentUpdateTime, null);
            }
        }

        public void Draw(SpriteBatch batch)
        {
            if (_IsVisible)
            {
                Localization loc = BubbleBreak.Instance.Loc;

                Frame bg = BubbleBreak.Instance.GameSheet.GetFrame("default", "LevelScoreBackground");
                BubbleBreak.Instance.GameSheet.Draw(batch, bg, Level.ActualCenter, 0, 2, Level.CurrentLevel.BackgroundColor * 0.5f);

                
                Vector2 titleDim = BubbleBreak.Font.MeasureString(title);
                batch.DrawString(BubbleBreak.Font, title, new Vector2(400 - titleDim.X / 2, 100), Color.Yellow);

                if (!_IsGameOver)
                {
                    batch.DrawString(BubbleBreak.Font, loc["lbl_extra_score"], new Vector2(260, 175), Color.Thistle);

                    batch.DrawString(BubbleBreak.Font, loc["lbl_barballs_bonus"], new Vector2(260, 225), Color.LightSteelBlue);
                    string barScore = barBallScore.ToString("# ##0");
                    Vector2 barScoreSize = BubbleBreak.Font.MeasureString(barScore);
                    batch.DrawString(BubbleBreak.Font, barScore, new Vector2(540 - barScoreSize.X, 250), Color.LightGreen);


                    batch.DrawString(BubbleBreak.Font, loc["lbl_time_bonus"], new Vector2(260, 275), Color.LightSteelBlue);
                    string timeScore = timeBonusScore.ToString("# ##0");
                    Vector2 timeScoreSize = BubbleBreak.Font.MeasureString(timeScore);
                    batch.DrawString(BubbleBreak.Font, timeScore, new Vector2(540 - timeScoreSize.X, 300), Color.LightGreen);
                }

                batch.DrawString(BubbleBreak.Font, loc["lbl_score"], new Vector2(260, 350), Color.Yellow);

                string score = LevelManager.Instance.TotalScore.ToString("# ### ##0");
                Vector2 size = BubbleBreak.Font.MeasureString(score);
                batch.DrawString(BubbleBreak.Font, score, new Vector2(540 - size.X, 350), Color.GreenYellow);

                // Draw Level Achievements
                if (levelAchievements.Count != 0)
                {
                    float posX = (BubbleBreak.PlayingFieldFlipped) ? 550 : 5;
                    batch.DrawString(BubbleBreak.FontSmall, loc["lbl_new_achievements"], new Vector2(posX, 5), Color.Wheat);

                    foreach (Ball b in levelAchievements)
                        b.DrawAsIcon(batch, 1, 1);
                }


                if (_IsGameOver)
                {
                    foreach (Ball b in gameAchievements)
                        b.DrawAsIcon(batch, 1, 1);

                    batch.End();
                    BubbleBreak.Instance.ModeProxy.GetMode<MainMenu>().Draw(null, batch);
                    batch.Begin();
                }
                //batch.DrawString(BubbleBreak.Font, _Title, new Vector2(35, 5), Color.Yellow);
                //batch.DrawString(BubbleBreak.Font, _Text, new Vector2(15, 15), Color.White);
                //batch.DrawString(BubbleBreak.Font, _ScoreText, new Vector2(175, 15), Color.LightBlue);
            }
        }

    }
}
