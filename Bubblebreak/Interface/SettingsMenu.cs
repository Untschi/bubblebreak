﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using System.IO;
using System.IO.IsolatedStorage;
using Sunbow.Util;

namespace Bubblebreak
{
    public class SettingsMenu : ButtonMenu
    {

        public int StartLevel { get; private set; }

        bool _IsInverted;

        int _MusicVolume = 50;

        public SettingsMenu()
            : base("SettingsMenu")
        {
            Load();

            AddButton("Invert Digital Input", new Action(InvertDigital));
            AddButton("Link Bar to Colors", new Action(LinkBar), new Vector2(10, 110));
            AddButton("Sound", new Action(Sound), new Vector2(10, 160));

            AddButton("+", new Action(VolumeIncrease), new Vector2(180, 190));
            AddButton("-", new Action(VolumeDecrease), new Vector2(180, 210));

            AddButton("+", new Action(LevelIncrease), new Vector2(180, 240));
            AddButton("-", new Action(LevelDecrease), new Vector2(180, 260));

            AddButton("Back", new Action(OnButtonB), new Vector2(0, 300));

        }

        void VolumeIncrease()
        {
            _MusicVolume = Math.Min(_MusicVolume + 10, 100);
            BubblebreakSound.SetMusicVolume(_MusicVolume / 100f);
        }
        void VolumeDecrease()
        {
            _MusicVolume = Math.Max(_MusicVolume - 10, 0);
            BubblebreakSound.SetMusicVolume(_MusicVolume / 100f);
        }

        void LevelIncrease()
        {
            StartLevel = Math.Min(StartLevel + 1, 20);
        }
        void LevelDecrease()
        {
            StartLevel = Math.Max(StartLevel - 1, 1);
        }
        void InvertDigital()
        {
            _IsInverted = !_IsInverted;
        }
        void Sound()
        {
            BubblebreakSound.IsSoundEnabled = !BubblebreakSound.IsSoundEnabled;
        }

        void LinkBar()
        {
            BubblebreakInput.Instance.IsBarConstrained = !BubblebreakInput.Instance.IsBarConstrained;
        }

        public override void OnButtonB()
        {
            BubblebreakInput.Instance.Invert(_IsInverted);
            Save();
            MenuManager.SwitchTo("MainMenu");
        }

        public override void Draw(SpriteBatch batch)
        {
            base.Draw(batch);

            batch.DrawString(BubbleBreak.Font,(_IsInverted) ? "On" : "Off", new Vector2(180, 83), Color.White);
            batch.DrawString(BubbleBreak.Font, (BubblebreakInput.Instance.IsBarConstrained) ? "On" : "Off", new Vector2(180, 123), Color.White);

            batch.DrawString(BubbleBreak.Font, (BubblebreakSound.IsSoundEnabled) ? "On" : "Off", new Vector2(180, 160), Color.White);
            batch.DrawString(BubbleBreak.Font, "Music Volume:        " + _MusicVolume.ToString(), new Vector2(10, 200), Color.White);
            batch.DrawString(BubbleBreak.Font, "Start Level:         " + StartLevel.ToString(), new Vector2(20, 250), Color.White);

        }



        public void Save()
        {
            //IAsyncResult selectorResult = StorageDevice.BeginShowSelector(PlayerIndex.One, null, null);
            //StorageDevice device = StorageDevice.EndShowSelector(selectorResult);
            //IAsyncResult containerResult = device.BeginOpenContainer("Bubblebreak", null, null);
            //StorageContainer container = device.EndOpenContainer(containerResult);

            //string filename ="";// Path.Combine(, @"settings");

            using (IsolatedStorageFile file = Helper.GetUserStoreForAppDomain())
            {
                using (IsolatedStorageFileStream stream = file.OpenFile(@"settings", FileMode.OpenOrCreate))
                {
                    BinaryWriter writer = new BinaryWriter(stream);//File.Open(filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite));
            
                    writer.Write(_IsInverted);
                    writer.Write(BubblebreakInput.Instance.IsBarConstrained);
                    writer.Write(BubblebreakSound.IsSoundEnabled);
                    writer.Write(_MusicVolume);
                    writer.Write(StartLevel);

                    writer.Close();
                }
            }

            
            //container.Dispose();

        }

        public void Load()
        {
            //IAsyncResult result = StorageDevice.BeginShowSelector(PlayerIndex.One, null, null);
            //StorageDevice device = StorageDevice.EndShowSelector(result);
            //StorageContainer container = device.OpenContainer("Bubblebreak");
            //String filename = Path.Combine(container.Path, @"settings");

            //if (!File.Exists(filename))
            //{
            //    BinaryWriter writer = new BinaryWriter(File.Open(filename, FileMode.Create, FileAccess.Write));
            //    writer.Write(false);
            //    writer.Write(true);
            //    writer.Write(true);
            //    writer.Write(50);
            //    writer.Write(1);
            //    writer.Close();
            //}


            using (IsolatedStorageFile file = Helper.GetUserStoreForAppDomain())
            {
                if (file.FileExists(@"settings"))
                {
                    try
                    {
                        using (IsolatedStorageFileStream stream = file.OpenFile(@"settings", FileMode.Open))
                        {
                            BinaryReader reader = new BinaryReader(stream);

                            //writer.Write(_IsInverted);
                            _IsInverted = reader.ReadBoolean();
                            //writer.Write(BubblebreakInput.Instance.IsBarConstrained);
                            BubblebreakInput.Instance.IsBarConstrained = reader.ReadBoolean();
                            //writer.Write(BubblebreakSound.IsSoundEnabled);
                            BubblebreakSound.IsSoundEnabled = reader.ReadBoolean();
                            //writer.Write(_MusicVolume);
                            _MusicVolume = reader.ReadInt32();
                            BubblebreakSound.SetMusicVolume(_MusicVolume / 100f);
                            //writer.Write(StartLevel);
                            StartLevel = reader.ReadInt32();

                            reader.Close();
                        }
                    }
                    catch (Exception ex)
                    {
                       // System.Diagnostics.Contracts.Contract.Assert(false, "couldn't read settings file");
                    }
                }
                else
                {
                    _IsInverted = false;
                    BubblebreakInput.Instance.IsBarConstrained = true;
                    BubblebreakSound.IsSoundEnabled = true;
                    _MusicVolume = 50;
                    BubblebreakSound.SetMusicVolume(1);
                    StartLevel = 1;
                }
            }



            //BinaryReader reader = new BinaryReader(File.OpenRead(filename));
            ////writer.Write(_IsInverted);
            //_IsInverted = reader.ReadBoolean();
            ////writer.Write(BubblebreakInput.Instance.IsBarConstrained);
            //BubblebreakInput.Instance.IsBarConstrained = reader.ReadBoolean();
            ////writer.Write(BubblebreakSound.IsSoundEnabled);
            //BubblebreakSound.IsSoundEnabled = reader.ReadBoolean();
            ////writer.Write(_MusicVolume);
            //_MusicVolume = reader.ReadInt32();
            //BubblebreakSound.SetMusicVolume(_MusicVolume / 100f);
            ////writer.Write(StartLevel);
            //StartLevel = reader.ReadInt32();

            //reader.Close();
            //container.Dispose();

        }

    }
}
