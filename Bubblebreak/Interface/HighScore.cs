﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.GamerServices;
using System.IO.IsolatedStorage;
using Sunbow.Util;

namespace Bubblebreak
{
    public class HighScore : MenuScreen
    {
        const int MaxEntries = 5;

        List<HighScoreEntry> _Entries = new List<HighScoreEntry>();
        HighScoreEntry _CurrentEntry;
        TextChooser _TextChooser = new TextChooser(5);

        ColorChanger _BackgroundColor = new ColorChanger(1, 0.5f);
        Texture2D _Background;

        int _Score = 0;

        public HighScore()
            : base("Highscore")
        {
            //_Background = BubbleBreak.Instance.Content.Load<Texture2D>("highscoreBG");
            _TextChooser.OnTextChange += new Action<string>(TextChooser_OnTextChange);
        }

        public void Show()
        {
            Show(null);
            _TextChooser.IsInputLocked = true;
        }
        public void Show(HighScoreEntry newEntry)
        {
            _TextChooser.Reset();

            _Entries = Load();

            _CurrentEntry = newEntry;
            if (newEntry != null)
            {
                _Entries.Add(newEntry);
                _Score = newEntry.Score;
            }

            _Entries.Sort();

            if (_CurrentEntry != null && _Entries.IndexOf(_CurrentEntry) >= MaxEntries)
            {
                _CurrentEntry = null;
                _Entries.Remove(_CurrentEntry);

                _TextChooser.IsInputLocked = true;
            }

        }

        void TextChooser_OnTextChange(string obj)
        {
            if(_CurrentEntry != null)
                _CurrentEntry.Name = obj;
        }

        public override void OnButtonA()
        {
            if (!_TextChooser.IsInputLocked)
                _TextChooser.OnEnterKey();
            else
            {
                Save(_Entries, MaxEntries);
                MenuManager.SwitchTo("MainMenu");
            }
        }
        public override void OnButtonB()
        {
            _TextChooser.OnBackKey();
        }

        public override void Update(float seconds)
        {
            if (_CurrentEntry == null)
                _TextChooser.IsInputLocked = true;

            _TextChooser.Update(seconds);
            _BackgroundColor.Update(seconds);
        }

        public override void Draw(SpriteBatch batch)
        {
            batch.Draw(_Background, Vector2.Zero, _BackgroundColor.CurrentColor);
            _TextChooser.Draw(batch);

            Color col;
            for(int i = 0; i < MaxEntries && i < _Entries.Count; i++)
            {
                col = (_Entries[i] == _CurrentEntry)? Color.Red : Color.White;

                batch.DrawString(BubbleBreak.Font, (i + 1).ToString(), new Vector2(35, 65 + i * 21), col);
                batch.DrawString(BubbleBreak.Font, _Entries[i].Name, new Vector2(55, 65 + i * 21), col);
                batch.DrawString(BubbleBreak.Font, _Entries[i].Score.ToString(), new Vector2(155, 65 + i * 21), col);

            }

            if(_Score != 0)
                batch.DrawString(BubbleBreak.Font, "Your Score:   " + _Score, new Vector2(25, 250), Color.Yellow);
            
            if(_TextChooser.IsInputLocked)
                if(_CurrentEntry != null)
                    batch.DrawString(BubbleBreak.Font, "Key 1:   return to Menu\nKey 2:  edit Name", new Vector2(5, 280), Color.White);
                else
                    batch.DrawString(BubbleBreak.Font, "Key 1:   return to Menu", new Vector2(5, 280), Color.White);
            else
                batch.DrawString(BubbleBreak.Font, "Key 1:   add Glyph\nKey 2:  remove Glyph", new Vector2(5, 280), Color.White);
        }



        public static void Save(List<HighScoreEntry> sortedEntries, int maxEntries)
        {
            //IAsyncResult result = Guide.BeginShowStorageDeviceSelector(PlayerIndex.One, null, null);
            //StorageDevice device = Guide.EndShowStorageDeviceSelector(result);
            //StorageContainer container = device.OpenContainer("Bubblebreak");
            //String filename = Path.Combine(container.Path, @"high.score");

            using (IsolatedStorageFile file = Helper.GetUserStoreForAppDomain())
            {
                try
                {

                    bool fileExist = file.FileExists(@"high.score");

                    using (IsolatedStorageFileStream stream = file.OpenFile(@"high.score", FileMode.OpenOrCreate))
                    {
                        BinaryWriter writer = new BinaryWriter(stream);//File.Open(filename, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite));

                        for (int i = 0; i < maxEntries && i < sortedEntries.Count; i++)
                        {
                            writer.Write(sortedEntries[i].Name);
                            writer.Write(sortedEntries[i].Score);
                        }
                        writer.Close();
                    }
                }
                catch { }
            }
            //container.Dispose(); 
        }

        public static List<HighScoreEntry> Load()
        {
            //IAsyncResult result = Guide.BeginShowStorageDeviceSelector(PlayerIndex.One, null, null);
            //StorageDevice device = Guide.EndShowStorageDeviceSelector(result);
            //StorageContainer container = device.OpenContainer("Bubblebreak");
            //String filename = Path.Combine(container.Path, @"high.score");

            List<HighScoreEntry> entries = new List<HighScoreEntry>();

            using (IsolatedStorageFile file = Helper.GetUserStoreForAppDomain())
            {
                try
                {

                    bool fileExist = file.FileExists(@"high.score");

                    using (IsolatedStorageFileStream stream = file.OpenFile(@"high.score", FileMode.OpenOrCreate))
                    {
                        if (!fileExist)
                        {
                            BinaryWriter writer = new BinaryWriter(stream);
                            writer.Write("CHIEF");
                            writer.Write(3000);
                            writer.Write("SKILL");
                            writer.Write(2000);
                            writer.Write("CHECK");
                            writer.Write(1000);
                            writer.Write("NOOB");
                            writer.Write(500);
                            writer.Close();

                        }
                    }
                    using (IsolatedStorageFileStream stream = file.OpenFile(@"high.score", FileMode.OpenOrCreate))
                    {
                        BinaryReader reader = new BinaryReader(stream);


                        while (reader.PeekChar() != '\0')
                        {
                            try
                            {
                                entries.Add(new HighScoreEntry(reader.ReadString(), reader.ReadInt32()));
                            }
                            catch
                            {
                                break;
                            }
                        }
                        reader.Close();
                    }
                }
                catch
                {
                }
            }

            //container.Dispose();
            return entries;
        }
    }
}
