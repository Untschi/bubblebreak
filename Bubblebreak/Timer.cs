﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bubblebreak
{
    public class Timer
    {
        float _ElapsedSeconds;
        public int ElapsedSeconds { get { return (int)_ElapsedSeconds; } }
        bool _IsStopped;

        public Timer()
        {

        }
        public void Stop()
        {
            _IsStopped = true;
        }
        public void Play()
        {
            _IsStopped = false;
        }
        public void Reset()
        {
            _ElapsedSeconds = 0;
        }
        public void Update(float seconds)
        {
            if(!_IsStopped)
                _ElapsedSeconds += seconds;
        }

        public string GetTimeString()
        {
            return GetTimeString(_ElapsedSeconds);
        }
        public static string GetTimeString(float seconds)
        {
            int sec = (int)seconds % 60;
            int min = ((int)seconds - sec) / 60;

            if (sec < 10)
            {
                return min + ":0" + sec;
            }
            return min + ":" + sec;
            
        }
    }
}
