﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bubblebreak.Achievements
{
    public enum AchievementType
    {
        GameStartet = 0,                // done
        Play10Games,                    // done
        Play25Games,                    // done
        Play50Games,                    // done
        Play100Games,                   // done
        MoreBallsBouncedThanInLevel,    // done
        FirstLevelDone,                 // done
        Level10Done,                    // done
        Level15Done,                    // done
        Level18Done,                    // done
        CompleteIn30Seconds,            // done
        CompleteIn10Seconds,            // done
        CompleteIn5Seconds,             // done
        CompleteWith3Shots,             // done
        CompleteWith2Shots,             // done
        CompleteWith1Shot,              // done
        NoBallEscaped,                  // done
        AllBallsEscaped,                // done
        Pop20BallsWithOneShot,          // done
        Pop30BallsWithOneShot,          // done
        Pop40BallsWithOneShot,          // done
        FinishLevelWithTheLastShot,     // done
        CompleteLevelAfterGameOver,     // done
        CollectNoItemDuringAGame,       // done
        CollectAllItemsDuringAGame,     // done
        CollectAllItemTypesInAGame,     // done
        Hit3MagnetsWithOneBubble,       // done
        PlayOnly1ColorInALevel,         // done
        GetANegativeHighscore,          // done

        _Count,
        undefined
    }
}
