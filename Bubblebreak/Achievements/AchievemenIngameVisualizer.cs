﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using Sunbow.Util.Manipulation;
using Bubblebreak.Game.Gui;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace Bubblebreak.Achievements
{
    public class AchievemenIngameVisualizer
    {
        Queue<AchievementType> achievementQueue = new Queue<AchievementType>();
        AchievementType currentVisualization = AchievementType.undefined;

        public SwitchLogic Switch { get; set; }

        Ball visual = null;
        CurveSingleAnimator smileScaler;
        FloatingText text1, text2;

        public AchievemenIngameVisualizer()
        {
            smileScaler = new CurveSingleAnimator(BubbleBreak.Instance.Content.Load<Curve>(@"Curves\achievement"),
                FloatingText.Duration, AnimationRepeatLogic.Pause);
            smileScaler.EndOfAnimation += smileScaler_EndOfAnimation;
        }

        void smileScaler_EndOfAnimation(object sender, EventArgs e)
        {
            if (achievementQueue.Count > 0)
            {
                AchievementType next = achievementQueue.Dequeue();
                SetAchievement(next);
            }
            else
            {
                currentVisualization = AchievementType.undefined;

                if (Switch != SwitchLogic.None)
                {
                    LevelManager.Instance.FinishLevel(Switch == SwitchLogic.NextLevel, false);
                    Switch = SwitchLogic.None;
                }
            }
        }

        private void SetAchievement(AchievementType achievement)
        {
            currentVisualization = achievement;
            smileScaler.Reset();
            smileScaler.Play();

            string t = string.Format("{0}:\n\n\n\n", BubbleBreak.Instance.Loc["new_achievement"]);
            text1 = new FloatingText(t, Level.ActualCenter, Color.Yellow);

            t = string.Format("\n\n\n\n{0}", BubbleBreak.Instance.Loc[achievement.ToString()]);
            text2 = new FloatingText(t, Level.ActualCenter, Color.Yellow);


            visual = new Ball(achievement, true) { Position = Level.ActualCenter, Color = Color.Wheat };
        }

        public void Achieve(AchievementType achievement)
        {
            if (currentVisualization == AchievementType.undefined)
                SetAchievement(achievement);
            else
                achievementQueue.Enqueue(achievement);
        }

        public void Update(float seconds)
        {
            if (currentVisualization == AchievementType.undefined)
                return;

            smileScaler.Update(seconds);
            text1.Update(seconds);
            text2.Update(seconds);

        }

        public void Draw(SpriteBatch batch)
        {
            if (currentVisualization == AchievementType.undefined)
                return;

            visual.DrawAsIcon(batch, 2 * text1.AlphaValue, smileScaler);
            text1.Draw(batch);
            text2.Draw(batch);
        }
    }
}
