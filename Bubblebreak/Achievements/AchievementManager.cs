﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.IO;

namespace Bubblebreak.Achievements
{
    public class AchievementManager
    {
        Table table;

        List<AchievementType> achievedAchievements = new List<AchievementType>();

        List<AchievementType> newGameAchievements = new List<AchievementType>();
        List<AchievementType> newLevelAchievements = new List<AchievementType>();

        public AchievementManager()
        {
            InitTable();
        }

        private void InitTable()
        {
             try
            {
                table = new Table("achievements", true);
            }
            catch (System.IO.IOException)
            {
                table = CreateTable(null);
            }

            try
            {
                CacheAchievements();
            }
            catch (Exception) // something has changed...
            {
                table = CreateTable(table);
                CacheAchievements();
            }
        }

        private Table CreateTable(Table reference)
        {
            int cnt =  (int)AchievementType._Count + 1 + 1;
            Table t = new Table("achievements", 2, cnt);
            t[0, 0] = "Achievement";
            t[1, 0] = "Value";

            for (int i = 0; i < (int)AchievementType._Count; i++)
            {
                string achievement = ((AchievementType)i).ToString();
                t[0, i + 1] = achievement;

                if (reference != null && reference.ContainsRow(achievement))
                    t[1, i + 1] = reference[1, achievement];
                else
                    t[1, i + 1] = "FALSE";
            }
            t[1, 1] = "TRUE";

            t[0, cnt - 1] = "_Games_Played";
            t[1, cnt - 1] = "1";

            return t;
        }


        private void CacheAchievements()
        {
            foreach (int idx in table.GetFilledRowIterator("Value"))
            {
                bool achieved = false;
                if (table.TryGet<bool>("Value", idx, out achieved)
                    && achieved)
                {
                    AchievementType achievement;
                    if (table.TryGet<AchievementType>("Achievement", idx, out achievement))
                    {
                        achievedAchievements.Add(achievement);
                    }
                }
            }
        }

        public void AnotherGamePlayed()
        {
            int gameNumber = table.Get<int>("Value", "_Games_Played") + 1;
            table["Value", "_Games_Played"] = gameNumber.ToString();

            switch(gameNumber)
            {
                case 10:
                    Achieve(AchievementType.Play10Games);
                    break;
                case 25:
                    Achieve(AchievementType.Play25Games);
                    break;
                case 50:
                    Achieve(AchievementType.Play50Games);
                    break;
                case 100:
                    Achieve(AchievementType.Play100Games);
                    break;
            }
        }
        public void ClearAchievements()
        {
            achievedAchievements.Clear();
            InitTable();
        }

        public void ClearLevelList()
        {
            newLevelAchievements.Clear();
        }

        public void ClearGameAchievements()
        {
            newGameAchievements.Clear();
        }

        public void Save()
        {
            if(!BubbleBreak.IsTestVersion)
                table.Save();
        }

        public bool IsAchieved(AchievementType achievement)
        {
            return table.Get<bool>("Value", achievement.ToString());
        }

        public bool Achieve(AchievementType achievement)
        {
            bool tmp = false;
            return Achieve(achievement, ref tmp);
        }
        public bool Achieve(AchievementType achievement, ref bool achieveHack)
        {
            achieveHack = achieveHack || !IsAchieved(achievement);
            if (!IsAchieved(achievement))
            {
                table["Value", achievement.ToString()] = "TRUE";

                newLevelAchievements.Add(achievement);
                newGameAchievements.Add(achievement);

                achievedAchievements.Add(achievement);

                LevelManager.Instance.AchievementVisual.Achieve(achievement);

                System.Diagnostics.Debug.WriteLine("Achievement: " + achievement);

                return true;
            }
            return false;
        }

        public AchievementType GetRandomAvailableAchievement()
        {
            int idx = Sunbow.Util.Random.Next(achievedAchievements.Count);

            return achievedAchievements[idx];
        }

        public IEnumerable<AchievementType> GetLevelAchievementIterator()
        {
            foreach (AchievementType a in newLevelAchievements)
                yield return a;
        }
        public IEnumerable<AchievementType> GetGameAchievementIterator()
        {
            foreach (AchievementType a in newGameAchievements)
                yield return a;
        }
    }
}
