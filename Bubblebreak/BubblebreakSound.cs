﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;

namespace Bubblebreak
{
    public class BubblebreakSound
    {
        public static readonly SoundEffect BounceBar, ShootBar, LoadBar, PopBunch, PopDestroy, LevelUp, Explosion, CollectSpecial, ToMagnet, ToStone;
        static SoundEffectInstance _GameTheme;

        public static float SoundEffectVolume { get; set; }
        public static float MusicVolume { get { return musicVolume; } set { SetMusicVolume(value); } }
        static float musicVolume;

        static SoundEffect _NextEffect;

        static BubblebreakSound()
        {
            musicVolume = 0.8f;
            SoundEffectVolume = 0.8f;

            BounceBar = BubbleBreak.Instance.Content.Load<SoundEffect>("Sound/bounce_bar");
            ShootBar = BubbleBreak.Instance.Content.Load<SoundEffect>("Sound/shoot_bar");
            LoadBar = BubbleBreak.Instance.Content.Load<SoundEffect>("Sound/load_bar");
            PopBunch = BubbleBreak.Instance.Content.Load<SoundEffect>("Sound/pop_bunch");
            PopDestroy = BubbleBreak.Instance.Content.Load<SoundEffect>("Sound/pop_destroy");
            LevelUp = BubbleBreak.Instance.Content.Load<SoundEffect>("Sound/level_up");
            Explosion = BubbleBreak.Instance.Content.Load<SoundEffect>("Sound/boom");
            CollectSpecial = BubbleBreak.Instance.Content.Load<SoundEffect>("Sound/swosh");
            ToMagnet = BubbleBreak.Instance.Content.Load<SoundEffect>("Sound/to_magnet");
            ToStone = BubbleBreak.Instance.Content.Load<SoundEffect>("Sound/to_stone");

            _GameTheme = BubbleBreak.Instance.Content.Load<SoundEffect>("Sound/bubblebreak_theme").CreateInstance();
            _GameTheme.IsLooped = true;
            _GameTheme.Volume = musicVolume;
            _GameTheme.Play();
            
        }

        public static void Play(SoundEffect effect)
        {
            if (SoundEffectVolume > 0 && _NextEffect != effect)
                _NextEffect = effect;
        }

        public static void Update()
        {
            if (_NextEffect != null)
            {
                _NextEffect.Play(SoundEffectVolume, 0, 0);
                _NextEffect = null;
            }
        }

        public static void SetMusicVolume(float volume)
        {
            musicVolume = volume;
            _GameTheme.Volume = volume;
        }

    }
}
