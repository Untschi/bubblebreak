﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DPSF;
using Microsoft.Xna.Framework;
namespace Bubblebreak.Game.Particles
{
    public class BubbleBackgroundParticles 
    {
        DefaultSpriteTextureCoordinatesParticleSystem system;

        public BubbleBackgroundParticles()
        {
            DefaultSpriteTextureCoordinatesParticle prototype = new DefaultSpriteTextureCoordinatesParticle() 
            {
                EndColor = Color.White * 0,
                EndSize = 3f,
                Lifetime = 1f,
                TextureCoordinates = new Rectangle(204, 1004, 20, 20),
                StartSize = 20f,
            };

            system = new DefaultSpriteTextureCoordinatesParticleSystem(BubbleBreak.Instance);
            system.Texture = BubbleBreak.Instance.GameSheet.Texture;
            system.ParticleInitializationFunction = new DPSF<DefaultSpriteTextureCoordinatesParticle,DefaultSpriteParticleVertex>.InitializeParticleDelegate(prototype);
        }

        public void EmitParticles()
        {

        }

    }
}
