﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Bubblebreak
{
    public class Bar
    {
        const float MoveScaleShoot = 300;
        const float MoveScaleBounce = 1.5f;
        const float AngleOffset = 0.11f;

        static readonly float _BallIntersectionDistanceSquared = (Level.PlayfieldRadius - 2 * Ball.BoundingRadius) * (Level.PlayfieldRadius - 2 * Ball.BoundingRadius);

        public Ball ShootBall { get { return _ShootBall; } }
        Ball _ShootBall;
        List<Ball> _BarBalls = new List<Ball>();

        Action _ShootKeyUp, _ShootKeyDown;

        float _PreviousAngle;

        BounceFeedbackPool bounces = new BounceFeedbackPool();

        public Bar()
        {
            _ShootKeyDown = new Action(OnShootKeyDown);
            _ShootKeyUp = new Action(OnShootKeyUp);
            BubblebreakInput.Instance.OnShootKeyDown += _ShootKeyDown;
            BubblebreakInput.Instance.OnShootKeyUp += _ShootKeyUp;
        }

        void OnShootKeyUp()
        {
            if (_ShootBall == null
                || LevelManager.Instance.ScoreBoard.IsVisible
                || !BubbleBreak.Instance.ModeProxy.IsActiveMode<Modes.Game>())
                return;

            BubblebreakSound.Play(BubblebreakSound.ShootBar);
            _ShootBall.Velocity = Ball.ColoredMovementSpeed 
                * Vector2.Normalize(new Vector2(
                    Level.PlayfieldCenterX - _ShootBall.Position.X,
                    Level.PlayfieldCenterY - _ShootBall.Position.Y) + GetCurrentBarNormalOffset(MoveScaleShoot));

            _ShootBall.State = BallState.MovingColorBall;
            Level.CurrentLevel.Stats.AddPlayedColor(_ShootBall.Color);
            Level.CurrentLevel.SetSpecial(ItemType.Undefined);
            Level.CurrentLevel.Stats.ShotBalls++;
            _ShootBall = null;
        }

        void OnShootKeyDown()
        {
            if (_BarBalls.Count == 0 
                || LevelManager.Instance.ScoreBoard.IsVisible
                || !BubbleBreak.Instance.ModeProxy.IsActiveMode<Modes.Game>())
                return;

            BubblebreakSound.Play(BubblebreakSound.LoadBar);
            _ShootBall = _BarBalls[0];
            _ShootBall.State = BallState.PreparedBarBall;
            _BarBalls.RemoveAt(0);

            switch (Level.CurrentLevel.CurrentSpecial)
            {
                case ItemType.AllColorBall:
                    _ShootBall.Type = BallType.AllColor;
                    break;
                case ItemType.BombBall:
                    _ShootBall.Type = BallType.Bomb;
                    break;
                case ItemType.ColorKillerBall:
                    _ShootBall.Type = BallType.ColorKiller;
                    break;
                case ItemType.MovableBall:
                    _ShootBall.Type = BallType.MovableBall;
                    break;
                default: break;
            }
        }

        public void AddBall(Ball ball)
        {
            if (!_BarBalls.Contains(ball))
                _BarBalls.Add(ball);

            ball.State = BallState.BarBall;
        }

        public bool ContainsBall(Ball ball)
        {
            return _BarBalls.Contains(ball);
        }

        public void Update(float seconds)
        {
            bounces.Update(seconds);

            float angle = (BubblebreakInput.Instance.CurrentBarAngle + MathHelper.TwoPi) % MathHelper.TwoPi;
            float angleOff = -AngleOffset * (_BarBalls.Count / 2f) + AngleOffset / 2;

            foreach (Ball b in _BarBalls)
            {
                b.Position = BubblebreakInput.Instance.GetBarPositionFromAngle(Level.PlayfieldRadius, angleOff);
                b.Color = Level.CurrentLevel.ColorField.GetColorLight(angle + angleOff, 0.5f);
                b.ColorIndex = ColorField.GetColorIndex(Level.CurrentLevel.ColorField.GetColor(angle + angleOff));
                angleOff += AngleOffset;
            }

            if (_ShootBall != null)
            {
                _ShootBall.Position = BubblebreakInput.Instance.GetBarPositionFromAngle(Level.PlayfieldRadius - (2 / BubbleBreak.Scale) * Ball.Radius, 0);
                _ShootBall.Color = Level.CurrentLevel.ColorField.GetColor(BubblebreakInput.Instance.CurrentBarAngle);

                if (_ShootBall.Type == BallType.MovableBall)
                {
                    _ShootBall.Velocity = Level.PlayfieldCenter - _ShootBall.Position;
                }
            }

            _PreviousAngle = angle;

            // Check for Levelend
            if (_BarBalls.Count == 0)
            {
                foreach (Ball b in Level.CurrentLevel.AllBalls)
                {
                    if (b.State == BallState.MovingColorBall || b.State == BallState.PreparedBarBall)
                    {
                        return;
                    }
                }
                Level.CurrentLevel.QuitLevel(false);
            }

            
        }

        public void CheckBallIntersection(Ball ball)
        {
            if (Vector2.DistanceSquared(ball.Position, Level.PlayfieldCenter) >= _BallIntersectionDistanceSquared)
            {
                Vector2 reflect = Vector2.Zero;

                float angle = MathHelper.WrapAngle(((float)Math.Atan2(ball.Position.Y - Level.PlayfieldCenterY, ball.Position.X - Level.PlayfieldCenterX) + MathHelper.TwoPi) % MathHelper.TwoPi);

                if (IsInBounds(angle))
                {
                    ball.Position = new Vector2(
                        Level.PlayfieldCenterX + (float)Math.Cos(angle) * (Level.PlayfieldRadius - 2 * Ball.BoundingRadius), 
                        Level.PlayfieldCenterY + (float)Math.Sin(angle) * (Level.PlayfieldRadius - 2 * Ball.BoundingRadius));
                    reflect =  Vector2.Normalize(new Vector2(Level.PlayfieldCenterX - ball.Position.X, Level.PlayfieldCenterY - ball.Position.Y));
                }
                else if (_BarBalls.Count > 0)
                {
                    if (Vector2.DistanceSquared(ball.Position, _BarBalls[0].Position) < Ball.IntersectingDistanceSquared)
                    {
                        reflect = Vector2.Normalize(new Vector2(ball.Position.X - _BarBalls[0].Position.X, ball.Position.Y - _BarBalls[0].Position.Y));
                    }
                    else if (Vector2.DistanceSquared(ball.Position, _BarBalls[_BarBalls.Count - 1].Position) < Ball.IntersectingDistanceSquared)
                    {
                        reflect = Vector2.Normalize(new Vector2(ball.Position.X - _BarBalls[_BarBalls.Count - 1].Position.X, ball.Position.Y - _BarBalls[_BarBalls.Count - 1].Position.Y));
                    }
                }

                if (reflect != Vector2.Zero)
                {
                    if (ball.State == BallState.ColorlessBall)
                        Ball.ColorlessBallCount--;

                    reflect += GetCurrentBarNormalOffset(MoveScaleBounce);

                    ball.Color = Level.CurrentLevel.ColorField.GetColor(angle);
                    ball.Velocity = Vector2.Reflect(Ball.ColoredMovementSpeed * Vector2.Normalize(ball.Velocity), Vector2.Normalize(reflect));
                    ball.State = BallState.MovingColorBall;

                    Level.CurrentLevel.Stats.AddPlayedColor(ball.Color);
                    Level.CurrentLevel.Stats.BounceAnotherBall(ball);

                    BubblebreakSound.Play(BubblebreakSound.BounceBar);

                    float maxDiff = MathHelper.WrapAngle(AngleOffset * (0.5f * _BarBalls.Count - 1));
                    float angleDiff = MathHelper.WrapAngle(angle - BubblebreakInput.Instance.CurrentBarAngle);
                    BounceLocation bl = (angleDiff <= -maxDiff) ? BounceLocation.UpperArc : ((angleDiff >= maxDiff) ? BounceLocation.LowerArc : BounceLocation.Line);
                    bounces.AddBounce(bl, angleDiff, ball.Color);
                }

            }
        }

        public bool IsInBounds(float angle)
        {
            float min = MathHelper.WrapAngle((BubblebreakInput.Instance.CurrentBarAngle - AngleOffset * 0.5f *  _BarBalls.Count + MathHelper.TwoPi) % MathHelper.TwoPi);
            float max = MathHelper.WrapAngle((BubblebreakInput.Instance.CurrentBarAngle + AngleOffset * 0.5f * _BarBalls.Count  + MathHelper.TwoPi) % MathHelper.TwoPi);

            return IsAngleBetween(angle, min, max);
        }

        public static bool IsAngleBetween(float angle, float RightHandAngle, float LeftHandAngle)
        {

            if (RightHandAngle > LeftHandAngle)
            {
                if ((angle >= RightHandAngle) || (angle <= LeftHandAngle))
                {
                    return true;
                }
            }
            else
            {
                if ((angle >= RightHandAngle) && (angle <= LeftHandAngle))
                {
                    return true;
                }
            }
            return false;
        }

        private Vector2 GetCurrentBarNormalOffset(float scale)
        {
            Vector2 a = new Vector2((float)Math.Cos(BubblebreakInput.Instance.CurrentBarAngle), (float)Math.Sin(BubblebreakInput.Instance.CurrentBarAngle));
            Vector2 b = new Vector2((float)Math.Cos(_PreviousAngle), (float)Math.Sin(_PreviousAngle));
            return (a - b) * scale;
        }

        public int GetBallCount()
        {
            int shootball = (_ShootBall != null) ? 1 : 0;
            return _BarBalls.Count + shootball;
        }

        internal void Delete()
        {
            _ShootBall = null;
            _BarBalls.Clear();
            _BarBalls = null;

            BubblebreakInput.Instance.OnShootKeyDown -= _ShootKeyDown;
            _ShootKeyDown = null;
            BubblebreakInput.Instance.OnShootKeyUp -= _ShootKeyUp;
            _ShootKeyUp = null;
        }

        public void Draw(SpriteBatch batch)
        {
            bounces.Draw(batch, BubblebreakInput.Instance.CurrentBarAngle, MathHelper.WrapAngle(AngleOffset * 0.5f * (_BarBalls.Count - 1)));
        }
    }
}
