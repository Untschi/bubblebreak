﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Sunbow.Util.Sprites;
using Bubblebreak.Achievements;

namespace Bubblebreak
{
    public enum BallState
    {
        undefined = 0,
        BarBall = 1,
        PreparedBarBall = 2,
        MovingColorBall = 3,
        FixedBall = 4,
        ColorlessBall = 5
    }

    public enum BallType
    {
        Normal = 0,
        Central = 1,
        AllColor = 2,
        Bomb = 3,
        ColorKiller = 4,
        MovableBall = 5,
    }

    public enum BallAppearance
    {
        Smilie = 0,
        Bubble = 1,
        ColorSymbol = 2,
    }

    public class Ball
    {
        public const float ColoredMovementSpeed = 40;
        public const float ColorlessMovementSpeed = 20;
        public const float BoundingRadius = 5f;
        public const float BounceAlphaDecrease = 0.4f;
        const byte AlphaDestroyingThreshold = 200;
        const int ColorSymbolStart = 62;

        //static Texture2D _Texture;
        public static float Radius { get; private set; }
        public static float IntersectingDistanceSquared { get; private set; }

        public static int ColorlessBallCount { get; set; }

        public static BallAppearance BallAppearance { get { return ballAppearance; } set { ballAppearance = value; } }
        static BallAppearance ballAppearance = BallAppearance.Smilie;

        static List<Frame> ColorSymbols = new List<Frame>();

        BallState _State;
        public BallState State { get { return _State; } set { _State = value; } }

        public Smilie Smilie { get { return _Smilie; } }
        Smilie _Smilie;

        BallType _Type;
        public BallType Type { get { return _Type; } 
            set { 
                _Type = value;

                _CurrentFrameAnimation = null;
                switch (value)
                {
                    case BallType.AllColor:
                        _CurrentFrameAnimation = BubbleBreak.Instance.BallFrames.CreateAnimation("AllColorBall");
                        break;
                    case BallType.Bomb:
                        _CurrentFrameAnimation = BubbleBreak.Instance.BallFrames.CreateAnimation("BombBall");
                        _Smilie = null;
                        break;
                    case BallType.ColorKiller:
                        _CurrentFrameAnimation = BubbleBreak.Instance.BallFrames.CreateAnimation("ColorKillerBall");
                        break;
                    case BallType.MovableBall:
                        _CurrentFrame = BubbleBreak.Instance.BallFrames.GetFrame("MovableBall");
                        break;
                    case BallType.Normal:
                        _CurrentFrame = BubbleBreak.Instance.BallFrames.GetFrame("NormalBall");
                        break;
                }
            } 
        }

        protected Vector2 _Position = Vector2.Zero;
        public Vector2 Position { get { return _Position; } set { _Position = value; } }

        float _Rotation;
        public float Rotation { get { return _Rotation; } set { _Rotation = value; } }

        Vector2 _Velocity;
        public Vector2 Velocity { get { return _Velocity; } set { _Velocity = value; } }

        //protected Vector2 _Origin;

        Color _Color = Color.White;
        public Color Color { 
            get { return _Color; } 
            set { 
                _Color = value; 
                int idx = ColorField.GetColorIndex(value);
                //if (idx != -1)
                    ColorIndex = idx;
            } 
        }
        public int ColorIndex { get; set; }

        Frame _CurrentFrame;
        FrameAnimation _CurrentFrameAnimation;


        static Ball()
        {
            Radius = 14;//_Texture.Width / 2f;
            IntersectingDistanceSquared = (2 * BoundingRadius) * (2 * BoundingRadius);
            
            FrameSet set = BubbleBreak.Instance.GameSheet.GetFrameSet("smile");
            for (int i = 0; i < ColorField.MaxColors; i++)
            {
                ColorSymbols.Add(set.CalculateFrame(i + ColorSymbolStart));
            }
        }
        public Ball()
            :this(AchievementType.undefined)
        {
        }
        public Ball(AchievementType achievement)
            : this(achievement, ballAppearance == Bubblebreak.BallAppearance.Smilie)
        {
        }
        public Ball(AchievementType achievement, bool smile)
        {
            _Type = BallType.Normal;
            _CurrentFrame = BubbleBreak.Instance.BallFrames.GetFrame("NormalBall");
            if(smile)
                _Smilie = new Smilie(this, achievement);
        }

        public void SetSmilieIcon(AchievementType achivement)
        {
            if (achivement == AchievementType.undefined)
            {
                _Smilie = null;
                return;
            }                  
            _Smilie = new Smilie(this, achivement);
        }

        public virtual void Update(float seconds)
        {
            if (_CurrentFrameAnimation != null)
                _CurrentFrameAnimation.Update(seconds);
            

            switch (_State)
            {
                case BallState.PreparedBarBall:
                case BallState.BarBall:
                    {
                        Vector2 dir = Position - Level.PlayfieldCenter;
                        Rotation = (float)Math.Atan2(dir.Y, dir.X) - MathHelper.PiOver2;
                    }
                    break;

                case BallState.MovingColorBall:

                    _Position.X += _Velocity.X * seconds;
                    _Position.Y += _Velocity.Y * seconds;

                    if (BubbleBreak.Instance.ModeProxy.IsActiveMode<Modes.Game>())
                    {
                        if (_Type == BallType.MovableBall)
                        {
                            Vector2 startPos = BubblebreakInput.Instance.GetBarPositionFromAngle(Level.PlayfieldRadius, 0);
                            Vector2 dir = Vector2.Normalize(Level.PlayfieldCenter - startPos);

                            _Velocity = _Velocity.Length() * dir;
                            _Position = startPos + Vector2.Distance(startPos, _Position) * dir;
                            if (Velocity.LengthSquared() > 0)
                                _Rotation = (float)Math.Atan2(Velocity.Y, Velocity.X) + MathHelper.PiOver2;
                        }

                        foreach (HexPoint h in Level.CurrentLevel.Bunch.FixedBalls.Keys)
                        {
                            Ball b = Level.CurrentLevel.Bunch.FixedBalls[h];
                            if (Vector2.DistanceSquared(b._Position, _Position) <= IntersectingDistanceSquared)
                            {
                                float angle = (float)Math.Atan2(_Position.Y - b._Position.Y, _Position.X - b._Position.X) * 180 / MathHelper.Pi;
                                angle = (angle + 360) % 360;

                                if (angle > 330 || angle <= 30)
                                    FixBall(h, 1, 0, -1);
                                else if (angle > 30 && angle <= 90)
                                    FixBall(h, 0, 1, -1);
                                else if (angle > 90 && angle <= 150)
                                    FixBall(h, -1, 1, 0);
                                else if (angle > 150 && angle <= 210)
                                    FixBall(h, -1, 0, 1);
                                else if (angle > 210 && angle <= 270)
                                    FixBall(h, 0, -1, 1);
                                else if (angle > 270 && angle <= 330)
                                    FixBall(h, 1, -1, 0);
                                else
                                    throw new Exception("Unexpected angle");

                                break;
                            }
                        }
                        Level.CurrentLevel.Bunch.CheckCentralBallIntersection(this);
                        Level.CurrentLevel.Bar.CheckBallIntersection(this);
                        if (Vector2.DistanceSquared(_Position, Level.PlayfieldCenter) > Level.PlayfieldRadius * Level.PlayfieldRadius)
                        {
                            BubblebreakSound.Play(BubblebreakSound.PopDestroy);
                            Delete(true);
                        }
                    }
                    break;

                case BallState.ColorlessBall:
                    _Position.X += _Velocity.X * seconds;
                    _Position.Y += _Velocity.Y * seconds;

                    if (BubbleBreak.Instance.ModeProxy.IsActiveMode<Modes.Game>())
                    {
                        for (int i = 0; i < Level.CurrentLevel.AllBalls.Count; i++)
                        {
                            Ball b = Level.CurrentLevel.AllBalls[i];

                            if (b.State != BallState.FixedBall)
                                continue;

                            if (Vector2.DistanceSquared(b._Position, _Position) <= IntersectingDistanceSquared)
                            {
                                //_Color.A -= BounceAlphaDecrease; -- old thought: a ball could live longer than one collision
                                if (_Color.A <= AlphaDestroyingThreshold)
                                {
                                    BubblebreakSound.Play(BubblebreakSound.PopDestroy);
                                    Delete(false);
                                }
                                else
                                {
                                    _Velocity = Vector2.Reflect(_Velocity, Vector2.Normalize(b._Position - _Position));
                                }
                            }
                        }

                        Level.CurrentLevel.Bar.CheckBallIntersection(this);

                        if (Vector2.DistanceSquared(_Position, Level.PlayfieldCenter) > Level.PlayfieldRadius * Level.PlayfieldRadius)
                        {
                            BubblebreakSound.Play(BubblebreakSound.PopDestroy);
                            Delete(true);
                        }
                    }
                    break;
                default:
                    break;
            }

            if(_Smilie != null)
                _Smilie.Update(seconds);
        }

        public virtual void Draw(SpriteBatch batch, float transparency)
        {
            if (_CurrentFrameAnimation != null)
                _CurrentFrame = _CurrentFrameAnimation.CurrentFrame;

            Color color = (_Type == BallType.Bomb || _Type == BallType.AllColor) ? Color.White : _Color;
            color *= transparency;
            float rotation = (_Type == BallType.MovableBall) ? _Rotation - MathHelper.PiOver4 : 0;
            float scale = (transparency == 1) ? 1 : 1 + transparency;
            BubbleBreak.Instance.GameSheet.Draw(batch, _CurrentFrame, BubbleBreak.Transform(_Position), rotation, scale, color);
            //batch.Draw(_Texture,  null, color, 0, _Origin, 1, SpriteEffects.None, 0);

            if ( _Smilie != null)
            {
                _Smilie.Draw(batch);
            }
            else if (BallAppearance == Bubblebreak.BallAppearance.ColorSymbol)
            {
                switch (State)
                {
                    case BallState.BarBall:
                    case BallState.FixedBall:
                    case BallState.MovingColorBall:
                    case BallState.PreparedBarBall:
                        if(ColorIndex != -1)
                            BubbleBreak.Instance.GameSheet.Draw(batch, ColorSymbols[ColorIndex], BubbleBreak.Transform(_Position), 0, 1);
                        break;
                }
            }
        }

        public void DrawAsIcon(SpriteBatch batch, float transparency, float scale)
        {
            BubbleBreak.Instance.GameSheet.Draw(batch, _CurrentFrame, _Position, 0, scale, _Color * transparency);
            
            if(_Smilie != null)
                _Smilie.DrawAsIcon(batch, transparency, scale);
        }
        public void DrawAsMenuBackground(SpriteBatch batch, float transparency, float scale)
        {
            BubbleBreak.Instance.GameSheet.Draw(batch, _CurrentFrame, _Position, 0, scale, _Color * transparency);

            if (_Smilie != null)
                _Smilie.Draw(batch, false);
        }


        private void FixBall(HexPoint referenceHex, int offsetU, int offsetV, int offsetW)
        {
            referenceHex.U += offsetU;
            referenceHex.V += offsetV;
            referenceHex.W += offsetW;

            if (_Type == BallType.Bomb || Level.CurrentLevel.Bunch.AddBall(referenceHex, this))
            {
                switch (_Type)
                {
                    case BallType.AllColor:
                        foreach (Color c in Level.CurrentLevel.ColorField.IterateAllColors())
                        {
                            Level.CurrentLevel.Bunch.CheckForChain(referenceHex, c);
                        }
                        if (!Level.CurrentLevel.Bunch.HasNeighbours(this))
                        {
                            Level.CurrentLevel.Bunch.RemoveBall(referenceHex);
                            MakeColorless();
                        }
                        break;
                    case BallType.Bomb:

                        Level.CurrentLevel.SpecialEffect.PlayBallEffect(Position, ItemType.BombBall);

                        Level.CurrentLevel.Stats.RemoveScoreForBomb(Position);
                        Level.CurrentLevel.Bunch.BombAllNeighbours(referenceHex, 0);
                        Delete(false);

                        BubblebreakSound.Play(BubblebreakSound.Explosion);
                        break;
                    default:
                        Level.CurrentLevel.Bunch.CheckForChain(referenceHex, Color);
                        break;
                }
                if (_State == BallState.FixedBall) // if it has not popped
                {
                    if (Vector2.DistanceSquared(_Position, Level.PlayfieldCenter) >= (Level.PlayfieldRadius - 2 * Radius / BubbleBreak.Scale) * (Level.PlayfieldRadius - 2 * Radius / BubbleBreak.Scale))
                        Level.CurrentLevel.QuitLevel(false);
                }

            }
        }

        internal void MakeColorless()
        {
            Item item = ItemFactory.SpawnItem(_Position);
            if (item != null)
            {
                BubblebreakSound.Play(BubblebreakSound.PopDestroy);
                Delete(false);
                Level.CurrentLevel.Item = item;
                return;
            }

            _State = BallState.ColorlessBall;
            Type = BallType.Normal; // through the setter to keep the visualization up to date
            Color = Color.White * (1 - BounceAlphaDecrease);
            ColorIndex = -1;

            double angle = BubbleBreak.Random.NextDouble() * MathHelper.TwoPi;
            _Velocity = ColorlessMovementSpeed * new Vector2((float)Math.Cos(angle), (float)Math.Sin(angle));

            ColorlessBallCount++;
            LevelManager.Instance.CurrentLevel.Stats.PoppedBalls++;

            BubblebreakSound.Play(BubblebreakSound.PopBunch);
        }

        internal void Delete(bool outOfLevelBounds)
        {
            Level.CurrentLevel.AllBalls.Remove(this);

            if (_State == BallState.ColorlessBall)
                ColorlessBallCount--;

            if (outOfLevelBounds)
                Level.CurrentLevel.Stats.EscapedBalls++;
        }

    }
}
