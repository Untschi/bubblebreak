﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Manipulation;
using Sunbow.Util.Sprites;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Bubblebreak
{
    public enum BounceLocation
    {
        Line,
        UpperArc,
        LowerArc,
    }

    public class BounceFeedback
    {
        static readonly Frame normalLine = BubbleBreak.Instance.GameSheet.GetFrame("default", "BounceLine");
        static readonly Frame upperLine = BubbleBreak.Instance.GameSheet.GetFrame("default", "BounceUpperLine");
        static readonly Frame lowerLine = BubbleBreak.Instance.GameSheet.GetFrame("default", "BounceLowerLine");
        static readonly Frame upperArc = BubbleBreak.Instance.GameSheet.GetFrame("default", "BounceUpperArc");
        static readonly Frame lowerArc = BubbleBreak.Instance.GameSheet.GetFrame("default", "BounceLowerArc");

        BounceLocation appearance;
        float offset;
        Color color;
        internal LinearSingleAnimator animator = new LinearSingleAnimator(1, 0, 1, AnimationRepeatLogic.Pause);

        public BounceFeedback(EventHandler endOfAnimation)
        {
            animator.EndOfAnimation += endOfAnimation;
        }

        public void Setup(BounceLocation appearance, float offset, Color color)
        {
            this.appearance = appearance;
            this.offset = offset;
            this.color = Color.White;

            animator.Reset();
            animator.Play();
        }

        public void Update(float seconds)
        {
            animator.Update(seconds);
        }

        public void Draw(SpriteBatch batch, float angle, float halfBarAngleSize)
        {
            switch(appearance)
            {
                case BounceLocation.Line:
                    BubbleBreak.Instance.GameSheet.Draw(batch, normalLine, Level.ActualCenter, angle + offset, 1, color * animator);
                    break;
                case BounceLocation.LowerArc:
                    BubbleBreak.Instance.GameSheet.Draw(batch, lowerArc, Level.ActualCenter, angle + halfBarAngleSize, 1, color * animator);
                    BubbleBreak.Instance.GameSheet.Draw(batch, upperLine, Level.ActualCenter, angle + halfBarAngleSize, 1, color * animator);
                    break;
                case BounceLocation.UpperArc:
                    BubbleBreak.Instance.GameSheet.Draw(batch, upperArc, Level.ActualCenter, angle - halfBarAngleSize, 1, color * animator);
                    BubbleBreak.Instance.GameSheet.Draw(batch, lowerLine, Level.ActualCenter, angle - halfBarAngleSize, 1, color * animator);
                    break;
            }
        }

    }
    public class BounceFeedbackPool
    {
        Queue<BounceFeedback> activeBounces = new Queue<BounceFeedback>();
        Queue<BounceFeedback> inactiveBounces = new Queue<BounceFeedback>();

        int deques = 0;

        public BounceFeedbackPool()
        {

        }

        public void AddBounce(BounceLocation appearance, float angleOffset, Color color)
        {
            BounceFeedback feedback;
            if (inactiveBounces.Count == 0)
                feedback = new BounceFeedback(GettingInactive);
            else
                feedback = inactiveBounces.Dequeue();

            feedback.Setup(appearance, angleOffset, color);
            activeBounces.Enqueue(feedback);
        }

        private void GettingInactive(object sender, EventArgs args)
        {
            deques++;
        }

        public void Update(float seconds)
        {
            for (int i = 0; i < deques; i++)
            {
                inactiveBounces.Enqueue(activeBounces.Dequeue());
            }
            deques = 0;

            foreach (BounceFeedback bf in activeBounces)
            {
                bf.Update(seconds);
            }
        }


        public void Draw(SpriteBatch batch, float barAngle, float halfBarAngleSize)
        {
            foreach (BounceFeedback bf in activeBounces)
            {
                bf.Draw(batch, barAngle, halfBarAngleSize);
            }
        }
    }
}
