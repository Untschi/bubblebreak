﻿using System;
using System.Collections.Generic;
using System.Text;

using Microsoft.Xna.Framework;

namespace Bubblebreak
{
    public class HexSpace
    {
        Vector2 _ZeroPoint, _VectorU, _VectorV, _VectorW;


        public HexSpace(Vector2 zeroPoint, float size)
        {
            _ZeroPoint = zeroPoint;

            size *= 0.53f; // HACK

            _VectorU = new Vector2(
                (float)Math.Cos(MathHelper.ToRadians(330)) * size,
                (float)Math.Sin(MathHelper.ToRadians(330)) * size);

            _VectorV = new Vector2(
                (float)Math.Cos(MathHelper.ToRadians(90)) * size,
                (float)Math.Sin(MathHelper.ToRadians(90)) * size);

            _VectorW = new Vector2(
                (float)Math.Cos(MathHelper.ToRadians(210)) * size,
                (float)Math.Sin(MathHelper.ToRadians(210)) * size);
        }


        public Vector2 GetPositionOfPoint(HexPoint point)
        {
            Vector2 result = _ZeroPoint;
            result.X += (_VectorU.X * point.U + _VectorV.X * point.V +_VectorW.X * point.W);
            result.Y += (_VectorU.Y * point.U + _VectorV.Y * point.V +_VectorW.Y * point.W);

            return result;

        }

    }
}
