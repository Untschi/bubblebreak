﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util.Sprites;
using Microsoft.Xna.Framework;
using Bubblebreak.Achievements;

namespace Bubblebreak
{

    public enum SmilieAnimation
    {
        undefined,
        WinkLeft,
        WinkRight,
        WinkBoth,
        Tongue,
    }

    public class Smilie
    {
        const int MAX_WAIT = 30;
        const float WINK_DURATION = 0.3f;
        const float TONGUE_DURATION = 0.6f;

        const int MOUTH_START = 31;
        const int DOUBLE_EYE_START = 21;
        const int NO_TONGUE_START = MOUTH_START + 26;
        const int WINK_IDX = 29;
        const int GHOST_IDX = MOUTH_START + 29;
        const int TONGUE_IDX = MOUTH_START + 30;
        const int FRAME_COUNT = 62;

        static List<Frame> frames = new List<Frame>();
        static Smilie()
        {
            FrameSet set = BubbleBreak.Instance.GameSheet.GetFrameSet("smile");
            for (int i = 0; i < FRAME_COUNT; i++)
            {
                frames.Add(set.CalculateFrame(i));
            }
        }

        int leftEye, rightEye, mouth;
        float timer;
        SmilieAnimation animation;


        Ball ball;

        public Smilie(Ball ball, AchievementType achievement)
        {
            this.ball = ball;

            if (achievement == AchievementType.undefined)
            {
                RandomizeFace();
            }
            else
            {
                leftEye = (int)achievement;
                rightEye = leftEye;
                mouth = MOUTH_START + (int)achievement;
            }

            SetWaitTimer();

            ball.Rotation = MathHelper.TwoPi / 6 * BubbleBreak.Random.Next(6);

        }

        public void RandomizeFace()
        {
            leftEye = (int)BubbleBreak.Instance.Achievements.GetRandomAvailableAchievement();
            rightEye = leftEye;
            mouth = MOUTH_START + (int)BubbleBreak.Instance.Achievements.GetRandomAvailableAchievement();
        }


        public void Update(float seconds)
        {


            timer -= seconds;

            if (timer <= -MAX_WAIT)
            {
                animation = (SmilieAnimation)(1 + BubbleBreak.Random.Next(4));

                switch(animation)
                {
                    case SmilieAnimation.Tongue:
                        if (mouth < NO_TONGUE_START)
                        {
                            timer = 1 + TONGUE_DURATION;
                        }
                        else
                        {
                            SetWaitTimer();
                            animation = SmilieAnimation.undefined;
                        }
                        break;
                    case SmilieAnimation.WinkBoth:
                    case SmilieAnimation.WinkLeft:
                    case SmilieAnimation.WinkRight:
                        if (leftEye < DOUBLE_EYE_START)
                        {
                            timer = 1 + WINK_DURATION;
                        }
                        else
                        {
                            SetWaitTimer();
                            animation = SmilieAnimation.undefined;
                        }
                        break;
                    default:
                        throw new Exception();
                }
            }
            else if (timer > 0 && timer <= 1)
            {
                SetWaitTimer();
                animation = SmilieAnimation.undefined;
            }
        
        }
        public void Draw(SpriteBatch batch)
        {
            Draw(batch, true);
        }

        public void Draw(SpriteBatch batch, bool transform)
        {
            bool isDoubleEyed = leftEye >= DOUBLE_EYE_START;
            //bool 

            Vector2 position = (transform) ? BubbleBreak.Transform(ball.Position) : ball.Position;
            float rotation = ball.Rotation;
            
            Frame currentMouth = (ball.State != BallState.ColorlessBall) 
                ? ((animation == SmilieAnimation.Tongue) 
                    ? frames[TONGUE_IDX] 
                    : frames[mouth]) 
                : frames[GHOST_IDX];

            BubbleBreak.Instance.GameSheet.Draw(batch, currentMouth, position, rotation, Vector2.One, Color.White, SpriteEffects.None, 0);

            Frame currentLeftEye = (animation == SmilieAnimation.WinkLeft || animation == SmilieAnimation.WinkBoth)
                ? frames[WINK_IDX]
                : frames[leftEye];
            BubbleBreak.Instance.GameSheet.Draw(batch, currentLeftEye, position, rotation, Vector2.One, Color.White, SpriteEffects.None, 0);
            
            if (!isDoubleEyed)
            {
                Frame currentRightEye = (animation == SmilieAnimation.WinkRight || animation == SmilieAnimation.WinkBoth)
                    ? frames[WINK_IDX] 
                    : frames[rightEye];
                BubbleBreak.Instance.GameSheet.Draw(batch, currentRightEye, position, rotation, Vector2.One, Color.White, SpriteEffects.FlipHorizontally, 0);
            }
        }

        void SetWaitTimer()
        {
            timer = -BubbleBreak.Random.Next(MAX_WAIT * 100) / 100f;
        }

        internal void DrawAsIcon(SpriteBatch batch, float transparency, float scale)
        {
            Vector2 s = new Vector2(scale, scale);
            Vector2 position = ball.Position;

            bool isDoubleEyed = leftEye >= DOUBLE_EYE_START;
            BubbleBreak.Instance.GameSheet.Draw(batch, frames[mouth], position, 0, s, Color.White * transparency, SpriteEffects.None, 0);

            BubbleBreak.Instance.GameSheet.Draw(batch, frames[leftEye], position, 0, s, Color.White * transparency, SpriteEffects.None, 0);

            if (!isDoubleEyed)
                BubbleBreak.Instance.GameSheet.Draw(batch, frames[rightEye], position, 0, s, Color.White * transparency, SpriteEffects.FlipHorizontally, 0);

        }
    }
}
