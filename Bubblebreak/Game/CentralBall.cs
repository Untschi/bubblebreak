﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util.Sprites;

namespace Bubblebreak
{
    public class CentralBall : Ball
    {
       // AnimatedSprite _Sprite;
        FrameAnimation animation;
        ColorChanger changer;

        public CentralBall()
            : base()
        {
            //Texture2D tex = BubbleBreak.Instance.Content.Load<Texture2D>("CentralBall");
            //_Sprite = new AnimatedSprite(tex, 14, 0.1f, true);
            animation = BubbleBreak.Instance.BallFrames.CreateAnimation("CentralBall");
            changer = new ColorChanger(3f, 0);

            //Color = Color.Black;
        }

        public override void Update(float seconds)
        {
            //_Sprite.Update(seconds);
            animation.Update(seconds);
            changer.Update(seconds);
        }

        public override void Draw(SpriteBatch batch, float transparency)
        {

            BubbleBreak.Instance.GameSheet.Draw(batch, animation.CurrentFrame, BubbleBreak.Transform(_Position), 0, 1, changer.CurrentColor);
            //batch.Draw(_Sprite.Texture, BubbleBreak.Transform(_Position), _Sprite.SourceRect, Color.White, 0, _Origin, 1, SpriteEffects.None, 1);
        }

    }
}
