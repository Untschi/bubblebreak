﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Bubblebreak
{
    public class BunchField
    {
        public static readonly Color StoneColor = Color.Gray;

        public HexSpace Space { get; private set; }

        Dictionary<HexPoint, Ball> _FixedBalls = new Dictionary<HexPoint, Ball>();
        public Dictionary<HexPoint, Ball> FixedBalls { get { return _FixedBalls; } }

        Dictionary<HexPoint, CentralBall> _CentralBalls = new Dictionary<HexPoint, CentralBall>();

        public int BallCount { get { return _FixedBalls.Count; } }
        public int StoneBallCount { get; private set; }

        public BunchField()
        {
            Space = new HexSpace(new Vector2(Level.PlayfieldCenterX, Level.PlayfieldCenterY), (2 / BubbleBreak.Scale) * Ball.Radius);
        }

        public bool AddBall(HexPoint point, Ball ball)
        {
            if (_FixedBalls.ContainsKey(point) || _FixedBalls.ContainsValue(ball) || _CentralBalls.ContainsKey(point))
                return false;

            _FixedBalls.Add(point, ball);
            ball.Position = Space.GetPositionOfPoint(point);
            ball.State = BallState.FixedBall;
            return true;
        }

        public bool AddCentralBall(HexPoint point, CentralBall ball)
        {
            if (_CentralBalls.ContainsKey(point) || _CentralBalls.ContainsValue(ball) || _FixedBalls.ContainsKey(point))
                return false;

            _CentralBalls.Add(point, ball);
            ball.Position = Space.GetPositionOfPoint(point);
            ball.State = BallState.FixedBall;
            ball.Type = BallType.Central;
            return true;
        }

        public bool RemoveBall(HexPoint point)
        {
            if (!_FixedBalls.ContainsKey(point))
                return false;

            _FixedBalls.Remove(point);
            return true;
        }
        public bool RemoveBall(Ball ball)
        {
            if (!_FixedBalls.ContainsValue(ball))
                return false;

            bool isRemoved = false;

            foreach (HexPoint h in _FixedBalls.Keys)
            {
                if (ball == _FixedBalls[h])
                {
                    _FixedBalls.Remove(h);
                    isRemoved = true;
                    break;
                }
            }
            return isRemoved;
        }

        private void DestroyCentralBall(HexPoint point)
        {
            Level.CurrentLevel.AllBalls.Remove(_CentralBalls[point]);
            Level.CurrentLevel.Stats.AddScoreForDestroyedCenralBall(Space.GetPositionOfPoint(point));
            _CentralBalls.Remove(point);
            CheckAllCentralBalls(0, point);
            
        }


        public void CheckCentralBallIntersection(Ball ball)
        {
            for (int i = 0; i < _CentralBalls.Count; i++)
            {
                if (Vector2.DistanceSquared(ball.Position, _CentralBalls.Values.ElementAt(i).Position) < Ball.IntersectingDistanceSquared)
                {
                    DestroyCentralBall(_CentralBalls.Keys.ElementAt(i));
                    Level.CurrentLevel.MagnetHit(ball);
                }
            }
        }
        public bool CheckForChain(HexPoint point, Color color)
        {
            if (_CentralBalls.ContainsKey(point))
            {
                DestroyCentralBall(point);
                return false;
            }

            List<HexPoint> chainBalls = new List<HexPoint>();

            bool killColor = false;
            CheckChainNeighbors(chainBalls, point, color, ref killColor);

            if (chainBalls.Count >= 3)
            {
                int counter = 0;
                if (killColor)
                {
                    chainBalls.Clear();
                    foreach(HexPoint p in _FixedBalls.Keys)
                    {
                        if (_FixedBalls[p].Color == color)
                            chainBalls.Add(p);
                    }
                }

                foreach (HexPoint p in chainBalls)
                {
                    if (_FixedBalls[p].Type == BallType.AllColor)
                    {
                        bool delete = true;
                        foreach (HexPoint h in GetAllNeighbourCoordinates(p))
                        {
                            if (!chainBalls.Contains(h) && _FixedBalls.ContainsKey(h))
                                delete = false;
                        }
                        if (!delete)
                            continue;
                    }
                    else
                    {
                        counter++; // all color balls don't count for score
                    }

                    _FixedBalls[p].MakeColorless();
                    RemoveBall(_FixedBalls[p]);
                }
                
                CheckAllCentralBalls(counter, (!killColor) ? point : HexPoint.Invalid);

                return true;                
            }
            return false;
        }
        private void CheckChainNeighbors(List<HexPoint> chainBalls, HexPoint p, Color color, ref bool killColor)
        {
            
            if (_FixedBalls.ContainsKey(p) 
                && !chainBalls.Contains(p) 
                && (_FixedBalls[p].Color == color || _FixedBalls[p].Type == BallType.AllColor))
            {
                chainBalls.Add(p);

                if (_FixedBalls[p].Type == BallType.ColorKiller)
                    killColor = true;
                

                CheckChainNeighbors(chainBalls, new HexPoint(p.U + 1, p.V, p.W - 1), color, ref killColor);
                CheckChainNeighbors(chainBalls, new HexPoint(p.U, p.V + 1, p.W - 1), color, ref killColor);
                CheckChainNeighbors(chainBalls, new HexPoint(p.U - 1, p.V + 1, p.W), color, ref killColor);
                CheckChainNeighbors(chainBalls, new HexPoint(p.U - 1, p.V, p.W + 1), color, ref killColor);
                CheckChainNeighbors(chainBalls, new HexPoint(p.U, p.V - 1, p.W + 1), color, ref killColor);
                CheckChainNeighbors(chainBalls, new HexPoint(p.U + 1, p.V - 1, p.W), color, ref killColor);
            }
        }

        public void CheckAllCentralBalls(int alreadyRemovedBalls, HexPoint point)
        {
            bool normalPop = alreadyRemovedBalls > 0 && !point.Equals(HexPoint.Invalid);

            int counter = alreadyRemovedBalls;
            List<Ball> stayingBalls = new List<Ball>();
            foreach (HexPoint h in _CentralBalls.Keys)
            {
                CheckConnectedNeighbors(stayingBalls, h);
            }

            for (int i = _FixedBalls.Count - 1; i >= 0; i--)
            {
                Ball b = _FixedBalls.Values.ElementAt(i);
                if (!stayingBalls.Contains(b))
                {
                    if (b.Color == StoneColor)
                    {
                        Level.CurrentLevel.Stats.AddScoreForePoppedStones(b.Position);
                        StoneBallCount--;
                    }

                    b.MakeColorless();
                    RemoveBall(b);
                    counter++;
                }
            }

            if (!point.Equals(HexPoint.Invalid))
                Level.CurrentLevel.Stats.AddScoreForPoppingBalls(counter, this.Space.GetPositionOfPoint(point), normalPop);


            CheckColors();

        }
        public IEnumerable<Ball> GetAllNeighbourBalls(HexPoint p)
        {
            foreach (HexPoint hp in GetAllNeighbourCoordinates(p))
            {
                if (_FixedBalls.ContainsKey(hp))
                    yield return _FixedBalls[hp];
            }
        }
        public IEnumerable<HexPoint> GetAllNeighbourCoordinates(HexPoint p)
        {
            yield return new HexPoint(p.U + 1, p.V, p.W - 1);
            yield return new HexPoint(p.U, p.V + 1, p.W - 1);
            yield return new HexPoint(p.U - 1, p.V + 1, p.W);
            yield return new HexPoint(p.U - 1, p.V, p.W + 1);
            yield return new HexPoint(p.U, p.V - 1, p.W + 1);
            yield return new HexPoint(p.U + 1, p.V - 1, p.W);
        }

        private void CheckConnectedNeighbors(List<Ball> balls, HexPoint p)
        {
            if (_FixedBalls.ContainsKey(p) && !balls.Contains(_FixedBalls[p]))
                balls.Add(_FixedBalls[p]);
            else if (_CentralBalls.ContainsKey(p) && !balls.Contains(_CentralBalls[p]))
                balls.Add(_CentralBalls[p]);
            else
                return;
                
            CheckConnectedNeighbors(balls, new HexPoint(p.U + 1,  p.V    ,  p.W - 1));
            CheckConnectedNeighbors(balls, new HexPoint(p.U    ,  p.V + 1,  p.W - 1));
            CheckConnectedNeighbors(balls, new HexPoint(p.U - 1,  p.V + 1,  p.W    ));
            CheckConnectedNeighbors(balls, new HexPoint(p.U - 1,  p.V    ,  p.W + 1));
            CheckConnectedNeighbors(balls, new HexPoint(p.U    ,  p.V - 1,  p.W + 1));
            CheckConnectedNeighbors(balls, new HexPoint(p.U + 1,  p.V - 1,  p.W    ));
        }

        private void CheckColors()
        {
            List<Color> colors = new List<Color>();
            foreach (Ball b in _FixedBalls.Values)
            {
                if (!colors.Contains(b.Color) && b.Color != StoneColor)
                    colors.Add(b.Color);
            }
            Level.CurrentLevel.ColorField.DeleteLostColors(colors);
        }


        internal void Delete()
        {
            _FixedBalls.Clear();
            _FixedBalls = null;
            _CentralBalls.Clear();
            _CentralBalls = null;

            Space = null;
        }

        internal void BombAllNeighbours(HexPoint p, int recursiveDepth)
        {
            foreach (HexPoint h in GetAllNeighbourCoordinates(p))
            {
                if(recursiveDepth < 1)
                BombAllNeighbours(h, recursiveDepth + 1);
                Bomb(h);
            }
            CheckAllCentralBalls(0, p);
        }

        private void Bomb(HexPoint p)
        {
            if (_CentralBalls.ContainsKey(p))
                DestroyCentralBall(p);
            else if (_FixedBalls.ContainsKey(p))
            {
                _FixedBalls[p].MakeColorless();
                RemoveBall(p);
            }
        }



        internal void ColorizeRandomBalls(Color color, float percent)
        {
            int count = (int)(percent * _FixedBalls.Count);

            for (int i = 0; i < count; i++)
            {
                int r = BubbleBreak.Random.Next(_FixedBalls.Count);
                Ball b = _FixedBalls.ElementAt(r).Value;

                if(b.Color == color)
                    i--;
                else
                    b.Color = color;
            }
        }

        public void RandomBallToCentralBall()
        {
            if (_FixedBalls.Count == 0)
                return;


            int r = BubbleBreak.Random.Next(_FixedBalls.Count);
            HexPoint p = _FixedBalls.ElementAt(r).Key;
            Level.CurrentLevel.AllBalls.Remove(_FixedBalls[p]);
            RemoveBall(p);

            Level.CurrentLevel.AddCentralBall(p);

            Level.CurrentLevel.SpecialEffect.PlayBallEffect(Space.GetPositionOfPoint(p), ItemType.RandomBallToCentral);
        }

        public void RandomBallToStone()
        {
            if (_FixedBalls.Count == 0)
                return;

            Ball b;
            do
            {
                int r = BubbleBreak.Random.Next(_FixedBalls.Count);
                b = _FixedBalls.ElementAt(r).Value;
            } while (b.Color == StoneColor);

            b.Color = StoneColor;
            StoneBallCount++;

            Level.CurrentLevel.SpecialEffect.PlayBallEffect(b.Position, ItemType.RandomBallToStone);
        }

        internal bool HasNeighbours(Ball ball)
        {
            int count = 0;
            HexPoint? coord = GetCoordinate(ball);
            if(coord == null)
                return false;
            foreach (Ball b in GetAllNeighbourBalls(coord.Value))
                count++;

            return count > 0;
        }

        private HexPoint? GetCoordinate(Ball ball)
        {
            foreach (HexPoint p in _FixedBalls.Keys)
            {
                if (_FixedBalls[p] == ball)
                    return p;
            }
            return null;
        }
    }
}
