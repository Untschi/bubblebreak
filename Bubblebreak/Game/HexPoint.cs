﻿using System;
namespace Bubblebreak
{
    public struct HexPoint : IEquatable<HexPoint>, IComparable<HexPoint>
    {
        public static readonly HexPoint Invalid = new HexPoint() { U = -1, V = -1, W = -1 };

        public static HexPoint CreateFromUV(int u, int v)
        {
            return new HexPoint(u, v, -(u + v));
        }
        public static HexPoint CreateFromUW(int u, int w)
        {
            return new HexPoint(u, -(u + w), w);
        }
        public static HexPoint CreateFromVW(int v, int w)
        {
            return new HexPoint(-(v + w), v, w);
        }


        public int U, V, W;

        public HexPoint(int u, int v, int w)
        {
            U = u;
            V = v;
            W = w;

            if ((u + v != -w) && (u + w != -v) && (v + w != -u))
                throw new Exception("this coordinate is impossible.");
        }

        public bool Equals(HexPoint other)
        {
            if (U == other.U && V == other.V && W == other.W)
                return true;
            return false;
        }

        public override string ToString()
        {
            return "Hex: " + U + " " + V + " " + W;
        }

        #region IComparable<HexPoint> Member

        public int CompareTo(HexPoint other)
        {
            if (U < other.U)
                return -1;
            else if (U > other.U)
                return +1;
            else
            {
                if (V < other.V)
                    return -1;
                else if (V > other.V)
                    return +1;
                else
                {
                    if (W < other.W)
                        return -1;
                    if (W > other.W)
                        return +1;
                    else
                        return 0;
                }
            }
        }

        #endregion

    }
}
