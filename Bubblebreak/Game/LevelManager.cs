﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Bubblebreak.Achievements;

namespace Bubblebreak
{
    public enum SwitchLogic
    {
        None,
        NextLevel,
        HighScore
    }

    public class LevelManager
    {
        const int MaxBarBallCount = 13;
        const float BarBallLevelStepDecrease = 2;
        const float ColorLevelStepIncrease = 3;
        const int LevelTypeCount = 10;
        const int TrialLevelTypeCount = 4;
        const int BonusTime = 120;


        public static LevelManager Instance { get; private set; }

        Hud _Hud = new Hud();
        public Hud Hud { get { return _Hud; } }

        ScoreBoard _ScoreBoard = new ScoreBoard();
        public ScoreBoard ScoreBoard { get { return _ScoreBoard; } }

        Level _CurrentLevel;
        public Level CurrentLevel { get { return _CurrentLevel; } }

        int _LevelNumber;
        public int CurrentLevelNumber { get { return _LevelNumber; } }

        public int TotalScore { get; set; }
        
        SwitchLogic _Switch;
        Action _ShootKeyUp;

        public AchievemenIngameVisualizer AchievementVisual { get; private set; }

        public Dictionary<ItemType, int> CollectedItemCounter = new Dictionary<ItemType, int>();
        public int EscapedSpecialsCount = 0;

        List<int> levelOrder = new List<int>();

        internal bool Sleep;

        public LevelManager()
        {
            _LevelNumber = 0;// BubbleBreak.Random.Next(60);
            TotalScore = 0;
            Instance = this;

            ShuffleLevels();
            NextLevel();

            _ShootKeyUp = new Action(OnShootKeyUp);
            BubblebreakInput.Instance.OnShootKeyUp += _ShootKeyUp;
            BubblebreakInput.Instance.OnPauseDown += new Action(OnPause);
            AchievementVisual = new AchievemenIngameVisualizer();

        }

        void ShuffleLevels()
        {
            levelOrder.Clear();

            List<int> tmp = new List<int>();
            int cnt = (BubbleBreak.IsTestVersion) ? TrialLevelTypeCount : LevelTypeCount;
            for (int i = 1; i < cnt; i++)
                tmp.Add(i);

            levelOrder.Add(0);

            while (tmp.Count > 0)
            {
                int idx = Sunbow.Util.Random.Next(tmp.Count);
                levelOrder.Add(tmp[idx]);
                tmp.RemoveAt(idx);
            }
        }
        void OnPause()
        {
            if (!Sleep)
            {
                //MenuManager.Instance.Sleep = false;
                //MenuManager.Instance.SetActiveMenu("PauseMenu");
                //Sleep = true;
                GameState.ModeProxy proxy = BubbleBreak.Instance.ModeProxy;
                proxy.GetMode<Modes.MainMenu>().EnterMode = typeof(Menu.PauseMenuMode);
                proxy.SetMode<Modes.MainMenu>();
            }
        }

        public void Start(int levelNum)
        {
            _LevelNumber = levelNum;
            NextLevel();
            _Hud.Reset();
            Sleep = false;
        }

        public void Update(float seconds)
        {
            if (!Sleep)
            {
                _CurrentLevel.Update(seconds);
                _Hud.Update(seconds);
                _ScoreBoard.Update(seconds);
                AchievementVisual.Update(seconds);
            }
        }
        public void OnShootKeyUp()
        {
            _Hud.Shoot();

            if (_Switch != SwitchLogic.None)
            {

                if (_Switch == SwitchLogic.NextLevel)
                {
                    _CurrentLevel.Delete();
                    _CurrentLevel = null;
                    GC.Collect();

                    NextLevel();
                    Hud.Reset();
                    _ScoreBoard.Hide();
                    _Hud.Show();
                }
                else
                {
                   // Sleep = true;
                    //MenuManager.Instance.SwitchTo("Highscore");
                    //(MenuManager.Instance._ActiveMenuScreen as HighScore).Show(new HighScoreEntry(TotalScore));
                    //MenuManager.Instance.Sleep = false;
                }
                _Switch = SwitchLogic.None;
            }
        }

        public void Draw(SpriteBatch batch)
        {
            if (!Sleep)
            {
                _CurrentLevel.Draw(batch);

                _ScoreBoard.Draw(batch);
                _CurrentLevel.FloatingScores.Draw(batch);
                AchievementVisual.Draw(batch);
            }
        }
        public void NextLevel()
        {
            _CurrentLevel = new Level();

            _CurrentLevel.SetBar(Math.Max(MaxBarBallCount - (int)(_LevelNumber / BarBallLevelStepDecrease), 3));
            _CurrentLevel.SetColorField(Math.Min(3 + (uint)(_LevelNumber / ColorLevelStepIncrease), ColorField.MaxColors));

            switch (levelOrder[(this._LevelNumber % ((BubbleBreak.IsTestVersion) ? TrialLevelTypeCount : LevelTypeCount))])
            {
                case 0:
                    this._CurrentLevel.AddCentralBall(new HexPoint(0, 0, 0));
                    this._CurrentLevel.CreateBunch(-3, 3, -3, 3, -3, 3, -3, 3, -3, 3, -3, 3);
                    break;

                case 1:
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUV(1, 1));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUV(-1, -1));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromVW(1, 1));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromVW(-1, -1));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUW(1, 1));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUW(-1, -1));
                    this._CurrentLevel.CreateBunch(1, 3, 1, 3, 1, 3, 1, 3, 1, 3, 1, 3);
                    this._CurrentLevel.CreateBunch(-3, -1, -3, -1, -3, -1, -3, -1, -3, -1, -3, -1);
                    break;

                case 2:
                    this._CurrentLevel.AddCentralBall(new HexPoint(0, 0, 0));
                    this._CurrentLevel.CreateBunch(-2, 3, -4, -4, -2, 3, -4, -4, -4, -4, -2, 3);
                    this._CurrentLevel.CreateBunch(-4, -4, -2, 3, -4, -4, -2, 3, -2, 3, -4, -4);
                    this._CurrentLevel.CreateBunch(-3, 3, 3, 3, -3, 3, 3, 3, 3, 3, -3, 3);
                    this._CurrentLevel.CreateBunch(3, 3, -3, 3, 3, 3, -3, 3, -3, 3, 3, 3);
                    this._CurrentLevel.CreateBunch(-2, 2, -2, -2, -2, 2, -2, -2, -2, -2, -2, 2);
                    this._CurrentLevel.CreateBunch(-2, -2, -2, 2, -2, -2, -2, 2, -2, 2, -2, -2);
                    this._CurrentLevel.CreateBunch(-1, 1, 1, 1, -1, 1, 1, 1, 1, 1, -1, 1);
                    this._CurrentLevel.CreateBunch(1, 1, -1, 1, 1, 1, -1, 1, -1, 1, 1, 1);
                    break;

                case 3:
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUW(0, -2));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUW(0, 2));
                    this._CurrentLevel.CreateBunch(-4, 0, -3, 0, 0, 4, -4, 0, 0, 4, 0, 3);
                    break;

                case 4:
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUV(-1, 2));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromVW(-2, 1));
                    this._CurrentLevel.CreateBunch(-1, 1, 1, 2, 1, 2, -1, 1, 1, 2, 1, 2);
                    this._CurrentLevel.CreateBunch(-1, 1, -2, -1, -2, -1, -1, 1, -2, -1, -2, -1);
                    break;

                case 5:
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUV(-1, 2));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromVW(-1, -1));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromVW(-1, 3));
                    this._CurrentLevel.CreateBunch(-3, -1, -2, 0, -2, 0, -2, 0, -2, 0, -2, 0);
                    break;

                case 6:
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUV(2, 0));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUV(-2, 0));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUW(1, 1));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUW(-1, -1));
                    this.CreateSnowflake(this._CurrentLevel, HexPoint.CreateFromUV(3, 0));
                    this.CreateSnowflake(this._CurrentLevel, HexPoint.CreateFromUV(-3, 0));
                    this.CreateSnowflake(this._CurrentLevel, HexPoint.CreateFromUW(-2, -2));
                    this.CreateSnowflake(this._CurrentLevel, HexPoint.CreateFromUW(2, 2));
                    break;

                case 7:
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUV(1, 1));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUW(1, 1));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromVW(1, 1));
                    this._CurrentLevel.CreateBunch(0, 3, 0, 0, 0, 3, 0, 0, 0, 0, 0, 3);
                    this._CurrentLevel.CreateBunch(0, 0, 0, 3, 0, 0, 0, 3, 0, 3, 0, 0);
                    this._CurrentLevel.CreateBunch(-3, 3, -3, -3, -3, 3, -3, -3, -3, -3, -3, 3);
                    this._CurrentLevel.CreateBunch(3, 3, -6, 0, -3, 3, 3, 3, 3, 3, 3, 3);
                    this._CurrentLevel.CreateBunch(-3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3);
                    this._CurrentLevel.CreateBunchBall(HexPoint.CreateFromUV(-5, 5));
                    this._CurrentLevel.CreateBunchBall(HexPoint.CreateFromUV(-5, 4));
                    this._CurrentLevel.CreateBunchBall(HexPoint.CreateFromVW(-5, 5));
                    this._CurrentLevel.CreateBunchBall(HexPoint.CreateFromVW(-5, 4));
                    this._CurrentLevel.CreateBunchBall(HexPoint.CreateFromUW(5, -5));
                    this._CurrentLevel.CreateBunchBall(HexPoint.CreateFromUW(4, -5));
                    break;

                case 8:
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUV(0, -2));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUV(0, 2));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUW(-2, 0));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUW(2, 0));
                    this._CurrentLevel.CreateBunch(-1, 1, -3, 3, -3, 3, -1, 1, -1, 1, -1, 1);
                    this._CurrentLevel.CreateBunch(0, -1, -5, -5, 0, 3, -5, -5, -5, -5, 2, 5);
                    this._CurrentLevel.CreateBunch(0, -1, 5, 5, -3, 0, 5, 5, 5, 5, -5, -2);
                    break;

                case 9:
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUV(-3, 3));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromUW(3, -3));
                    this._CurrentLevel.AddCentralBall(HexPoint.CreateFromVW(-3, 3));
                    this.CreateSnowflake(this._CurrentLevel, HexPoint.CreateFromUV(-3, 3));
                    this.CreateSnowflake(this._CurrentLevel, HexPoint.CreateFromUV(3, -3));
                    this.CreateSnowflake(this._CurrentLevel, HexPoint.CreateFromUW(-3, 3));
                    this.CreateSnowflake(this._CurrentLevel, HexPoint.CreateFromUW(3, -3));
                    this.CreateSnowflake(this._CurrentLevel, HexPoint.CreateFromVW(-3, 3));
                    this.CreateSnowflake(this._CurrentLevel, HexPoint.CreateFromVW(3, -3));
                    break;

                default:
                    throw new NotImplementedException();
            }
            CurrentLevel.Stats.LevelStartBallCount = CurrentLevel.Bunch.BallCount;
            this._LevelNumber++;
        }
        private void CreateSnowflake(Level level, HexPoint point)
        {
            level.CreateBunchBall(point);
            level.CreateBunchBall(HexPoint.CreateFromUV(point.U - 1, point.V));
            level.CreateBunchBall(HexPoint.CreateFromUV(point.U - 2, point.V + 1));
            level.CreateBunchBall(HexPoint.CreateFromUV(point.U + 1, point.V));
            level.CreateBunchBall(HexPoint.CreateFromUV(point.U + 2, point.V - 1));
            level.CreateBunchBall(HexPoint.CreateFromUW(point.U, point.W - 1));
            level.CreateBunchBall(HexPoint.CreateFromUW(point.U + 1, point.W - 2));
            level.CreateBunchBall(HexPoint.CreateFromUW(point.U, point.W + 1));
            level.CreateBunchBall(HexPoint.CreateFromUW(point.U - 1, point.W + 2));
            level.CreateBunchBall(HexPoint.CreateFromVW(point.V - 1, point.W));
            level.CreateBunchBall(HexPoint.CreateFromVW(point.V - 2, point.W + 1));
            level.CreateBunchBall(HexPoint.CreateFromVW(point.V + 1, point.W));
            level.CreateBunchBall(HexPoint.CreateFromVW(point.V + 2, point.W - 1));
        }



        internal void FinishLevel(bool hasWon, bool checkAchievements)
        {
            _Hud.Timer.Stop();
            _Hud.Hide();

            if (hasWon)
            {
                if (checkAchievements && CheckCompleteLevelAchievements())
                    return;

                if (BubbleBreak.IsTestVersion && _LevelNumber == 2)
                {
                    BubbleBreak.Instance.Achievements.AnotherGamePlayed();

                    _ScoreBoard.SetEndOfTestVersion(_CurrentLevel.Stats);
                    _Switch = SwitchLogic.HighScore;
                }
                else
                {
                    _ScoreBoard.SetLevelCompleted(_LevelNumber, _CurrentLevel.Bar.GetBallCount(), _CurrentLevel.Stats);
                    _Switch = SwitchLogic.NextLevel;
                    BubblebreakSound.Play(BubblebreakSound.LevelUp);
                }
            }
            else if (_Switch != SwitchLogic.HighScore)
            {
                if (checkAchievements && CheckGameOverAchievements())
                    return;

                BubbleBreak.Instance.Achievements.AnotherGamePlayed();

                _ScoreBoard.SetGameOver(_CurrentLevel.Stats);
                _Switch = SwitchLogic.HighScore;
            }

            BubbleBreak.Instance.Achievements.Save();
            _ScoreBoard.Show();
        }

        public void Delete()
        {
            if (_CurrentLevel != null)
                _CurrentLevel.Delete();
            _CurrentLevel = null;
            _Hud.Delete();
            _Hud = null;
            _ScoreBoard = null;

            BubblebreakInput.Instance.OnShootKeyUp -= _ShootKeyUp;
            _ShootKeyUp = null;
        }


        private bool CheckCompleteLevelAchievements()
        {
            bool achieved = false;

            if (this._LevelNumber == 1)
                BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.FirstLevelDone, ref achieved);
            else if (this._LevelNumber == 10)
                BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.Level10Done, ref achieved);
            else if (this._LevelNumber == 15)
                BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.Level15Done, ref achieved);
            else if (this._LevelNumber == 18)
                BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.Level18Done, ref achieved);

            if (CurrentLevel.Stats.ShotBalls == 1)
            {
                if (!BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteWith1Shot, ref achieved))
                    if (!BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteWith2Shots, ref achieved))
                        BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteWith3Shots, ref achieved);
            }
            else if (CurrentLevel.Stats.ShotBalls <= 2)
            {
                if (!BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteWith2Shots, ref achieved))
                    BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteWith3Shots, ref achieved);
            }
            else if (CurrentLevel.Stats.ShotBalls <= 3)
            {
                BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteWith3Shots, ref achieved);
            }

            if (CurrentLevel.Stats.ElapsedTime <= 5)
            {
                if (!BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteIn5Seconds, ref achieved))
                    if (!BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteIn10Seconds, ref achieved))
                        BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteIn30Seconds, ref achieved);
            }
            else if (CurrentLevel.Stats.ElapsedTime <= 10)
            {
                if (!BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteIn10Seconds, ref achieved))
                    BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteIn30Seconds, ref achieved);
            }
            else if (CurrentLevel.Stats.ElapsedTime <= 30)
            {
                BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteIn30Seconds, ref achieved);
            }

            if (CurrentLevel.Stats.BouncedBalls == 0)
            {
                BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.AllBallsEscaped, ref achieved);
            }
            else if (CurrentLevel.Stats.EscapedBalls == 0)
            {
                BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.NoBallEscaped, ref achieved);
            }

            if (CurrentLevel.Bar.GetBallCount() == 0)
            {
                BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.FinishLevelWithTheLastShot, ref achieved);
            }

            if (_CurrentLevel.Stats.PlayedColors.Count <= 1)
            {
                BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.PlayOnly1ColorInALevel, ref achieved);
            }


            if (achieved)
                AchievementVisual.Switch = SwitchLogic.NextLevel;

            return achieved;
        }

        private bool CheckGameOverAchievements()
        {
            bool achieved = false;


            if (CollectedItemCounter.Count > 0 || EscapedSpecialsCount > 0)
            {
                if (CollectedItemCounter.Count == 0)
                    BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CollectNoItemDuringAGame, ref achieved);
                else if (EscapedSpecialsCount == 0)
                    BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CollectAllItemsDuringAGame, ref achieved);
            }
            if (CollectedItemCounter.Count == (int)ItemType._Count)
                BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CollectAllItemTypesInAGame, ref achieved);

            if (TotalScore < 0)
                BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.GetANegativeHighscore, ref achieved);


            if (achieved)
                AchievementVisual.Switch = SwitchLogic.HighScore;

            return achieved;
        }
    }
}
