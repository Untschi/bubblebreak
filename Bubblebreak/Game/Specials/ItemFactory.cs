using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Bubblebreak
{
    public static class ItemFactory
    {
        static ItemType lastItem = ItemType.Undefined;

        public static Item SpawnItem(Vector2 position)
        {
            if (float.IsNaN(position.X) || float.IsNaN(position.Y))
                return null;


            // make sure there is only one item / special every time
            if (Level.CurrentLevel.Item != null || !Level.CurrentLevel.CurrentSpecial.IsInstantType())
                return null;

            if (BubbleBreak.Random.Next(100) > 9)
                return null;

            Item item = new Item(position);
            ItemType t;

            do
            {
                t = ((ItemType)BubbleBreak.Random.Next((int)ItemType._Count));
            } while (!IsTypeAllowed(t));

            item.Type = t;
            lastItem = t;

            return item;
        }

        private static bool IsTypeAllowed(ItemType type)
        {
            if (type == lastItem)
                return false;

            switch (type)
            {
                case ItemType.ColorKillerBall:
                case ItemType.RandomBallToStone:
                case ItemType.RandomBallToCentral:
                    {
                        // no colored ball available
                        int coloredBalls = Level.CurrentLevel.Bunch.BallCount - Level.CurrentLevel.Bunch.StoneBallCount;
                        return coloredBalls > 1;
                    }
                case ItemType.ColorRandomizer:
                    {
                        // only one color left
                        return Level.CurrentLevel.ColorField.ColorCount > 1;
                    }
                case ItemType.ExtraColor:
                    {
                        // color maximum arrived
                        return Level.CurrentLevel.ColorField.ColorCount >= ColorField.MaxColors;
                    }
                default:
                    {
                        return true;
                    }
            }
        }
    }
}
