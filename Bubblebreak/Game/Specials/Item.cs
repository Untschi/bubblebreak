using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Sunbow.Util.Sprites;

namespace Bubblebreak
{
    public class Item
    {
        const float Velocity = 25;


        public ItemType Type { 
            get { return _Type; } 
            set { 
                _Type = value;
                _Frame = BubbleBreak.Instance.GameSheet.GetFrame("SmallItems", value.ToString());
            } 
        }
        ItemType _Type;
        Frame _Frame;

        Vector2 _Position;
        Vector2 _Direction;
        float _Rotation;

        public Item(Vector2 spawnPosition)
        {
            _Position = spawnPosition;
            _Direction = Vector2.Normalize(_Position - Level.PlayfieldCenter);
            _Rotation = ((float)Math.Atan2(_Direction.Y, _Direction.X) + MathHelper.TwoPi) % MathHelper.TwoPi;
        }

        public void Update(float seconds)
        {
            _Position += (Velocity * seconds) * _Direction;

            float distThresh = (Level.PlayfieldRadius - Ball.Radius / BubbleBreak.Scale);
            float sqDist = Vector2.DistanceSquared(_Position, Level.PlayfieldCenter);

            if (sqDist > distThresh * distThresh)
            {
                if (Level.CurrentLevel.Bar.IsInBounds(MathHelper.WrapAngle(_Rotation)))
                {
                    Collect();
                    Level.CurrentLevel.Item = null;
                    Level.CurrentLevel.SetSpecial(_Type);
                }
                else if (sqDist > Level.PlayfieldRadius * Level.PlayfieldRadius)
                {
                    Level.CurrentLevel.Item = null;
                    LevelManager.Instance.EscapedSpecialsCount++;
                }
            }
        }

        public void Draw(SpriteBatch batch)
        {
           // batch.Draw(_Sprite, BubbleBreak.Transform(_Position), null, Color.White, _Rotation, new Vector2(5, 10), 1, SpriteEffects.None, 0);
            BubbleBreak.Instance.GameSheet.Draw(batch, _Frame, BubbleBreak.Transform(_Position), _Rotation + MathHelper.PiOver2, 1);
        }

        public void Collect()
        {
            Level.CurrentLevel.Stats.CollectedItems[_Type]++;

            switch (_Type)
            {
                case ItemType.ExtraBall:


                    Ball ball = new Ball();
                    Level.CurrentLevel.AllBalls.Add(ball);
                    Level.CurrentLevel.Bar.AddBall(ball);

                    Level.CurrentLevel.Stats.RemoveScoreForExtraBall(Level.CurrentLevel.Bar.GetBallCount() - 1, LevelManager.Instance.CurrentLevelNumber);
                    break;

                case ItemType.ColorRandomizer:

                    foreach (Ball b in Level.CurrentLevel.AllBalls)
                        if (b.State == BallState.FixedBall && b.Color != BunchField.StoneColor)
                            b.Color = Level.CurrentLevel.ColorField.GetRandomColor();
                    break;

                case ItemType.BallsFromEverywhere:
                    Level.CurrentLevel.ColorField.ThowBallFromEverywhere();
                    break;

                case ItemType.ExtraColor:
                    Level.CurrentLevel.ColorField.CreateExtraColor();
                    break;

                case ItemType.RandomBallToCentral:
                    Level.CurrentLevel.Bunch.RandomBallToCentralBall();
                    break;

                case ItemType.RandomBallToStone:
                    Level.CurrentLevel.Bunch.RandomBallToStone();
                    break;

                default:
                    break;
            }
            switch (_Type)
            {
                case ItemType.RandomBallToCentral:
                    BubblebreakSound.Play(BubblebreakSound.ToMagnet);
                    break;
                case ItemType.RandomBallToStone:
                    BubblebreakSound.Play(BubblebreakSound.ToStone);
                    break;
                default:
                    BubblebreakSound.Play(BubblebreakSound.CollectSpecial);
                    break;
            }
            LevelManager.Instance.Hud.SetItem(_Type);
        }
    }
}
