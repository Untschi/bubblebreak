using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Bubblebreak
{
    public enum ItemType
    {
        // outside white
        ExtraBall,              // green
        AllColorBall,           // multi colors
        BombBall,               // yellow
        ColorKillerBall,        // red
        MovableBall,            // blue

        // outside gray
        BallsFromEverywhere,    // turqouise

        // outside black
        ColorRandomizer,        // purple
        RandomBallToCentral,    // black
        RandomBallToStone,      // brown
        ExtraColor,             // multi color

        _Count,
        Undefined,
    }

    public static class ItemTypeExtensions
    {
        public static bool IsInstantType(this ItemType self)
        {
            switch(self)
            {
                case ItemType.Undefined:
                case ItemType.ExtraBall:
                case ItemType.ColorRandomizer:
                case ItemType.BallsFromEverywhere:
                case ItemType.ExtraColor:
                case ItemType.RandomBallToCentral:
                case ItemType.RandomBallToStone:
                    return true;

                default:
                    return false;
            }
        }
    }
}
