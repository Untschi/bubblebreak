﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Sunbow.Util.Sprites;
using Bubblebreak.Game.Gui;
using Sunbow.Util.Manipulation;

namespace Bubblebreak
{
    public class Level
    {
        public const float PlayfieldRadius = 113;
        public const float PlayfieldCenterX = 120;
        public const float PlayfieldCenterY = 120;
        public static Vector2 PlayfieldCenter { get { return new Vector2(PlayfieldCenterX, PlayfieldCenterY); } }

        public static readonly Vector2 ActualCenter = new Vector2(400, 240);

        public static Level CurrentLevel { get; protected set; }

        public static SpriteEffects Flip { get { return (BubbleBreak.PlayingFieldFlipped) ? SpriteEffects.FlipHorizontally : SpriteEffects.None; } }

        Item _Item;
        public Item Item { get { return _Item; } set { _Item = value; } }

        ItemType _CurrentSpecial;
        public ItemType CurrentSpecial { get { return _CurrentSpecial; } }

        List<Ball> _AllBalls = new List<Ball>();
        public List<Ball> AllBalls { get { return _AllBalls; } }

        Bar _Bar;
        public Bar Bar { get { return _Bar; } }
        
        ColorField _ColorField;
        public ColorField ColorField { get { return _ColorField; } }

        BunchField _Bunch;
        public BunchField Bunch { get { return _Bunch; } }

        //Texture2D _Background;
        ColorChanger _BackgroundColor = new ColorChanger(2, 0.7f);
        public Color BackgroundColor { get { return _BackgroundColor.CurrentColor; } }

        FrameAnimation _Line;
        Frame _Background, _ColorWindow, _Cog;

        public readonly BallSpecialEffect SpecialEffect = new BallSpecialEffect();

        public FloatingScoreManager FloatingScores { get; private set; }

        SinusSingleAnimator blendAnimator = new SinusSingleAnimator(0, MathHelper.TwoPi, 0.25f, 0.333f, 3, AnimationRepeatLogic.Repeat);

        public LevelStats Stats { get; private set; }

        bool _LevelFinished = false;

        Ball lastMagnetKillerBall;
        int magnetsKilledByBall;

        public Level()
        {
            CurrentLevel = this;
            // _Background = background;
            Ball b = new Ball();
            _Bunch = new BunchField();
            Stats = new LevelStats();
            FloatingScores = new FloatingScoreManager();

            _Line = BubbleBreak.Instance.GameSheet.CreateAnimation("DashedLine", "LineAnimation");//new AnimatedSprite(BubbleBreak.Instance.Content.Load<Texture2D>("Line"), 3, 0.1f, false);
            _Background = BubbleBreak.Instance.GameSheet.GetFrame("default", "BackgroundNormal");
            _ColorWindow = BubbleBreak.Instance.GameSheet.GetFrame("default", "ColorWindow");
            _Cog = BubbleBreak.Instance.GameSheet.GetFrame("default", "Cog");
        }

        public void Update(float seconds)
        {
            Stats.Update(seconds);

            _Line.Update(seconds);

            if(_Item != null)
                _Item.Update(seconds);
            
            
            //foreach (Ball b in _AllBalls)
            for(int i = 0; i < _AllBalls.Count; i++)
            {
                _AllBalls[i].Update(seconds);
            }
            _Bar.Update(seconds);

            _ColorField.Update(seconds);

            FloatingScores.Update(seconds);

            _BackgroundColor.Update(seconds);

            blendAnimator.Update(seconds);

            SpecialEffect.Update(seconds);
        }

        public void Draw(SpriteBatch batch)
        {
            float addAngle = (BubbleBreak.PlayingFieldFlipped) ? MathHelper.Pi : 0;
            float cogAngle = _ColorField.Angle % MathHelper.ToRadians(10);
            _ColorField.Draw(batch);

            BubbleBreak.Instance.GameSheet.Draw(batch, _ColorWindow, ActualCenter, addAngle, 1);
            BubbleBreak.Instance.GameSheet.Draw(batch, _Cog, ActualCenter, cogAngle + addAngle, 1);
            BubbleBreak.Instance.GameSheet.Draw(batch, _Background, Vector2.Zero, 0, 1, _BackgroundColor.CurrentColor, Flip, 0);

            LevelManager.Instance.Hud.Draw(batch);

            Color c = ColorField.GetColor(BubblebreakInput.Instance.CurrentBarAngle);

            BubbleBreak.Instance.GameSheet.Draw(batch, _Line.CurrentFrame, new Vector2(400, 240), BubblebreakInput.Instance.CurrentBarAngle, 1, c);


            //batch.End();

            //batch.Begin();
            foreach (Ball b in _AllBalls)
                b.Draw(batch, 1); 
            
            _Bar.Draw(batch);

            batch.End();

            batch.Begin(SpriteSortMode.Immediate, BlendState.Additive);
            foreach (Ball b in _AllBalls)
                if (b.Color == c)
                    b.Draw(batch, 0.333f + 0.5f * blendAnimator);
                else
                    b.Draw(batch, 0.2f + 0.5f * blendAnimator);
            batch.End();

            batch.Begin();
            if(_Item != null)
                _Item.Draw(batch);

            SpecialEffect.Draw(batch);
        }

        public void AddCentralBall(HexPoint h)
        {
            CentralBall ball = new CentralBall();
            _AllBalls.Add(ball);
            _Bunch.AddCentralBall(h, ball);
        }
        public void CreateBunch(
            int uvUmin, int uvUmax, int uvVmin, int uvVmax,
            int vwVmin, int vwVmax, int vwWmin, int vwWmax,
            int uwUmin, int uwUmax, int uwWmin, int uwWmax)
        {

            for (int u = uvUmin; u <= uvUmax; u++)
            {
                for (int v = uvVmin; v <= uvVmax; v++)
                {
                    CreateBunchBall(HexPoint.CreateFromUV(u, v));
                }
            }

            for (int v = vwVmin; v <= vwVmax; v++)
            {
                for (int w = vwWmin; w <= vwWmax; w++)
                {
                    CreateBunchBall(HexPoint.CreateFromVW(v, w));
                }
            }
            for (int u = uwUmin; u <= uwUmax; u++)
            {
                for (int w = uwWmin; w <= uwWmax; w++)
                {
                    CreateBunchBall(HexPoint.CreateFromUW(u, w));
                }
            }

        }
        public void CreateBunchBall(HexPoint point)
        {
            Ball ball = new Ball();
            ball.Color = _ColorField.GetRandomColor();
            if (_Bunch.AddBall(point, ball))
                _AllBalls.Add(ball);
        }

        public void SetBar(int ballCount)
        {
            _Bar = new Bar();
            for (int i = 0; i < ballCount; i++)
            {
                Ball b = new Ball();
                _AllBalls.Add(b);
                _Bar.AddBall(b);
            }
        }

        public void SetColorField(uint colorCount)
        {
            _ColorField = new ColorField(colorCount);
        }

        public void QuitLevel(bool hasWon)
        {
            if (_LevelFinished)
            {
                if (hasWon && LevelManager.Instance.ScoreBoard.IsGameOver)
                    BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.CompleteLevelAfterGameOver);
                else
                    return;
            }
            _Item = null;
            _CurrentSpecial = ItemType.Undefined;
            LevelManager.Instance.FinishLevel(hasWon, true);
            _LevelFinished = true;
        }

        public void Delete()
        {
            _Background = null;
            _BackgroundColor = null;
            _Bar.Delete();
            _Bunch.Delete();
            _ColorField.Delete();

            while (_AllBalls.Count > 0)
            {
                _AllBalls[0].Delete(false);
                //_AllBalls.RemoveAt(0);
            }
            _AllBalls = null;

           // BubblebreakInput.Instance.ClearEvents();

            GC.Collect();
        }

        public void SetSpecial(ItemType special)
        {
            _CurrentSpecial = special;

            if (Bar.ShootBall != null)
            {
                switch (special)
                {
                    case ItemType.AllColorBall:
                        Bar.ShootBall.Type = BallType.AllColor;
                        break;
                    case ItemType.BombBall:
                        Bar.ShootBall.Type = BallType.Bomb;
                        break;
                    case ItemType.ColorKillerBall:
                        Bar.ShootBall.Type = BallType.ColorKiller;
                        break;
                    case ItemType.MovableBall:
                        Bar.ShootBall.Type = BallType.MovableBall;
                        break;
                    default:
                        break;
                }
            }

            if (special != ItemType.Undefined)
            {
                if (!LevelManager.Instance.CollectedItemCounter.ContainsKey(special))
                    LevelManager.Instance.CollectedItemCounter.Add(special, 1);
                else
                    LevelManager.Instance.CollectedItemCounter[special]++;
            }
        }

        public void MagnetHit(Ball killer)
        {
            if (killer == lastMagnetKillerBall)
            {
                magnetsKilledByBall++;

                if (magnetsKilledByBall >= 3)
                {
                    BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.Hit3MagnetsWithOneBubble);
                }
            }
            else
            {
                lastMagnetKillerBall = killer;
                magnetsKilledByBall = 1;
            }
        }

    }
}
