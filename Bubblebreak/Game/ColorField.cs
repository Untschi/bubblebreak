﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using Sunbow.Util.Sprites;

namespace Bubblebreak
{
    public class ColorField
    {
        public const int MaxColors = 9;
        const float RotationSpeed = MathHelper.TwoPi / 60;

        static readonly List<Color> colors = new Color[]
        {              
            Color.Red,
            Color.Yellow,
            Color.Blue,
            Color.LawnGreen,
            Color.DarkMagenta,
            Color.DarkOliveGreen,
            Color.Orange,
            Color.Turquoise,
            Color.Violet,
        }.ToList();

        internal static int GetColorIndex(Color color)
        {
            return colors.IndexOf(color);
        }
        internal static Color GetColor(int index)
        {
            return colors[index];
        }

        static Frame _Frame;

        List<Color> _Colors = new List<Color>();
        public int ColorCount { get { return _Colors.Count; } }

        float _CenterDistance, _AngleStep;
        Vector2 _Scale;
        float _Angle, _AngleOffset;

        public bool IsUserControl { get { return _IsUserControl; } }
        bool _IsUserControl, _IsRotationClockwise;

        public float Angle { get { return _Angle; } }

        Action _ColorKeyDown, _ColorKeyUp;



        public ColorField(uint colorCount)
        {
            if (_Frame == null)
                _Frame = BubbleBreak.Instance.GameSheet.GetFrame("default", "ColorArc");

            for (int i = 1; i <= colorCount; i++)
                _Colors.Add(GetNextColor(i));

            CalculateTransform();
            _IsRotationClockwise = true;

            _ColorKeyDown = new Action(OnColorKeyDown);
            _ColorKeyUp = new Action(OnColorKeyUp);
            BubblebreakInput.Instance.OnColorKeyDown += _ColorKeyDown;
            BubblebreakInput.Instance.OnColorKeyUp += _ColorKeyUp;
        }

        private Color GetNextColor(int colorNumber)
        {
            Color color;
            do
            {
                color = colors[colorNumber - 1];
                //switch (colorNumber)
                //{
                //    case 1:
                //        color = Color.Red;
                //        break;
                //    case 2:
                //        color = Color.Yellow;
                //        break;
                //    case 3:
                //        color = Color.Blue;
                //        break;
                //    case 4:
                //        color = Color.LawnGreen;
                //        break;
                //    case 5:
                //        color = Color.DarkMagenta;
                //        break;
                //    case 6:
                //        color = Color.DarkOliveGreen;
                //        break;
                //    case 7:
                //        color = Color.Orange;
                //        break;
                //    case 8:
                //        color = Color.Turquoise;
                //        break;
                //    case 9:
                //        color = Color.Violet;
                //        break;
                //    default:
                //        throw new Exception("This number of colors is not allowed.");
                //}

                colorNumber--;
                if (colorNumber <= 0)
                    colorNumber = MaxColors;

            } while (_Colors.Contains(color));

            return color;
        }

        void OnColorKeyUp()
        {
            _IsUserControl = false;
            _IsRotationClockwise = !_IsRotationClockwise;
        }

        void OnColorKeyDown()
        {
            _AngleOffset = _Angle - BubblebreakInput.Instance.ColorAngle;
            _IsUserControl = true;
        }

        public void RemoveColor(Color color)
        {
            if (_Colors.Contains(color))
                _Colors.Remove(color);

            CalculateTransform();

            if (_Colors.Count == 0)
                Level.CurrentLevel.QuitLevel(true);
        }

        public void Update(float seconds)
        {
            if (!_IsUserControl)
                _Angle = (_IsRotationClockwise) 
                    ? (_Angle + seconds * RotationSpeed) 
                    : (_Angle - seconds * RotationSpeed);
            else
                _Angle = _AngleOffset + BubblebreakInput.Instance.ColorAngle;

            if (_Angle >= MathHelper.TwoPi)
                _Angle -= MathHelper.TwoPi;
        }

        public void Draw(SpriteBatch batch)
        {
            //Vector2 pos = Vector2.Zero;
            for (int i = 0; i < 3 * _Colors.Count; i++)
            {
                //pos.X = BubblebreakInput.Instance.Center.X + (float)Math.Cos(_Angle + i * _AngleStep) * _CenterDistance;
                //pos.Y = BubblebreakInput.Instance.Center.Y + (float)Math.Sin(_Angle + i * _AngleStep) * _CenterDistance;

                //batch.Draw(_ColorItem, BubbleBreak.Transform(pos), null, _Colors[i % _Colors.Count], 0, new Vector2(50, 50), _Scale, SpriteEffects.None, 1);
                BubbleBreak.Instance.GameSheet.Draw(batch, _Frame, Level.ActualCenter, _Angle + i * _AngleStep, _Scale, _Colors[i % _Colors.Count]);
            }
        }

        public Color GetColorLight(float angle, float lightValue)
        {
            Vector3 v = GetColor(angle).ToVector3();

            float diff = 1 - lightValue;
            v.X = lightValue + v.X * diff;
            v.Y = lightValue + v.Y * diff;
            v.Z = lightValue + v.Z * diff;

            return new Color(v);
        }
        public Color GetColor(float angle)
        {
            if (_Colors.Count == 0)
                return Color.White;

            angle -= _Angle - _AngleStep / 2;
            while (angle < 0)
                angle += MathHelper.TwoPi;

            angle /= _AngleStep;

            return _Colors[((int)angle % _Colors.Count)];
        }

        private void CalculateTransform()
        {
            float r = Level.PlayfieldRadius + 3; //+ 0.5f * BubbleBreak.Scale * Ball.Radius;
            _CenterDistance = r;// +r / (3 * _Colors.Count - 2);

            _AngleStep = MathHelper.TwoPi / (3 * _Colors.Count);

            const float width = 149;
            const float height = 516;

            float otherAngle = MathHelper.PiOver2 - 0.5f * _AngleStep;
            float cathetus = width / (float)Math.Sin(otherAngle);

            _Scale = new Vector2(2, 2 * (cathetus * (float)(Math.Sin(_AngleStep) / Math.Sin(otherAngle)) / height));


            //Vector2 p1 = new Vector2(_CenterDistance, 0);
            //Vector2 p2 = new Vector2(
            //    (float)Math.Cos(_AngleStep) * _CenterDistance, 
            //    (float)Math.Sin(_AngleStep) * _CenterDistance);
            //_Scale = BubbleBreak.Scale * Vector2.Distance(p1, p2) / _ColorItem.Width; // or Height... the same in this case
        }

        public Color GetRandomColor()
        {
            return _Colors[BubbleBreak.Random.Next(_Colors.Count)];
        }

        public void DeleteLostColors(List<Color> currentColors)
        {
            int colorDifference = _Colors.Count - currentColors.Count;


            if (colorDifference > 0)
            {
                Level.CurrentLevel.Stats.AddScoreForColorRemovement(colorDifference);

                for (int i = _Colors.Count - 1; i >= 0; i--)
                {
                    if (!currentColors.Contains(_Colors[i]))
                        RemoveColor(_Colors[i]);
                }
            }
            else if (colorDifference < 0)
            {
                for (int i = currentColors.Count - 1; i >= 0; i--)
                {
                    if (!_Colors.Contains(currentColors[i]))
                    {
                        _Colors.Add(currentColors[i]);
                        CalculateTransform();
                    }
                }
            }
        }

        internal void Delete()
        {
            _Colors.Clear();
            _Colors = null;
            BubblebreakInput.Instance.OnColorKeyDown -= _ColorKeyDown;
            BubblebreakInput.Instance.OnColorKeyUp -= _ColorKeyUp;
        }

        public IEnumerable<Color> IterateAllColors()
        {
            Color[] tmp = _Colors.ToArray();
            foreach(Color c in tmp)
                yield return c;
        }

        internal void ThowBallFromEverywhere()
        {
            Vector2 pos = Vector2.Zero;
            for (int i = 0; i < 3 * _Colors.Count; i++)
            {
                float angle = MathHelper.WrapAngle(_Angle + i * _AngleStep);
                if (Level.CurrentLevel.Bar.IsInBounds(angle))
                    continue;

                pos.X = BubblebreakInput.Instance.Center.X + (float)Math.Cos(angle) * (_CenterDistance - (2 / BubbleBreak.Scale) * Ball.Radius);
                pos.Y = BubblebreakInput.Instance.Center.Y + (float)Math.Sin(angle) * (_CenterDistance - (2 / BubbleBreak.Scale) * Ball.Radius);

                Ball ball = new Ball();
                ball.Position = pos;
                ball.Velocity = Ball.ColoredMovementSpeed  * Vector2.Normalize(new Vector2(Level.PlayfieldCenterX - pos.X, Level.PlayfieldCenterY - pos.Y));
                ball.Color = _Colors[i % _Colors.Count];
                ball.State = BallState.MovingColorBall;

                Level.CurrentLevel.AllBalls.Add(ball);
            }
        }

        public void CreateExtraColor()
        {
            if (_Colors.Count < MaxColors)
            {
                _Colors.Add(GetNextColor(_Colors.Count + 1));
                Color c = _Colors[_Colors.Count - 1];

                Level.CurrentLevel.Bunch.ColorizeRandomBalls(c, 1f / _Colors.Count);

                CalculateTransform();
            }
        }

    }
}
