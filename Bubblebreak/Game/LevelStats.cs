﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Bubblebreak
{
    public class LevelStats
    {
        public int Score { get { return LevelManager.Instance.TotalScore; } set { LevelManager.Instance.TotalScore = value; } }
        public int PoppedBalls                  { get; set; }
        public int MostPoppedBallsWithOneShot   { get; set; }
        public float ElapsedTime                { get; set; }
        public int ShotBalls                    { get; set; }
        public int BouncedBalls                 { get; private set; }
        public int EscapedBalls                 { get; set; }
        public int ColorCount                   { get; set; }
        public List<Color> PlayedColors         { get; private set; }
        public int LevelStartBallCount { get; set; }

        public Dictionary<ItemType, int> CollectedItems { get; private set; }

        Ball lastBouncedBall;


        public LevelStats()
        {
            CollectedItems = new Dictionary<ItemType, int>();
            for (int i = 0; i < (int)ItemType._Count; i++)
                CollectedItems.Add((ItemType)i, 0);

            Clear();
        }

        public void Update(float seconds)
        {
            ElapsedTime += seconds;
        }

        private void Clear()
        {
            PoppedBalls = 0;
            MostPoppedBallsWithOneShot = 0;
            ElapsedTime = 0;
            ShotBalls = 0;
            BouncedBalls = 0;
            EscapedBalls = 0;
            ColorCount = 0;

            PlayedColors = new List<Color>();

            for (int i = 0; i < (int)ItemType._Count; i++)
                CollectedItems[(ItemType)i] = 0;
        }

        public void AddPlayedColor(Color c)
        {
            if (!PlayedColors.Contains(c) && c != Color.White)
                PlayedColors.Add(c);
        }

        public int GetBarBallScore(int levelNumber, int barBallsLeft)
        {
            return (int)((barBallsLeft * barBallsLeft / 3f) * (levelNumber + 1) * (levelNumber + 1));
        }
        public int GetTimeBonus(int levelNumber)
        {
            return (int)(levelNumber / 3f * Math.Max(120 - ElapsedTime, 0));
        }

        internal void AddScoreForPoppingBalls(int poppedBalls, Vector2 position, bool normalPop)
        {
            int s = Figunatchi(poppedBalls - 2) / 3 + 1;
            if (!normalPop)
                s /= 2;

            Score += s;
            ShowScore(s, position);

            if (normalPop)
            {
                if (poppedBalls >= 40)
                {
                    if (!BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.Pop40BallsWithOneShot))
                        if (!BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.Pop30BallsWithOneShot))
                            BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.Pop20BallsWithOneShot);
                }
                else if (poppedBalls >= 30)
                {
                    if (!BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.Pop30BallsWithOneShot))
                        BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.Pop20BallsWithOneShot);
                }
                else if (poppedBalls >= 20)
                {
                    BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.Pop20BallsWithOneShot);
                }
            }
        }

        internal void AddScoreForColorRemovement(int colorCount)
        {
            int b = Level.CurrentLevel.Bunch.FixedBalls.Count;
            int score = 50 + b;
            score *= colorCount;

            Score += score;

            ShowScore(score, Level.PlayfieldCenter);
        }

        internal void AddScoreForePoppedStones(Vector2 position)
        {
            Score += 33;

            ShowScore(33, position);
        }

        internal void AddScoreForDestroyedCenralBall(Vector2 position)
        {
            Score += 100;
            ShowScore(100, position);
        }

        internal void RemoveScoreForExtraBall(int previousBarBalls, int levelNumber)
        {
            int negScore = (levelNumber + 1) * (levelNumber + 1);
            Score -= negScore;

            ShowScore(-negScore, BubblebreakInput.Instance.GetBarPositionFromAngle(Level.PlayfieldRadius, 0));
        }

        public void BounceAnotherBall(Ball ball)
        {
            if (lastBouncedBall != ball)
            {
                BouncedBalls++;

                lastBouncedBall = ball;

                if (BouncedBalls > LevelStartBallCount)
                {
                    BubbleBreak.Instance.Achievements.Achieve(Achievements.AchievementType.MoreBallsBouncedThanInLevel);
                }
            }
        }

        internal void RemoveScoreForBomb(Vector2 position)
        {
            int negScore = Figunatchi(6);
            Score -= negScore;

            ShowScore(-negScore, position);
        }

        private int Figunatchi(int iterations)
        {
            int value = 1;
            for (int i = 2; i <= iterations; i++)
            {
                value += i;
            }
            return value;
        }

        private void ShowScore(int score, Vector2 position)
        {
            Level.CurrentLevel.FloatingScores.AddScore(score, position);
        }

    }
}
