﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util.Manipulation;
using Sunbow.Util.Visual;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util.Sprites;

namespace Bubblebreak.Game.Gui
{
    
    public class BallSpecialEffect
    {
        LinearSingleAnimator animator = new LinearSingleAnimator(0, 1, 0.5f, AnimationRepeatLogic.Pause);
        ItemType type;
        Gradient gradient = new Gradient();
        Vector2 position;

        Frame frame;

        public BallSpecialEffect()
        {
            frame = BubbleBreak.Instance.GameSheet.GetFrame("default", "LevelScoreBackground");
            animator.Pause();
        }

        public void PlayBallEffect(Vector2 position, ItemType type)
        {
            this.animator.Reset();
            this.gradient.Clear();
            this.type = type;
            this.position = BubbleBreak.Transform(position);

            switch (type)
            {
                case ItemType.BombBall:
                    animator.Duration = 0.75f;
                    gradient.AddEntry(new GradientEntry(0, Color.Red * 0.3f));
                    gradient.AddEntry(new GradientEntry(0.2f, Color.Orange * 0.7f));
                    gradient.AddEntry(new GradientEntry(0.5f, Color.Yellow * 0.9f));
                    gradient.AddEntry(new GradientEntry(0.9f, Color.Gray * 0));
                    break;
                case ItemType.RandomBallToCentral:
                    animator.Duration = 0.5f;
                    gradient.AddEntry(new GradientEntry(0, Color.Black * 0));
                    gradient.AddEntry(new GradientEntry(1, Color.Black));
                    break;
                case ItemType.RandomBallToStone:
                    animator.Duration = 0.5f;
                    gradient.AddEntry(new GradientEntry(0, Color.Gray * 0));
                    gradient.AddEntry(new GradientEntry(1, Color.Gray));
                    break;
                default:
                    return;
            }

            animator.Play();
        }

        public void Update(float seconds)
        {
            animator.Update(seconds);
        }

        public void Draw(SpriteBatch batch)
        {
            if (animator.IsPaused)
                return;

            float scale = 1 - animator;

            if(type == ItemType.BombBall)
                scale *= 0.5f;

            BubbleBreak.Instance.GameSheet.Draw(batch, frame, position, 0, scale, gradient.GetColorAt(animator));
        }

    }
}
