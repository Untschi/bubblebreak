﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Sunbow.Util.Manipulation;
using Microsoft.Xna.Framework.Graphics;

namespace Bubblebreak.Game.Gui
{
    public class FloatingText
    {
        public const float Height = 25;
        const float MoveLength = -60;
        public const float Duration = 3;

        public Vector2 Position { get { return new Vector2(startPosition.X, startPosition.Y + MoveLength * animator); } }

        public float AlphaValue { get { return 1 - animator; } }

        string text;
        Vector2 startPosition;
        Color color;
        LinearSingleAnimator animator;

        public FloatingText(int score, Vector2 position)
            : this(score.ToString(), position, ((score < 0) ? Color.Red : Color.Green))
        {
        }
        public FloatingText(string text, Vector2 position, Color color)
        {
            this.text = text;
            Vector2 size = BubbleBreak.Font.MeasureString(text);
            this.startPosition = position - 0.5f * size;
            this.color = color;
            this.animator = new LinearSingleAnimator(0, 1, Duration, AnimationRepeatLogic.Pause);
        }

        public bool Update(float seconds)
        {
            if (animator.IsPaused)
                return false;

            animator.Update(seconds);

            return true;
        }

        public void Draw(SpriteBatch batch)
        {
            Color c = color * AlphaValue;
            batch.DrawString(BubbleBreak.Font, text, Position, c);
        }

    }
}
