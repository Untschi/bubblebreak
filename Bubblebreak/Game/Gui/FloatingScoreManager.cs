﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Sunbow.Util;

namespace Bubblebreak.Game.Gui
{
    public class FloatingScoreManager
    {
        List<FloatingText> scores = new List<FloatingText>();


        public void AddScore(int score, Vector2 position)
        {
            if (score == 0)
                return;

            position = BubbleBreak.Transform(position);

            foreach (FloatingText s in scores)
            {
                float diff = position.Y - s.Position.Y;
                if (diff.IsBetween(0, FloatingText.Height))
                {
                    position = new Vector2(position.X, position.Y + (FloatingText.Height - diff));
                }
                else
                {
                    diff = s.Position.Y - position.Y;
                    if (diff.IsBetween(0, FloatingText.Height))
                    {
                        position = new Vector2(position.X, position.Y - (FloatingText.Height - diff));
                    }
                }
            }

            scores.Add(new FloatingText(score, position));
        }

        public void Update(float seconds)
        {
            for (int i = 0; i < scores.Count; i++)
            {
                if (!scores[i].Update(seconds))
                {
                    scores.RemoveAt(i);
                    i--;
                }
            }
        }

        public void Draw(SpriteBatch batch)
        {
            foreach (FloatingText score in scores)
            {
                score.Draw(batch);
            }
        }
    }
}
