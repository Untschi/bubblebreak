﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Bubblebreak
{
    // wanna some spaghetti code?
    public class BubblebreakInput
    {
        const float MinDigitalVelocity = 0.3f;
        const float MaxDigitalVelocity = 4f;
        const float DigitalVelocityChange = 1f;

        const float UnlockAngleValue = 0.25f;

        public static BubblebreakInput Instance { get; private set; }

        GamePadState _PreviousGamePadState;

#if !ZUNE
        KeyboardState _PreviousKeyboardState;
        MouseState _PreviousMouseState;
#endif

        Point _Center;
        public Point Center { get { return _Center; } }

        float _CurrentBarAngle = 0;
        public float CurrentBarAngle { get { return _CurrentBarAngle; } set { ChangeBarAngle(value); } }

        float _ColorAngle = 0;
        public float ColorAngle { get { return _ColorAngle; } set { ChangeColorAngle(value); } }

        float _DigitalVelocity = 2;

        bool _IsBarConstrained = true;
        public bool IsBarConstrained { get { return _IsBarConstrained; } set { _IsBarConstrained = value; } }

        bool _IsAnalogInputLocked;

        int _DigitalInvert = 1;

        #region event calls

        public event Action OnColorKeyDown;
        public event Action OnColorKeyUp;
        public event Action OnShootKeyDown;
        public event Action OnShootKeyUp;

        public event Action OnDigitalUpDown;
        public event Action OnDigitalDownDown;

        public event Action OnPauseDown;


        bool _IsShootKeyDown, _IsColorKeyDown;
        

        private void CallOnColorKeyUp()
        {
            ChangeBarAngle(_CurrentBarAngle);

            if (OnColorKeyUp != null)
                OnColorKeyUp();

            _IsColorKeyDown = false;
            //_IsAnalogInputLocked = true;
        }
        private void CallOnColorKeyDown()
        {
            ChangeColorAngle(_CurrentBarAngle);

            if (OnColorKeyDown != null && !_IsColorKeyDown)
                OnColorKeyDown();

            _IsColorKeyDown = true;
            _IsAnalogInputLocked = false;
        }
        private void CallOnShootKeyUp()
        {
            if (OnShootKeyUp != null)
                OnShootKeyUp();

            _IsShootKeyDown = false;
        }
        private void CallOnShootKeyDown()
        {
            if (OnShootKeyDown != null && !_IsShootKeyDown)
                OnShootKeyDown();

            _IsShootKeyDown = true;
        }

        private void CallOnDigitalUpDown()
        {
            if (OnDigitalUpDown != null)
                OnDigitalUpDown();
        }

        private void CallOnDigitalDownDown()
        {
            if (OnDigitalDownDown != null)
                OnDigitalDownDown();
        }

        private void CallOnPauseDown()
        {
            if (OnPauseDown != null)
                OnPauseDown();
        }
        #endregion


        public BubblebreakInput(Point center)
        {
            Instance = this;

            _Center = center;
        }

        public void Invert(bool invert)
        {
            _DigitalInvert = (invert) ? -1 : 1;
        }

        // here comes the spaghetti code for you... want some tomato sauce?
        public void Update(float seconds)
        {
#if !ZUNE
            #region Mouse
            ///////////////////
            // *** MOUSE *** //
            MouseState mState = Mouse.GetState();
            if (mState != _PreviousMouseState)
            {
                // Move
                if (_IsColorKeyDown)
                {
                    _ColorAngle = GetMouseAngle(mState);
                    if (_IsBarConstrained)
                        _CurrentBarAngle = _ColorAngle;
                }
                else
                    _CurrentBarAngle = GetMouseAngle(mState);

                // Color Key
                if (mState.RightButton == ButtonState.Pressed && _PreviousMouseState.RightButton == ButtonState.Released)
                    CallOnColorKeyDown();
                else if (mState.RightButton == ButtonState.Released && _PreviousMouseState.RightButton == ButtonState.Pressed)
                    CallOnColorKeyUp();

                // Shoot Key
                if (mState.LeftButton == ButtonState.Pressed && _PreviousMouseState.LeftButton == ButtonState.Released)
                    CallOnShootKeyDown();
                else if (mState.LeftButton == ButtonState.Released && _PreviousMouseState.LeftButton == ButtonState.Pressed)
                    CallOnShootKeyUp();

                if(!BubbleBreak.Instance.IsMouseVisible)
                    ChangeMousePosition(_CurrentBarAngle);

                _PreviousMouseState = mState;
            }
            #endregion

            #region Keyboard
            //////////////////////
            // *** KEYBOARD *** //
            KeyboardState kState = Keyboard.GetState();
            // Move
            if (kState.IsKeyDown(Keys.Left))
            {
                MoveLeft(seconds);
            }
            if (kState.IsKeyDown(Keys.Right))
            {
                MoveRight(seconds);
            }
            // Change Velocity
            if (kState.IsKeyDown(Keys.Up))
            {
                if(!_PreviousKeyboardState.IsKeyDown(Keys.Up))
                    CallOnDigitalUpDown();

                IncreaseDigitalVelocity(seconds);
            }
            if (kState.IsKeyDown(Keys.Down))
            {
                if (!_PreviousKeyboardState.IsKeyDown(Keys.Down))
                    CallOnDigitalDownDown();

                DecreaseDigitalVelocity(seconds);
            }

            // Color Key
            if (kState.IsKeyDown(Keys.LeftShift) && _PreviousKeyboardState.IsKeyUp(Keys.LeftShift))
                CallOnColorKeyDown();
            else if (kState.IsKeyUp(Keys.LeftShift) && _PreviousKeyboardState.IsKeyDown(Keys.LeftShift))
                CallOnColorKeyUp();

            // Shoot Key
            if (kState.IsKeyDown(Keys.Space) && _PreviousKeyboardState.IsKeyUp(Keys.Space))
                CallOnShootKeyDown();
            else if (kState.IsKeyUp(Keys.Space) && _PreviousKeyboardState.IsKeyDown(Keys.Space))
                CallOnShootKeyUp();

            // pause
            if (kState.IsKeyDown(Keys.Escape) && _PreviousKeyboardState.IsKeyUp(Keys.Escape))
                CallOnPauseDown();

            _PreviousKeyboardState = kState;
            #endregion
#endif
            #region Zune / Gamepad
            GamePadState gState = GamePad.GetState(PlayerIndex.One);

            bool isDigitalInput = false;

            // Digital Move
            if (gState.DPad.Left == ButtonState.Pressed)
            {
                MoveLeft(seconds);
                isDigitalInput = true;
            }
            if (gState.DPad.Right == ButtonState.Pressed)
            {
                MoveRight(seconds);
                isDigitalInput = true;
            }
            // Velocity Change
            if (gState.DPad.Up == ButtonState.Pressed)
            {
                if (_PreviousGamePadState.DPad.Up != ButtonState.Pressed)
                    CallOnDigitalUpDown();

                IncreaseDigitalVelocity(seconds);
                isDigitalInput = true;
            } 
            if (gState.DPad.Down == ButtonState.Pressed)
            {
                if (_PreviousGamePadState.DPad.Down != ButtonState.Pressed)
                    CallOnDigitalDownDown();

                DecreaseDigitalVelocity(seconds);
                isDigitalInput = true;
            }

            if (!isDigitalInput)
            {
                // Check Digital <-> Analog change
                if (_IsAnalogInputLocked && gState.ThumbSticks.Left.LengthSquared() != 0)
                {
                    float angle1 = (float)Math.Atan2(-gState.ThumbSticks.Left.Y, gState.ThumbSticks.Left.X);
                    float angle2 = (_IsColorKeyDown) ? _ColorAngle : _CurrentBarAngle;

                    if ((angle1 - angle2 + 2 * MathHelper.TwoPi) % MathHelper.TwoPi < UnlockAngleValue)
                    {
                        _IsAnalogInputLocked = false;
                    }
                }
                // Analog Move
                if(!_IsAnalogInputLocked && gState.ThumbSticks.Left.LengthSquared() != 0)
                {
                    if (_IsColorKeyDown)
                    {
                        _ColorAngle = (float)Math.Atan2(-gState.ThumbSticks.Left.Y, gState.ThumbSticks.Left.X);
                        if (_IsBarConstrained)
                            _CurrentBarAngle = _ColorAngle;
                    }
                    else
                        _CurrentBarAngle = (float)Math.Atan2(-gState.ThumbSticks.Left.Y, gState.ThumbSticks.Left.X);
                }
            }

            // Color Key
            if (gState.Buttons.B == ButtonState.Pressed && _PreviousGamePadState.Buttons.B == ButtonState.Released)
            {
                CallOnColorKeyDown();
            }
            else if (gState.Buttons.B == ButtonState.Released && _PreviousGamePadState.Buttons.B == ButtonState.Pressed)
            {
                CallOnColorKeyUp();
                _IsAnalogInputLocked = true;
            }

            // Shoot Key
            if (gState.Buttons.A == ButtonState.Pressed && _PreviousGamePadState.Buttons.A == ButtonState.Released)
                CallOnShootKeyDown();
            else if (gState.Buttons.A == ButtonState.Released && _PreviousGamePadState.Buttons.A == ButtonState.Pressed)
                CallOnShootKeyUp();

            // pause
            if (gState.Buttons.Back == ButtonState.Pressed && _PreviousGamePadState.Buttons.Back == ButtonState.Released)
                CallOnPauseDown();

            _PreviousGamePadState = gState;

            #endregion

        }

        private void MoveLeft(float seconds)
        {
            if (_IsColorKeyDown)
                ChangeColorAngle(_ColorAngle - _DigitalVelocity * seconds * _DigitalInvert);
            else
                ChangeBarAngle(_CurrentBarAngle - _DigitalVelocity * seconds * _DigitalInvert);

            _IsAnalogInputLocked = true;
        }
        private void MoveRight(float seconds)
        {
            if (_IsColorKeyDown)
                ChangeColorAngle(_ColorAngle + _DigitalVelocity * seconds * _DigitalInvert);
            else
                ChangeBarAngle(_CurrentBarAngle + _DigitalVelocity * seconds * _DigitalInvert);

            _IsAnalogInputLocked = true;
        }

        private void IncreaseDigitalVelocity(float seconds)
        {
            _DigitalVelocity = Math.Min(_DigitalVelocity + DigitalVelocityChange * seconds, MaxDigitalVelocity);

            _IsAnalogInputLocked = true;
        }
        private void DecreaseDigitalVelocity(float seconds)
        {
            _DigitalVelocity = Math.Max(_DigitalVelocity - DigitalVelocityChange * seconds, MinDigitalVelocity);

            _IsAnalogInputLocked = true;
        }

        public Vector2 GetBarPositionFromAngle(float distanceToCenter, float angleOffset)
        {
            return new Vector2(
                _Center.X + (float)Math.Cos(_CurrentBarAngle + angleOffset) * distanceToCenter,
                _Center.Y + (float)Math.Sin(_CurrentBarAngle + angleOffset) * distanceToCenter);
        }



        private void ChangeBarAngle(float value)
        {
            _CurrentBarAngle = (value + MathHelper.TwoPi) % MathHelper.TwoPi;

#if !ZUNE
            ChangeMousePosition(value);
#endif
        }

        private void ChangeColorAngle(float value)
        {
            _ColorAngle = (value + MathHelper.TwoPi) % MathHelper.TwoPi;

            if (_IsBarConstrained)
                _CurrentBarAngle = _ColorAngle;
#if !ZUNE
            ChangeMousePosition(value);
#endif
        }

#if !ZUNE
        private float GetMouseAngle(MouseState mState)
        {
            return (float)Math.Atan2((mState.Y - _Center.Y), (mState.X - _Center.X));
        }

        private void ChangeMousePosition(float angle)
        {
            //float x = _PreviousMouseState.X - _Center.X;
            //float y = _PreviousMouseState.Y - _Center.Y;
            //float dist = (float)Math.Sqrt(x * x + y * y);
            float dist = Level.PlayfieldRadius;

            Mouse.SetPosition(
                _Center.X + (int)((Math.Cos(angle) * dist)),
                _Center.Y + (int)((Math.Sin(angle) * dist)));

            _PreviousMouseState = Mouse.GetState();
        }

        public Vector2 GetMousePosition()
        {
            return new Vector2(_PreviousMouseState.X, _PreviousMouseState.Y);
        }
#endif

        internal void ClearEvents()
        {
            OnColorKeyDown = null;
            OnColorKeyUp = null;
            OnShootKeyDown = null;
            OnShootKeyUp = null;
        }
    }
}
