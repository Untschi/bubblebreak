using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Sunbow.Util;

namespace Sunbow.Input.Touch
{
    public class TouchRect :TouchZone
    {
        Rectangle rect;

        // creates a zone over the whole screen
        public TouchRect(MultiTouchManager manager, GraphicsDeviceManager graphics)
            : this(manager, new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight))
        {
        }

        public TouchRect(MultiTouchManager manager, Rectangle rect)
            : base(manager)
        {
            this.rect = rect;
        }

        public override bool IsPointInside(Vector2 point)
        {
            return rect.Contains(point.ToPoint());
        }
    }
}
