using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Shapes.Geometry;

namespace Sunbow.Input.Touch
{
    public class TouchShape : TouchZone
    {
        public Shape Shape { get { return shape; } }
        Shape shape;

        public TouchShape(MultiTouchManager manager, Shape shape)
            : base(manager)
        {
            this.shape = shape;
        }

        public override bool IsPointInside(Vector2 point)
        {
            return shape.IsPointInside(point);
        }
    }
}
