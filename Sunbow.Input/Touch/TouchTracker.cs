using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework;

namespace Sunbow.Input.Touch
{
    public class TouchTracker
    {
        const float MaxTapDistanceSq = 3 * 3;
        const float MaxTapDuration = 0.4f;
        const float HoldTime = 1;

        struct TouchTimeInfo
        {
            internal TimeSpan Time;
            internal TouchLocation Location;
        }
        List<TouchTimeInfo> timeline = new List<TouchTimeInfo>();

        public Vector2 FirstPosition { get { return timeline[0].Location.Position; } }
        public Vector2 LatestPosition { get { return timeline[timeline.Count - 1].Location.Position; } }

        float lastDuration = 0;
        float duration = 0;
        TimeSpan timeStamp;

        MultiTouchManager manager;

        public TouchTracker(MultiTouchManager manager)
        {
            this.manager = manager;
        }

        internal void Update(GameTime gameTime, TouchLocation tl)
        {
            if(timeStamp == TimeSpan.Zero)
                timeStamp = gameTime.TotalGameTime;

            lastDuration = duration;
            duration = (float)(gameTime.TotalGameTime - timeStamp).TotalSeconds;

            timeline.Add(new TouchTimeInfo() { Time = gameTime.TotalGameTime, Location = tl });

            if (tl.State == TouchLocationState.Invalid || tl.State == TouchLocationState.Released)
            {
                manager.ObsoleteList.Add(tl.Id);
            }
        }

        public GestureSample? CheckGesture(GestureType gesture)
        {
            switch (gesture)
            {
                case GestureType.None:
                    break;

                case GestureType.DoubleTap:

                    if (duration <= MaxTapDuration
                        && WithinDistance(manager.LastTapPoint, MaxTapDistanceSq))
                    {
                        return new GestureSample(gesture, timeStamp, manager.LastTapPoint, new Vector2(), new Vector2(), new Vector2());
                    }
                    break;

                case GestureType.Tap:

                    if (duration <= MaxTapDuration 
                        && WithinDistance(FirstPosition, MaxTapDistanceSq))
                    {
                        manager.Tap(FirstPosition);
                        return new GestureSample(gesture, timeStamp, FirstPosition, new Vector2(), new Vector2(), new Vector2());
                    }
                    break;

                case GestureType.Hold:

                    if (lastDuration < HoldTime && duration >= HoldTime
                        && WithinDistance(FirstPosition, MaxTapDistanceSq))
                    {
                        return new GestureSample(gesture, timeStamp, FirstPosition, new Vector2(), new Vector2(), new Vector2());
                    }
                    break;

                case GestureType.Flick:
                    throw new NotImplementedException();        // TODO!

                case GestureType.FreeDrag:
                case GestureType.HorizontalDrag:
                case GestureType.VerticalDrag:

                    Vector2 lastPos = LatestPosition;
                    Vector2 nextToLastPos = timeline[timeline.Count - 2].Location.Position;

                    if (gesture == GestureType.HorizontalDrag)
                    {
                        lastPos.Y = 0;
                        nextToLastPos.Y = 0;
                    }
                    else if (gesture == GestureType.VerticalDrag)
                    {
                        lastPos.X = 0;
                        nextToLastPos.X = 0;
                    }

                    if (timeline.Count > 1
                        && lastPos != nextToLastPos)
                    {
                        return new GestureSample(gesture, timeStamp, LatestPosition,
                            new Vector2(), lastPos - nextToLastPos, new Vector2());
                    }
                    break;

                case GestureType.DragComplete:
                    if (timeline[timeline.Count - 1].Location.State == TouchLocationState.Released)
                    {
                        return new GestureSample(gesture, timeStamp, new Vector2(), new Vector2(), new Vector2(), new Vector2());
                    }
                    break;

                case GestureType.Pinch:
                case GestureType.PinchComplete:
                    throw new NotImplementedException();


            }
            return null;
        }

        private bool WithinDistance(Vector2 position, float distanceSquared)
        {
            foreach (TouchTimeInfo info in timeline)
            {

                if (Vector2.DistanceSquared(info.Location.Position, position) > distanceSquared)
                    return false;
            }

            return true;
        }

    }
}
