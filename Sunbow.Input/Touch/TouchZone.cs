using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework;
using Sunbow.Util;

namespace Sunbow.Input.Touch
{
    public abstract class TouchZone : IDisposable
    {
        public GestureType EnabledGestures { get { return enabledGestures; } set { enabledGestures = value; UpdateEnabledGestures(); } }
        GestureType enabledGestures;

        List<GestureType> enabledGestureTypes = new List<GestureType>();
        List<TouchTracker> trackers = new List<TouchTracker>();

        MultiTouchManager manager;

        protected TouchZone(MultiTouchManager manager)
        {
            this.manager = manager;
            this.manager.RegisterTouchZone(this);
        }
        ~TouchZone() { Dispose(); }


        internal void Update(GameTime gameTime)
        {
            foreach (TouchTracker tracker in trackers)
            {
                foreach (GestureType gesture in enabledGestureTypes)
                {
                    switch (gesture)
                    {
                        case GestureType.FreeDrag:
                        case GestureType.HorizontalDrag:
                        case GestureType.VerticalDrag:
                        case GestureType.DragComplete:
                            if (!IsPointInside(tracker.LatestPosition))
                                continue;
                            break;
                        default:
                            break;
                    }

                    GestureSample? sample = tracker.CheckGesture(gesture);

                    if (sample != null)
                    {
                        manager.OnGestureActionCallback(this, (GestureSample)sample);
                    }
                }
            }
        }

        internal void IntroduceTracker(TouchTracker tracker)
        {
            if (IsPointInside(tracker.FirstPosition))
                trackers.Add(tracker);
        }

        internal void UnregisterTracker(TouchTracker tracker)
        {
            trackers.Remove(tracker);
        }

        private void UpdateEnabledGestures()
        {
            enabledGestureTypes.Clear();

            foreach (GestureType t in Helper.GetEnumValues<GestureType>())
            {
                if ((EnabledGestures & t) == t)
                {
                    enabledGestureTypes.Add(t);
                }
            }
        }

        public abstract bool IsPointInside(Vector2 point);



        public void Dispose()
        {
            manager.UnregisterTouchZone(this);
        }
    }
}
