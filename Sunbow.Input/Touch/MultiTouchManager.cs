using System;
using System.Collections.Generic;
//using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Input.Touch;
using Microsoft.Xna.Framework.Media;

namespace Sunbow.Input.Touch
{
    public class MultiTouchManager
    {
        const float MaxDoubleTapWaitTime = 0.5f;

        internal static readonly Vector2 InvalidLocation = new Vector2(float.NaN, float.NaN);

        TouchCollection touchCollection;

        Dictionary<int, TouchTracker> touchTrackers = new Dictionary<int, TouchTracker>();
        internal List<int> ObsoleteList = new List<int>();

        internal List<TouchZone> zones = new List<TouchZone>();

        internal Vector2 LastTapPoint { get { return (doubleTapWaiter >= 0) ? lastTapPoint : InvalidLocation; } }
        Vector2 lastTapPoint = InvalidLocation;
        float doubleTapWaiter = -1;

        public event Action<TouchZone, GestureSample> GestureActionCallback;
        

        public void Update(GameTime gameTime)
        {
            doubleTapWaiter -= (float)gameTime.ElapsedGameTime.TotalSeconds;

            touchCollection = TouchPanel.GetState();

            // Update TouchTrackers
            foreach (TouchLocation tl in touchCollection)
            {
                if (!touchTrackers.ContainsKey(tl.Id))
                {
                    touchTrackers.Add(tl.Id, new TouchTracker(this));
                }
                touchTrackers[tl.Id].Update(gameTime, tl);
            }

            // Update TouchZones
            foreach (TouchZone zone in zones)
            {
                zone.Update(gameTime);
            }

            // Remove Released Fingers
            foreach (int i in ObsoleteList)
            {
                foreach (TouchZone zone in zones)
                {
                    zone.UnregisterTracker(touchTrackers[i]);
                }
                touchTrackers.Remove(i);
            }
            ObsoleteList.Clear();
        }

        internal void Tap(Vector2 position)
        {
            this.lastTapPoint = position;
            doubleTapWaiter = MaxDoubleTapWaitTime;
        }

        internal void OnGestureActionCallback(TouchZone zone, GestureSample sample)
        {
            if (GestureActionCallback != null)
                GestureActionCallback(zone, sample);
        }


        internal void RegisterTouchZone(TouchZone touchZone)
        {
            if(!zones.Contains(touchZone))
                zones.Add(touchZone);
        }

        internal void UnregisterTouchZone(TouchZone touchZone)
        {
            zones.Remove(touchZone);
        }
    }
}
