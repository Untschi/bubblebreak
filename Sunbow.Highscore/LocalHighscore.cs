﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Sunbow.Util;
using Sunbow.Util.IO;
using System.IO;
using System.IO.IsolatedStorage;

namespace Sunbow.Highscore
{
    public class LocalHighscore 
    {
        public Table ScoreTable { get { return scoreTable; } }
        Table scoreTable;

        public int MaximumEntries { get { return maxEntries; } set { maxEntries = value; dirty = true; } }
        int maxEntries = 100;

        bool dirty = false;

        public string PlayerRank { get { return playerRank.ToString(); } }
        int playerRank = -1;

        public LocalHighscore(string tableFileName)
        {

            using (IsolatedStorageFile file = Helper.GetUserStoreForAppDomain())
            {
                bool fileExist = file.FileExists(@"high.score");

                if (fileExist)
                {
                    using (IsolatedStorageFileStream stream = file.OpenFile(@"high.score", FileMode.OpenOrCreate))
                    {
                        scoreTable = new Table(stream, tableFileName, CSVSetting.UniqueColumnHeaders);
                        stream.Close();
                    }
                }
                else
                {
                    scoreTable = new Table(tableFileName, 3, 1);
                    scoreTable[0, 0] = "hash";
                    scoreTable[1, 0] = "player";
                    scoreTable[2, 0] = "score";
                    //scoreTable.AppendRow(GenerateHash("Chief", 3000), "Chief", "3000");
                    //scoreTable.AppendRow(GenerateHash("Skill", 2000), "Skill", "2000");
                    //scoreTable.AppendRow(GenerateHash("Check", 1000), "Check", "1000");
                    //scoreTable.AppendRow(GenerateHash("Noob", 500), "Noob", "500");
                }
            }
        }

        public void AddNewScore(string playerName, int score)
        {
            string scoreString = GetFormattedScore(score);
            scoreTable.AppendRow(GenerateHash(playerName, score), playerName, scoreString);
            dirty = true;
            PrepareTable(playerName, scoreString);
        }
        public string GetFormattedScore(int score)
        {
            return score.ToString("### ### ##0");
        }

        public void Save()
        {
            PrepareTable();
            ScoreTable.Save();
        }

        public IEnumerable<string[]> GetScoreIterator(int start, int count)
        {
            PrepareTable();

            for (int i = start; i < Math.Min(scoreTable.Rows, start + count); i++)
            {
                yield return new string[]
                    {
                        i.ToString(),
                        scoreTable["player", i],
                        scoreTable["score", i],
                    };
            }

        }

        private void PrepareTable() { PrepareTable(null, null); }
        private void PrepareTable(string playerName, string score)
        {
            if (!dirty)
                return;

            scoreTable.SortByColumn<int>("score", false);

            // evaluate player rank
            if (playerName != null)
            {
                for (int i = 1; i < scoreTable.Rows; i++)
                {
                    if (scoreTable["player", i] == playerName && scoreTable["score", i] == score)
                    {
                        playerRank = i;
                        break;
                    }
                }
            }

            if(playerRank < maxEntries)
                scoreTable.RemoveRowRange(MaximumEntries + 1, scoreTable.Rows - (MaximumEntries + 1));

            dirty = false;
        }

        private string GenerateHash(string player, int score)
        {
            return Helper.GenerateHash_SHA256("local" + player + "high" + score + "score");
        }

    }
}
