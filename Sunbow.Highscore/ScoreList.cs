﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunbow.Highscore
{
    public class ScoreList : WebQuery
    {
        public string StartRank { get { return startRank.ToString(); } }
        public string PlayerRank { get { return playerRank.ToString(); } }

        int startRank, playerRank;

        public ScoreList(int startRank, string playerName, string playerScore, string parseData)
        {
            this.startRank = startRank + 1;
            this.playerRank = -1;

            AddEntries(parseData);

            int idx = startRank;
            foreach(KeyValuePair<string, string> pair in Entries)
            {
                if (pair.Key == playerName && pair.Value == playerScore)
                {
                    this.playerRank = idx + 1;
                    break;
                }
                idx++;
            }
        }


        public IEnumerable<string[]> GetScoreIterator()
        {
            int idx = startRank;
            foreach (KeyValuePair<string, string> pair in Entries)
            {
                yield return new string[] { idx.ToString(), pair.Key, pair.Value };
                idx++;
            }
        }
    }
}
