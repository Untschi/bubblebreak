﻿using System;
using System.Collections.Generic;

using System.Linq;
using System.Text;
using System.Security.Cryptography;

namespace Sunbow.Highscore
{
    public class WebQuery
    {
        public List<KeyValuePair<string, string>> Entries { get { return entries; } }
        List<KeyValuePair<string, string>> entries = new List<KeyValuePair<string, string>>();

        public void AddEntries(params string[] keyValuePairs)
        {
            for (int i = 0; i < keyValuePairs.Length; i++)
            {
                Entries.Add(new KeyValuePair<string,string>(keyValuePairs[i], keyValuePairs[++i]));
            }
        }

        public void AddEntries(string queryString)
        {
            string[] parts = queryString.Split(new char[] { '&', '?' }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string s in parts)
            {
                string[] keyValue = s.Split(new char[] { '=' }, StringSplitOptions.RemoveEmptyEntries);
                entries.Add(new KeyValuePair<string,string>(keyValue[0], keyValue[1]));
            }
        }

        public string GetFirstEntry(string key)
        {
            foreach (KeyValuePair<string, string> pair in Entries)
            {
                if (pair.Key == key)
                    return pair.Value;
            }
            return null;
        }

        public byte[] ToByteArray()
        {
            return Encoding.UTF8.GetBytes(ToString());
        }

        /// <summary>
        /// Converts the Query data to a query-string
        /// </summary>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (KeyValuePair<string, string> entry in entries)
            {
                sb.Append(string.Format("{0}={1}&", entry.Key, entry.Value));
            }
            sb.Remove(sb.Length - 1, 1);

            return sb.ToString();
        }


        public bool Contains(params char[] chars)
        {
            foreach (KeyValuePair<string, string> pair in entries)
            {
                foreach(char c in chars)
                {
                    if (pair.Key.Contains(c) || pair.Value.Contains(c))
                        return true;
                }
            }
            return false;
        }
    }
}
