﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sunbow.Highscore
{
    public class ScoreRequester : WebRequester
    {
        public Dictionary<string, ScoreList> ScoreLists { get { return scoreLists; } }
        Dictionary<string, ScoreList> scoreLists = new Dictionary<string,ScoreList>();

        public ScoreRequester(string url, ScoreQuery query)
            : base(url)
        {
            Query = query;
        }

        protected override void ProcessResponse(string response)
        {
            try
            {
                string[] responseParts = response.Split(new char[] { '?' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < responseParts.Length; i += 2)
                {
                    int startIndex = 0;
                    string[] listDefinitionParts = responseParts[i].Split(new char[] { '#' }, StringSplitOptions.RemoveEmptyEntries);
                    if (listDefinitionParts.Length > 1)
                        startIndex = int.Parse(listDefinitionParts[1]);

                    ScoreList scoreList = new ScoreList(startIndex, Query.GetFirstEntry("name"), Query.GetFirstEntry("score"), responseParts[i + 1]);

                    scoreLists.Add(listDefinitionParts[0], scoreList);
                }
            }
            catch (Exception ex)
            {
                CallRequestFailedEvent(ex, RequestErrorCause.Response);
            }
        }

        public ScoreList GetScoreList(string scoreListName)
        {
            if (scoreLists.ContainsKey(scoreListName))
                return scoreLists[scoreListName];

            return null;
        }

    }
}
