﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using Sunbow.Util;

namespace Sunbow.Highscore
{
    public enum ProductInfo
    {
        undefined = 0,
        WP7_sell = 1,
        WP7_free = 2,
        iPhone_sell = 3,
        iPhone_free = 4,
        Android_sell = 5,
        Android_free = 6,
    }

    public class ScoreQuery : WebQuery
    {
        public ScoreQuery()
        {
            AddEntries("action", "view_scores");
        }

        public ScoreQuery(string name, long score, ProductInfo productInfo, string versionInfo)
        {
            string info = productInfo.ToString().ToLower();
            string hashString = "J" + name + "e" + info + "R" + versionInfo + "e" + ((3 * score) * (5 * score) + 7).ToString() + "T";
            string hash = Helper.GenerateHash_SHA256(hashString);

            AddEntries(
                "action", "submit_score",
                "name", name, 
                "score", score.ToString(), 
                "info", info, 
                "version", versionInfo, 
                "hash", hash);
        }
    }
}
