<?
$top_count = 10;
$view_count = 20;
$around = 2;
$max_entries = 5000;
$max_entry_tolerance = 500;


$allover = "TRUE";
$month = "EXTRACT( YEAR_MONTH FROM Time ) = EXTRACT( YEAR_MONTH FROM CURRENT_DATE )";
$week = "YearWeek( Time ) = YearWeek( CURRENT_DATE )";
$today = "Date( Time ) = CURRENT_DATE";

                    
$server = "liquidfirearts.com";
$username = "d0107dde";
$password = "Ud8SYkYxBuK8rdXQ";
$database = "d0107dde";
// $table = "bubblebreak_highscore";

$connection = mysql_connect($server, $username, $password);

if (!$connection) 
{
  echo "<dce>"; // database connection error
}
else
{
  if (!mysql_select_db($database)) 
  {
    echo "<dse>"; // database selection error
  }
  else 
  {
    $action = mysql_real_escape_string($_REQUEST["action"]);
  
    if($action == "submit_score")
    {
      $name = mysql_real_escape_string($_REQUEST["name"]);
      if($name == "")
      {
        $name = "_player";
      }
      
      $score = mysql_real_escape_string($_REQUEST["score"]);
      $info = mysql_real_escape_string($_REQUEST["info"]);
      $version = mysql_real_escape_string($_REQUEST["version"]);
      $hash = mysql_real_escape_string($_REQUEST["hash"]);
    
      $calc = (3 * $score) * (5 * $score) + 7;
      $secret = "J".$name."e".$info."R".$version."e".$calc."T";
      $compare = hash("sha256", $secret, FALSE);
                       
      if($hash==$compare)
      {                 
        keep_db_small($max_entries, $max_entry_tolerance);
      
        // insert
        mysql_query("
          INSERT INTO bubblebreak_highscore 
            ( `Info` , `Player` , `Score` , `Version`)
          VALUES 
            ('$info', '$name', '$score', '$version') ");
                  
        // allover
        echo_top_data($allover, $top_count, $connection, "top_all");
        echo_rank_data($allover, $around, $connection, "rank_all", $name, $score);
         
        // month
        echo_top_data($month, $top_count, $connection, "top_month");
        echo_rank_data($month, $around, $connection, "rank_month", $name, $score);
         
        // week
        echo_top_data($week, $top_count, $connection, "top_week");
        echo_rank_data($week, $around, $connection, "rank_week", $name, $score);
         
        // today
        echo_top_data($today, $top_count, $connection, "top_day");
        echo_rank_data($today, $around, $connection, "rank_day", $name, $score);
        
        
      }
      else
      {
        echo "<hnv>"; // hash not valid
      }
    
    }
    elseif($action == "view_scores")
    {
        // allover
        echo_top_data($allover, $view_count, $connection, "top_all");
        echo_top_data($month, $view_count, $connection, "top_month");
        echo_top_data($week, $view_count, $connection, "top_week");
        echo_top_data($today, $view_count, $connection, "top_day");
    }
    else
    {
      echo "<uae>"; // unknown action error
    }
  }
}

function echo_scores($sql, $name)
{
    echo "?#".$name."?";
    while($row = mysql_fetch_array($sql, MYSQL_ASSOC))
    {
      echo $row['Player']."=".$row['Score']."&";
    }
}

function echo_top_data($condition, $count, $connection, $name)
{
  $command = "                  
        SELECT Player, Score 
        FROM bubblebreak_highscore 
        WHERE " . $condition . "
        ORDER BY Score DESC, Time DESC
        LIMIT 0, $count
      ";
  $top_data = mysql_query($command, $connection);
  echo_scores($top_data, $name);
}

function echo_rank_data($condition, $half_range, $connection, $name, $player, $score)
{
    $ranks = mysql_query("
        SELECT x2.rownum
        FROM        
          (SELECT @rownum:=@rownum+1 rownum, t.*
          FROM (SELECT @rownum:=0) x1, bubblebreak_highscore t
          WHERE " . $condition . "
          ORDER BY Score DESC, Time DESC) x2
        WHERE Player = '$player' AND Score = $score
      ");
      
      $rank_array = mysql_fetch_row($ranks);
      $rank = $rank_array[0];
      
      $start = $rank - $half_range - 1;
      $range = 2 * $half_range + 1;
      
      if($start < 0)
      {
        $range += $start;
        $start = 0;
      }
      
      $rank_range = mysql_query("
        SELECT Player, Score
        FROM bubblebreak_highscore
        WHERE " . $condition . "
        ORDER BY Score DESC, Time DESC
        LIMIT $start, $range
      ", $connection);           
      
      
      echo_scores($rank_range, $name . "#" . $start);         
}

function keep_db_small($max_entries, $tolerance)
{
  $count_sql = mysql_query("SELECT * FROM bubblebreak_highscore");
  $entries_count = mysql_num_rows($count_sql);
  
  if($entries_count > $max_entries + $tolerance)
  {
      $min_score_sql = mysql_query("
        SELECT Score
        FROM        
          (SELECT @rownum:=@rownum+1 rownum, t.*
          FROM (SELECT @rownum:=0) x1, bubblebreak_highscore t
          ORDER BY Score DESC, Time DESC) x2
        WHERE x2.rownum = $max_entries
      ");
      
      $min_score_array = mysql_fetch_row($min_score_sql);
      
      if($min_score_array)
      {
        $min_score = $min_score_array[0];
      
        mysql_query("
          DELETE LOW_PRIORITY 
          FROM bubblebreak_highscore
          WHERE Score < $min_score
        ");
      }
  }  
}

?>