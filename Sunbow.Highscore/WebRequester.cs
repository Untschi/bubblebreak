﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net;
using System.Threading;

namespace Sunbow.Highscore
{


    public enum RequestMethod
    {
        POST = 0,
        GET = 1,
    }

    public enum RequestErrorCause
    {
        Request = 0,
        Response = 1,
        TimeOut = 2,
        Server = 3,
        ForbiddenChar = 4,
    }

    public class WebRequester
    {
        static readonly char[] FORBIDDEN_CHARS  = new char[] { '?', '&', '#', '<', '>' };

        protected class HttpWebRequestState
        {
            // This class stores the State of the request.
            const int BUFFER_SIZE = 1024;
            internal StringBuilder requestData;
            internal byte[] BufferRead;
            internal HttpWebRequest request;
            internal HttpWebResponse response;
            internal Stream streamResponse;
            internal HttpWebRequestState()
            {
                BufferRead = new byte[BUFFER_SIZE];
                requestData = new StringBuilder("");
                request = null;
                streamResponse = null;
            }
        }
        public static ManualResetEvent allDone = new ManualResetEvent(false);
        const int BUFFER_SIZE = 1024;
        const int DefaultTimeout = 60 * 1000; // 1 minute timeout


        public string Url { get; set; }

        public RequestMethod RequestMethod { get { return requestMethod; } set { requestMethod = value; } }
        RequestMethod requestMethod = RequestMethod.POST;

        public WebQuery Query { get { return query; } set { query = value; } }
        WebQuery query;

        public string ContentType { get { return contentType; } set { contentType = value; } }
        string contentType = "application/x-www-form-urlencoded";

        public string Response { get { return response; } }
        string response;

        public event Action<WebRequester, Exception, RequestErrorCause> RequestFailed;
        public event Action<WebRequester> RequestSucceeded;


        public WebRequester(string url)
        {
            Url = url;
        }

        public void CreateWebRequest()
        {
            if (string.IsNullOrEmpty(Url))
                throw new Exception("URL not set");

            if (query.Contains(FORBIDDEN_CHARS))
            {
                CallRequestFailedEvent(null, RequestErrorCause.ForbiddenChar);
                return;
            }

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
            request.ContentType = contentType;
            request.Method = requestMethod.ToString();

            request.BeginGetRequestStream(RequestStreamCallback, request);

        }

        public void RequestStreamCallback(IAsyncResult result)
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)result.AsyncState;
                Stream postStream = request.EndGetRequestStream(result);

                byte[] byteArray = query.ToByteArray();
                postStream.Write(byteArray, 0, byteArray.Length);
                postStream.Close();


                HttpWebRequestState state = new HttpWebRequestState();
                state.request = request;

                IAsyncResult asyncResult = (IAsyncResult)request.BeginGetResponse(ResponseCallback, state);

                ThreadPool.RegisterWaitForSingleObject(asyncResult.AsyncWaitHandle, new WaitOrTimerCallback(TimeoutCallback), request, DefaultTimeout, true);

                allDone.WaitOne();
                state.response.Close();
            }
            catch (WebException wex)
            {
                CallRequestFailedEvent(wex, RequestErrorCause.Request);
            }
            catch (Exception)
            { }
        }

        public void ResponseCallback(IAsyncResult result)
        {
            try
            {
                // State of request is asynchronous.
                HttpWebRequestState state = (HttpWebRequestState)result.AsyncState;
                HttpWebRequest request = state.request;
                state.response = (HttpWebResponse)request.EndGetResponse(result);

                // Read the response into a Stream object.
                Stream responseStream = state.response.GetResponseStream();
                state.streamResponse = responseStream;

                // Begin the Reading of the contents of the HTML page and print it to the console.
                IAsyncResult asynchronousInputRead = responseStream.BeginRead(state.BufferRead, 0, BUFFER_SIZE, new AsyncCallback(ReadCallBack), state);
                return;
            }
            catch (WebException ex)
            {
                CallRequestFailedEvent(ex, RequestErrorCause.Response);
            }
            allDone.Set();

        }

        private void ReadCallBack(IAsyncResult asyncResult)
        {
            try
            {

                HttpWebRequestState state = (HttpWebRequestState)asyncResult.AsyncState;
                Stream responseStream = state.streamResponse;
                int read = responseStream.EndRead(asyncResult);
                // Read the HTML page and then print it to the console.
                if (read > 0)
                {
                    state.requestData.Append(Encoding.UTF8.GetString(state.BufferRead, 0, read));
                    IAsyncResult asynchronousResult = responseStream.BeginRead(state.BufferRead, 0, BUFFER_SIZE, new AsyncCallback(ReadCallBack), state);
                    return;
                }
                else
                {
                    if (state.requestData.Length > 1)
                    {
                        
                        response = state.requestData.ToString();
                    }

                    responseStream.Close();

                    if (response.Contains('<'))
                    {
                        CallRequestFailedEvent(null, RequestErrorCause.Server);
                    }
                    else
                    {
                        ProcessResponse(response);

                        if(RequestSucceeded != null)
                            RequestSucceeded(this);
                    }
                }
                
            }
            catch (WebException ex)
            {
                CallRequestFailedEvent(ex, RequestErrorCause.Response);
            }
            allDone.Set();

        }


        private void TimeoutCallback(object state, bool timedOut)
        {
            if (timedOut)
            {
                HttpWebRequest request = state as HttpWebRequest;
                if (request != null)
                {
                    request.Abort();

                    CallRequestFailedEvent(null, RequestErrorCause.TimeOut);
                }
            }
        }

        protected virtual void ProcessResponse(string response)
        {
            // left blank for overrides
        }

        protected void CallRequestFailedEvent(Exception ex, RequestErrorCause cause)
        {
            if (RequestFailed != null)
                RequestFailed(this, ex, cause);
        }
    }
}
